#!/bin/bash
  ####################################--vgpu unlock tools--####################################
  #             ______  _______  __    __      __    __          __                   __       
  #            /      \|       \|  \  |  \    |  \  |  \        |  \                 |  \      
  #  __     __|  ▓▓▓▓▓▓\ ▓▓▓▓▓▓▓\ ▓▓  | ▓▓    | ▓▓  | ▓▓_______ | ▓▓ ______   _______| ▓▓   __ 
  # |  \   /  \ ▓▓ __\▓▓ ▓▓__/ ▓▓ ▓▓  | ▓▓    | ▓▓  | ▓▓       \| ▓▓/      \ /       \ ▓▓  /  \
  #  \▓▓\ /  ▓▓ ▓▓|    \ ▓▓    ▓▓ ▓▓  | ▓▓    | ▓▓  | ▓▓ ▓▓▓▓▓▓▓\ ▓▓  ▓▓▓▓▓▓\  ▓▓▓▓▓▓▓ ▓▓_/  ▓▓
  #   \▓▓\  ▓▓| ▓▓ \▓▓▓▓ ▓▓▓▓▓▓▓| ▓▓  | ▓▓    | ▓▓  | ▓▓ ▓▓  | ▓▓ ▓▓ ▓▓  | ▓▓ ▓▓     | ▓▓   ▓▓ 
  #    \▓▓ ▓▓ | ▓▓__| ▓▓ ▓▓     | ▓▓__/ ▓▓    | ▓▓__/ ▓▓ ▓▓  | ▓▓ ▓▓ ▓▓__/ ▓▓ ▓▓_____| ▓▓▓▓▓▓\ 
  #     \▓▓▓   \▓▓    ▓▓ ▓▓      \▓▓    ▓▓     \▓▓    ▓▓ ▓▓  | ▓▓ ▓▓\▓▓    ▓▓\▓▓     \ ▓▓  \▓▓\
  #      \▓     \▓▓▓▓▓▓ \▓▓       \▓▓▓▓▓▓       \▓▓▓▓▓▓ \▓▓   \▓▓\▓▓ \▓▓▓▓▓▓  \▓▓▓▓▓▓▓\▓▓   \▓▓
  #  Author : ksh
  #  Mail: kevinshane@vip.qq.com
  #  Version: v0.0.4
  #  Github: https://github.com/kevinshane/unlock
  #  Credit: https://github.com/DualCoder/vgpu_unlock
  #  Discor: https://discord.gg/qh6dvPtxvb
  ####################################--vgpu unlock tools--####################################

# For errors look in dmesg for:
  # - BAR3 mapped
  # - Magic Found
  # - Key Found
  # - Failed to find ...
  # - Invalid sign or blocks pointer
  # - Generate signature
  # - Signature does not match
  # - Decrypted first block

# nvidia-smi Perf
  # - P0/P1 - Maximum 3D performance
  # - P2/P3 - Balanced 3D performance-power
  # - P8 - Basic HD video playback
  # - P10 - DVD playback
  # - P12 - Minimum idle power consumption

# host driver download
  # https://cloud.google.com/compute/docs/gpus/grid-drivers-table
  # https://storage.googleapis.com/nvidiaowo/NVIDIA-GRID-Linux-KVM-450.89-452.57.zip
  # https://storage.googleapis.com/nvidiaowo/NVIDIA-GRID-Linux-KVM-460.32.04-460.32.03-461.33.zip
  # https://cn.driverscollection.com/?H=Quadro%20RTX%206000&By=NVidia&SS=Windows%2010%2064-bit&dpage=5

# define 11 uuid, do not modify these uuids
AAA="1728f397-99e5-47a6-9e70-ac00d8031596"
BBB="2d5d39f8-80f3-4925-b790-8f7a405b8cb5"
CCC="3b305d4e-88f7-4bea-b2e5-dd436142dc60"
DDD="44da7489-1b80-4e12-93e7-ae2b2b49876f"
EEE="5e694858-12ed-4c55-b57a-c4e889bee0b2"
FFF="6b749fe2-5835-46b5-aff2-19c79b60ddcc"
GGG="7fcb38e2-41c2-4807-80f1-3b79d501f1b5"
HHH="8f601d2d-431a-421c-9f51-49280cfddd8f"
III="94df9f85-44b9-4f48-a81b-8a19f0d19191"
JJJ="108d9d06-eb33-4fe0-8115-cc2d5f6f8589"
KKK="11049ddb-b7ab-4a0c-9111-ab4529b39489"

# define vgpu stuff
# PCI="$(lspci | grep -i nvidia | grep -i vga | grep -E 'GM|GP|TU' | awk '{print $1}')"
vgpuScriptPath="/root/vgpu_unlock/scripts/vgpu-name.sh"
PCI="$(lspci | grep -i nvidia | grep -i vga | awk '{print $1}' | head -n 1)"

# which pve version
pveRepo=`cat /etc/debian_version |awk -F"." '{print $1}'`
case "$pveRepo" in
  11 )
      pveRepo="bullseye"
      ;;
  10 )
      pveRepo="buster"
      ;;
  9 )
      pveRepo="stretch"
      ;;
  * )
      pveRepo=""
esac

startUpdate(){
  runUpdate(){
    # enable iommu group
    if [ `grep "iommu" /etc/default/grub | wc -l` = 0 ];then
      if [ `cat /proc/cpuinfo|grep Intel|wc -l` = 0 ];then 
        sed -i 's#quiet#quiet amd_iommu=on iommu=pt#' /etc/default/grub && update-grub
        echo "$(tput setaf 2)AMD iommu satisfied 已开启AMD IOMMU √$(tput sgr 0)"
      else
        sed -i 's#quiet#quiet intel_iommu=on iommu=pt#' /etc/default/grub && update-grub
        echo "$(tput setaf 2)Intel iommu satisfied 已开启Intel IOMMU √$(tput sgr 0)"
      fi
    else echo "$(tput setaf 2)iommu satisfied 已开启IOMMU √$(tput sgr 0)"
    fi

    # add vfio modules
    if [ `grep "vfio" /etc/modules|wc -l` = 0 ];then
      echo -e "vfio\nvfio_iommu_type1\nvfio_pci\nvfio_virqfd" >> /etc/modules
      echo "$(tput setaf 2)vfio satisfied 已添加VFIO模组 √$(tput sgr 0)"
      else echo "$(tput setaf 2)vfio satisfied 已添加VFIO模组 √$(tput sgr 0)"
    fi

    # blacklist nouveau
    if [ `grep "nouveau" /etc/modprobe.d/blacklist.conf|wc -l` = 0 ];then
      echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf && update-initramfs -u
      echo "$(tput setaf 2)vfio satisfied 已添加VFIO模组 √$(tput sgr 0)"
      else echo "$(tput setaf 2)nouveau satisfied 已屏蔽nouveau驱动 √$(tput sgr 0)"
    fi

    # laptop lid close
    if [ `grep "HandleLidSwitch=ignore" /etc/systemd/logind.conf|wc -l` = 0 ];then
      echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf
    fi

    # remove enterprise repo
    if test -f "/etc/apt/sources.list.d/pve-enterprise.list";then 
      rm /etc/apt/sources.list.d/pve-enterprise.list
    fi

    # add none-enterprise repo
    if [ `grep "pve-no-subscription" /etc/apt/sources.list|wc -l` = 0 ];then
      echo "deb http://download.proxmox.com/debian/pve $pveRepo pve-no-subscription" >> /etc/apt/sources.list
      echo "$(tput setaf 2)repo satisfied 已设置源 √$(tput sgr 0)"
    else echo "$(tput setaf 2)repo satisfied 已设置源 √$(tput sgr 0)"
    fi

    # update
    apt update && apt upgrade -y

    # adding cpu freq quick check
    if [ `grep "alias cpu" ~/.bashrc|wc -l` = 0 ];then
      echo "alias cpu='watch -n1 grep MHz /proc/cpuinfo'" >> ~/.bashrc
    fi

    echo "$(tput setaf 2)=================================================================================="
    echo "Done PVE updated ! --> please reboot 搞定！请重启PVE."
    echo "After reboot, run <unlock> to re-run script.重启后运行unlock命令可启动该脚本"
    echo "You can also run <cpu> to check CPU freq. 重启后可以运行<cpu>快速查看当前CPU频率"
    echo "==================================================================================$(tput setaf 0)"
    tput sgr 0
  }
  if [ $L = "cn" ];then # CN
    if (whiptail --title "同意条款及注意事项" --yes-button "继续" --no-button "返回"  --yesno "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------

    PVE无痛自动化:
    1. 一键无脑更新PVE到最新系统
    2. 添加社区源，开启IOMMU支持，添加VFIO模组，屏蔽nouveau驱动
    3. 适合反复食用，已设置或已安装的程序会自动跳过
    4. 保持良好的网络环境

    " 20 80) then
        runUpdate
    else
        main
    fi
  else # EN
    if (whiptail --title "Agreement" --yes-button "Continue" --no-button "Go Back"  --yesno "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    You are responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    ----------------------------------------------------------------

    PVE Automation:
    1. Auto run apt updating PVE to latest
    2. Adding community repo, VFIO modules, blacklist nouveau
    3. Enable IOMMU depends on hardware
    
    " 20 80) then
        runUpdate
    else
        main
    fi
  fi
  tput sgr 0
}

startUnlock(){

  runUnlock(){

    cd /root/
    # install apps
    echo "======================================================================="
    echo "$(tput setaf 2)installing apps... 正在安装必要软件$(tput sgr 0)"
    echo "======================================================================="
    for app in build-essential dkms pve-headers git python3-pip jq
    do
      if [ `dpkg -s $app|grep Status|wc -l` = 1 ]; then echo "$(tput setaf 2)√ Already installed $app! 已安装$app$(tput sgr 0)"
      else 
        echo "$(tput setaf 1)× You don't have $app install! 未安装$app$(tput sgr 0)"
        echo "$(tput setaf 2)installing $app! 正在安装$app$(tput sgr 0)"
        apt install -y $app
        echo "$(tput setaf 2)√ Done $app installed! 已安装$app$(tput sgr 0)"
      fi
    done

    if [ `pip3 install frida|wc -l` = 2 ]; then echo "$(tput setaf 2)√ Already installed $app! 已安装frida$(tput sgr 0)"
    else 
    pip3 install frida
    echo "$(tput setaf 2)installing $app! 正在安装$app$(tput sgr 0)"
    fi

    # install vgpu_unlock
    echo "======================================================================="
    echo "$(tput setaf 2)unlocking... 正在解锁$(tput sgr 0)"
    echo "======================================================================="
    cd /root
    if [ ! -d "/root/vgpu_unlock" ];then 
    git clone https://github.com/DualCoder/vgpu_unlock.git && chmod -R +x /root/vgpu_unlock/
    sed -i 's#if 0#if 1#' /root/vgpu_unlock/vgpu_unlock_hooks.c # enable debug for detail log
    else echo "$(tput setaf 2)√ Done cloned unlock! 已下载unlock$(tput sgr 0)"
    fi

    # Install driver for pve6.4
    echo "======================================================================="
    echo "$(tput setaf 2)installing Nvidia $driver Driver... 正在安装原版$driver显卡驱动程序$(tput sgr 0)"
    echo "======================================================================="
    cd /root/
    if [ ! -x /usr/bin/nvidia-smi ];then
    if test -f "NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run";then
    chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
    /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run --dkms
    else
    wget https://f000.backblazeb2.com/file/vGPUdrivers/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run -P /root/
    chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
    /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run --dkms
    fi
    fi

    # select python version
    if [ $1 = 'python' ];then
    # modify driver
    if [ `grep "vgpu_unlock_hooks.c" /usr/src/nvidia-$driver/nvidia/os-interface.c|wc -l` = 0 ];then
        sed -i '20a#include "/root/vgpu_unlock/vgpu_unlock_hooks.c"' /usr/src/nvidia-$driver/nvidia/os-interface.c
    fi
    if [ `grep "kern.ld" /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild|wc -l` = 0 ];then
        echo "ldflags-y += -T /root/vgpu_unlock/kern.ld" >> /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild
    fi
    if [ `grep "vgpu_unlock" /lib/systemd/system/nvidia-vgpud.service|wc -l` = 0 ];then
        sed -i 's#ExecStart=#ExecStart=/root/vgpu_unlock/vgpu_unlock #' /lib/systemd/system/nvidia-vgpud.service
    fi
    if [ `grep "vgpu_unlock" /lib/systemd/system/nvidia-vgpu-mgr.service|wc -l` = 0 ];then
        sed -i 's#ExecStart=#ExecStart=/root/vgpu_unlock/vgpu_unlock #' /lib/systemd/system/nvidia-vgpu-mgr.service
    fi
    fi

    # select c version
    if [ $1 = 'cversion' ];then
    # building c version
    cd /root
    wget https://gist.githubusercontent.com/HiFiPhile/b3267ce1e93f15642ce3943db6e60776/raw/6482897a3b15e4ada56e37afbb5d1f1ca37b9692/cvgpu.c -P /root/
    gcc -fPIC -shared -o cvgpu.o cvgpu.c
    rm cvgpu.c

    # modify driver
    if [ `grep "vgpu_unlock_hooks.c" /usr/src/nvidia-$driver/nvidia/os-interface.c|wc -l` = 0 ];then
        sed -i '20a#include "/root/vgpu_unlock/vgpu_unlock_hooks.c"' /usr/src/nvidia-$driver/nvidia/os-interface.c
    fi
    if [ `grep "kern.ld" /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild|wc -l` = 0 ];then
        echo "ldflags-y += -T /root/vgpu_unlock/kern.ld" >> /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild
    fi

    # adding LD_PRELOAD env to services
    if [ `grep "LD_PRELOAD" /lib/systemd/system/nvidia-vgpud.service|wc -l` = 0 ];then
        sed -i '14aEnvironment="LD_PRELOAD=/root/cvgpu.o"' /lib/systemd/system/nvidia-vgpud.service
    fi
    if [ `grep "LD_PRELOAD" /lib/systemd/system/nvidia-vgpu-mgr.service|wc -l` = 0 ];then
        sed -i '14aEnvironment="LD_PRELOAD=/root/cvgpu.o"' /lib/systemd/system/nvidia-vgpu-mgr.service
    fi
    fi

    # reaload daemon
    systemctl daemon-reload

    # remove and reinstall driver
    echo "======================================================================="
    echo "$(tput setaf 2)reconfiguring driver... 正在重新构建驱动$(tput sgr 0)"
    echo "======================================================================="
    dkms remove  -m nvidia -v $driver --all
    dkms install -m nvidia -v $driver

    # install mdev
    echo "======================================================================="
    echo "$(tput setaf 2)installing mdev... 正在安装mdev设备$(tput sgr 0)"
    echo "======================================================================="
    cd /root
    if [ -x /usr/sbin/mdevctl ];then echo "$(tput setaf 2)√ mdev installed! 已安装mdev$(tput sgr 0)"
    else
    wget https://github.com/mdevctl/mdevctl/archive/refs/tags/0.81.tar.gz
    tar -zxvf 0.81.tar.gz && rm 0.81.tar.gz
    cd mdevctl-0.81
    make install
    fi

  # adding 11 uuid to env
  if [ `grep "AAA" /etc/environment|wc -l` = 0 ];then
    cat <<EOF >> /etc/environment
export AAA=$AAA
export BBB=$BBB
export CCC=$CCC
export DDD=$DDD
export EEE=$EEE
export FFF=$FFF
export GGG=$GGG
export HHH=$HHH
export III=$III
export JJJ=$JJJ
export KKK=$KKK
EOF
    else echo "$(tput setaf 2)√ Done setup 11 UUID env! 已添加总共11个UUID环境变量$(tput sgr 0)"
    fi

    # adding vgpu command
    if [ `grep "vgpu" ~/.bashrc|wc -l` = 0 ];then
      echo "alias vgpu='/root/vgpu_unlock/scripts/vgpu-name.sh -p ALL'" >> ~/.bashrc
    else echo "$(tput setaf 2)√ Done vgpu command! 已设置vgpu命令$(tput sgr 0)"
    fi

    echo "======================================================================="
    echo "$(tput setaf 2)Done! Please reboot!"
    echo "after reboot, you can run <vgpu> to list all support vgpu"
    echo "搞定！请重启PVE！重启后可以运行vgpu查看所有支持的型号"
    echo "                                                                 by ksh"
    echo "======================================================================="
    tput sgr 0
}

  unlockMenu(){
      if [ $L = "cn" ];then # CN
      OPTION=$(whiptail --title "选择相应版本的解锁方式" --menu "
      采用C版本将获得更快的性能，python版本则有更好的兼容性
      如无其他特殊需求，推荐使用C版本解锁，PY解锁仅用于测试兼容
      请选择配置，回车执行：" 15 80 5 \
      "a" "C版本 --------- vGPU解锁" \
      "b" "Python版本 ---- vGPU解锁" \
      "q" "返回主菜单" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) runUnlock cversion;;
      b ) runUnlock python;;
      q ) main;;
      esac
      tput sgr 0
    else # EN
      OPTION=$(whiptail --title "Select option to Unlock" --menu "
      C version is a bit faster than Python version
      C version also use less memory usage than Python
      Choose your prefer option: " 15 80 5 \
      "a" "C Version ------- Faster, less memory" \
      "b" "Python version -- Compatibility" \
      "q" "Go Back to Main Menu" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) runUnlock cversion;;
      b ) runUnlock python;;
      q ) main;;
      esac
      tput sgr 0
    fi
  }

  uninstallCurrent(){
    if [ $L = "cn" ];then # CN
      if (whiptail --title "卸载当前驱动" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将卸载当前系统已安装的Nvidia驱动，并恢复到无vGPU驱动状态" 10 80) then
        nvidia-uninstall
        rm -r /usr/src/nvidia-*
        echo "$(tput setaf 2)搞定！请重新运行unlock选择你希望安装的版本！$(tput setaf 0)"
      else selectVersion
      fi
    else # EN
      if (whiptail --title "Uninstall Current Driver" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script will auto uninstall everything related to vGPU driver!" 10 80) then
        nvidia-uninstall
        rm -r /usr/src/nvidia-*
        echo "$(tput setaf 2)Done! Please run unlock to select other drivers for installation! $(tput setaf 0)"
      else selectVersion
      fi
    fi
  }

  pve7install(){

        # wget https://raw.githubusercontent.com/igormp/vgpu_unlock/patch_install/vgpu_unlock.patch -P /root/vgpu_unlock/
        # chmod +x /root/vgpu_unlock/vgpu_unlock.patch
        # sed -i 's#opt#root#g' /root/vgpu_unlock/vgpu_unlock.patch

        echo "============================================================================================="
        echo "$(tput setaf 2)installing Nvidia 460.32.04 Driver... 正在安装460.32.04显卡驱动程序$(tput sgr 0)"
        echo "============================================================================================="
        cd /root/
        if [ ! -x /usr/bin/nvidia-smi ];then
            if test -f "/root/vgpu_unlock/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run";then
            chmod +x /root/vgpu_unlock/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run
            /root/vgpu_unlock/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run --dkms
            else
            wget https://f000.backblazeb2.com/file/vGPUdrivers/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run -P /root/vgpu_unlock/
            chmod +x /root/vgpu_unlock/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run
            /root/vgpu_unlock/NVIDIA-Linux-x86_64-460.32.04-vgpu-kvm-custom.run --dkms
            fi
        fi

        # install mdev
        echo "======================================================================="
        echo "$(tput setaf 2)installing mdev... 正在安装mdev设备$(tput sgr 0)"
        echo "======================================================================="
        cd /root
        if [ -x /usr/sbin/mdevctl ];then echo "$(tput setaf 2)√ mdev installed! 已安装mdev$(tput sgr 0)"
        else
        wget https://github.com/mdevctl/mdevctl/archive/refs/tags/0.81.tar.gz
        tar -zxvf 0.81.tar.gz && rm 0.81.tar.gz
        cd mdevctl-0.81
        make install
        fi

        # adding 11 uuid to env
  if [ `grep "AAA" /etc/environment|wc -l` = 0 ];then
    cat <<EOF >> /etc/environment
export AAA=$AAA
export BBB=$BBB
export CCC=$CCC
export DDD=$DDD
export EEE=$EEE
export FFF=$FFF
export GGG=$GGG
export HHH=$HHH
export III=$III
export JJJ=$JJJ
export KKK=$KKK
EOF
    else echo "$(tput setaf 2)√ Done setup 11 UUID env! 已添加总共11个UUID环境变量$(tput sgr 0)"
    fi

    # adding vgpu command
    if [ `grep "vgpu" ~/.bashrc|wc -l` = 0 ];then
      echo "alias vgpu='/root/vgpu_unlock/scripts/vgpu-name.sh -p ALL'" >> ~/.bashrc
    else echo "$(tput setaf 2)√ Done vgpu command! 已设置vgpu命令$(tput sgr 0)"
    fi

    echo "======================================================================="
    echo "$(tput setaf 2)Done! Please reboot!"
    echo "after reboot, you can run <vgpu> to list all support vgpu"
    echo "搞定！请重启PVE！重启后可以运行vgpu查看所有支持的型号"
    echo "                                                                 by ksh"
    echo "======================================================================="
    tput sgr 0
  }

  selectVersion(){
    if [ $L = "cn" ];then # CN
      OPTION=$(whiptail --title "选择驱动版本" --menu "
      根据需求选择相应宿主机驱动版本
      PVE6请选a-e选项，PVE7请选p选项，回车执行：" 19 70 8 \
      "a" "PVE6 450.124 ------- PVE6 最稳定高性能 推荐" \
      "b" "PVE6 450.80 -------- PVE6 性能中规中矩" \
      "c" "PVE6 450.89 -------- PVE6 性能中规中矩" \
      "d" "PVE6 460.32.04 ----- PVE6 受支持的460分支版本" \
      "e" "PVE6 460.73.02 ----- PVE6 受支持的460分支最新版本" \
      "p" "PVE7 460.32.04 ----- 仅适用于PVE7及以上" \
      "u" "Uninstall Current -- 卸载已安装的驱动" \
      "q" "Main Menu ---------- 返回主菜单" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) driver="450.124"
          unlockMenu;;
      b ) driver="450.80"
          unlockMenu;;
      c ) driver="450.89"
          unlockMenu;;
      d ) driver="460.32.04"
          unlockMenu;;
      e ) driver="460.73.02"
          unlockMenu;;
      p ) driver="pve7"
          pve7install;;
      u ) uninstallCurrent;;
      q ) main;;
      esac
    else # EN
      OPTION=$(whiptail --title "Select driver version" --menu "
      Select drivers suit for your current PVE
      For PVE6 select a-e option, for PVE7 select p option" 19 70 8 \
      "a" "PVE6 450.124 --------- Most stable on PVE6.4" \
      "b" "PVE6 450.80 ---------- Alternative version" \
      "c" "PVE6 450.89 ---------- Alternative version" \
      "d" "PVE6 460.32.04 ------- 460 version" \
      "e" "PVE6 460.73.02 ------- 460 latest version" \
      "p" "PVE7 460.32.04 ------- Only works on PVE7" \
      "u" "Uninstall ------------ Uninstall current" \
      "q" "Main Menu ------------ Go Back to Main Menu" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) driver="450.124"
          unlockMenu;;
      b ) driver="450.80"
          unlockMenu;;
      c ) driver="450.89"
          unlockMenu;;
      d ) driver="460.32.04"
          unlockMenu;;
      e ) driver="460.73.02"
          unlockMenu;;
      p ) driver="pve7"
          pve7install;;
      u ) uninstallCurrent;;
      q ) main;;
      esac
    fi
  }

  if [ $L = "cn" ];then # CN
    if (whiptail --title "同意条款及注意事项" --yes-button "同意" --no-button "返回"  --yesno "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------
    1. 此脚本需主板和CPU均支持vt-d指令集，并且已开启
    2. 使用超微，戴尔等双路主板时，需关闭above4G选项
    3. 仅支持9，10，20系N卡！
    4. 使用6，7，8系，30系N卡，yes全系A卡的小伙伴请勿手抖运行！
    5. 仅支持单卡解锁，不支持不同型号的多卡混插
    6. 保持良好的网络环境

    请认真阅读以上条款，同意回车继续，不同意请退出" 22 80) then selectVersion
    else main
    fi
    else # EN
    if (whiptail --title "Agreement" --yes-button "I Agree" --no-button "Go Back"  --yesno "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    I'll not take responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    Credits to vgpu_unlock github that make this happen.
    ----------------------------------------------------------------
    1. Motherboard needs to support at least vt-d
    2. For dual sockets make sure to turn off Above4G in bios
    3. Only support 9, 10, 20 series
    4. Do not run this script on 6, 7, 8, 30 series
    5. Support only single card unlock
    6. Does not support mixed cards with different type

    Agree to continue, disagree to go back to main menu" 22 80) then selectVersion
    else main
    fi
  fi
  tput sgr 0
}

checkStatus(){

  # ref
  # nvidia-smi --query-gpu=timestamp,name,pci.bus_id,driver_version,pstate,pcie.link.gen.max,
  # pcie.link.gen.current,temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used --format=csv

  memory=$(nvidia-smi --query-gpu=memory.total --format=csv | awk '/^memory/ {getline; print}' | awk '{print $1}')

  # check if currently has sliced mdev, if returns a list, then no sliced mdev
  if [ ! `$vgpuScriptPath -p ALL | grep -w "$(mdevctl list | grep -m1 nvidia | awk '{print $3}')" | wc -l` = 1 ];then
    currentType=0
    Vnum=0
    Vmemory=$(($memory / 1000))
    float=0
  else
    currentType=$($vgpuScriptPath -p ALL | grep -w "$(mdevctl list | grep -m1 nvidia | awk '{print $3}')")
    Vnum=$(echo "$currentType" | grep -o '[[:digit:]]*' | sed -n '2p')
    Vmemory=$(($memory / 1000))
    float=$(($Vmemory / $Vnum))
  fi

  # check which VM has vgpu
  clear
  echo "Checking status... 正在检测，请稍等片刻"
  echo -n "">lsvm
  qm list|sed '1d'|awk '{print $1}'|while read line ; 
  do 
  if [ ! `qm config $line | grep -E 'Quadro uuid|vGPU added' | wc -l` = 0 ];then
    echo $(qm config $line | grep 'name:' | awk '{print $2}') ID:$line >> lsvm
    echo "$line"
  fi
  done
  hasVGPU=`cat lsvm`
  rm lsvm

  # check currently which unlock option
  if [ -f /etc/systemd/system/mdev-startup.service ]; then unlockType="Quadro"
  else unlockType="vGPU"
  fi

  if [[ $L = "cn" ]];then # CN
  echo "$(tput setaf 2)=====================================================================
- 物理显卡参数
驱动：$(nvidia-smi --query-gpu=driver_version --format=csv | sed -n '2p')
型号：$(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')
总线：$(nvidia-smi --query-gpu=gpu_bus_id --format=csv | sed -n '2p')
温度：$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
功耗：$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
显存：$memory兆

- 切分小贴士
1）当切分为1G显存时，可同时运行$(($Vmemory / 1))台VM虚拟机
2）当切分为2G显存时，可同时运行$(($Vmemory / 2))台VM虚拟机
3）当切分为3G显存时，可同时运行$(($Vmemory / 3))台VM虚拟机
4）当切分为4G显存时，可同时运行$(($Vmemory / 4))台VM虚拟机
5）当切分为6G显存时，可同时运行$(($Vmemory / 6))台VM虚拟机
6）当切分为8G显存时，可同时运行$(($Vmemory / 8))台VM虚拟机

- win10虚拟机下驱动版本建议
1）性能最理想的版本：443.18 443.66 446.14等，一切44x版本均可
2）如需使用专业软件：避开一切45x，46x，47x版本
3）如需纯游戏的：所有版本均有良好的游戏性能

- 当前切分状态
切分型号：$currentType
切分显存："$Vnum"G
解锁类型：$unlockType
可供使用的vGPU数量：$float个

- 以下VM正在使用切分：
$hasVGPU
                                                              -- by ksh
=======================================================================$(tput sgr 0)"
else # EN
echo "$(tput setaf 2)=====================================================================
- Graphics Card
Version: $(nvidia-smi --query-gpu=driver_version --format=csv | sed -n '2p')
Type: $(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')
BusID: $(nvidia-smi --query-gpu=gpu_bus_id --format=csv | sed -n '2p')
Temp: $(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
Power: $(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
Vram: $memory Mib

- Slice Tips
1) When slicing to 1G Vram, it can run up to $(($Vmemory / 1)) VM simultaneously
2) When slicing to 2G Vram, it can run up to $(($Vmemory / 2)) VM simultaneously
3) When slicing to 3G Vram, it can run up to $(($Vmemory / 3)) VM simultaneously
4) When slicing to 4G Vram, it can run up to $(($Vmemory / 4)) VM simultaneously
5) When slicing to 6G Vram, it can run up to $(($Vmemory / 6)) VM simultaneously
6) When slicing to 8G Vram, it can run up to $(($Vmemory / 8)) VM simultaneously

- vGPU slicing status
Sliced type: $currentType
Sliced vRam: "$Vnum"G
Unlock type: $unlockType
Current Available Count: $float

- Which VM has vGPU
$hasVGPU
                                                              -- by ksh
=======================================================================$(tput sgr 0)"
fi
}

chVram(){
  startVramSlice(){
    vxQ="$($vgpuScriptPath -p ALL | grep -e -"$selectVram"Q | awk '{print $3}')"
    memory=$(nvidia-smi --query-gpu=memory.total --format=csv | awk '/^memory/ {getline; print}' | awk '{print $1}')
    killvgpu(){
      mdevctl stop -u $AAA
      mdevctl stop -u $BBB
      mdevctl stop -u $CCC
      mdevctl stop -u $DDD
      mdevctl stop -u $EEE
      mdevctl stop -u $FFF
      mdevctl stop -u $GGG
      mdevctl stop -u $HHH
      mdevctl stop -u $III
      mdevctl stop -u $JJJ
      mdevctl stop -u $KKK
      OUTPUT=$(mdevctl list)
      echo "Released mdev devices 重新释放所有mdev设备"
    }

    clear
    killvgpu

    mdevctl start -u $AAA -p 0000:$PCI --type $vxQ
    mdevctl start -u $BBB -p 0000:$PCI --type $vxQ
    mdevctl start -u $CCC -p 0000:$PCI --type $vxQ
    mdevctl start -u $DDD -p 0000:$PCI --type $vxQ
    mdevctl start -u $EEE -p 0000:$PCI --type $vxQ
    mdevctl start -u $FFF -p 0000:$PCI --type $vxQ
    mdevctl start -u $GGG -p 0000:$PCI --type $vxQ
    mdevctl start -u $HHH -p 0000:$PCI --type $vxQ
    mdevctl start -u $III -p 0000:$PCI --type $vxQ
    mdevctl start -u $JJJ -p 0000:$PCI --type $vxQ
    mdevctl start -u $KKK -p 0000:$PCI --type $vxQ

    echo "
    [Unit]
    Description=ksh start mdev devices at system startup
    After=default.target
    [Service]
    Type=oneshot
    ExecStart=mdevctl start -u $AAA -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $BBB -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $CCC -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $DDD -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $EEE -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $FFF -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $GGG -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $HHH -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $III -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $JJJ -p 0000:$PCI --type $vxQ
    ExecStart=mdevctl start -u $KKK -p 0000:$PCI --type $vxQ
    ExecStartPost=/bin/sleep 10
    [Install]
    WantedBy=default.target" > /etc/systemd/system/mdev-startup.service
    systemctl daemon-reload
    systemctl enable mdev-startup.service

    currentType=$($vgpuScriptPath -p ALL | grep -w "$(mdevctl list | grep -m1 nvidia | awk '{print $3}')")
    # Vnum=$(echo "$currentType" | grep -o '[[:digit:]]*' | sed -n '2p')
    Vnum=$($vgpuScriptPath -p ALL | grep -e -"$selectVram"Q | grep -o '[[:digit:]]*' | sed -n '2p')
    Vmemory=$(($memory / 1000))
    float=$(($Vmemory / $Vnum))

    echo "$(tput setaf 2)
===================================================================
物理显存: $memory兆
当前切分状态:
切分型号: $currentType
当前vGPU显存为"$Vnum"G，可供使用的vGPU数量为$float个

TotalVram: $memory Mib
Slicing Status:
vGPU Type: $currentType
Current vGPU vRAM is "$Vnum"G, available vGPU count is $float
===================================================================$(tput sgr 0)"
}

  if [ $L = "cn" ];then # CN
    selectVram=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "选择配置，回车执行：" 16 60 7 \
    "a" "切分为1G显存" \
    "b" "切分为2G显存" \
    "c" "切分为3G显存" \
    "d" "切分为4G显存" \
    "e" "切分为6G显存" \
    "f" "切分为8G显存" \
    "q" "回主界面" \
    3>&1 1>&2 2>&3)
    case "$selectVram" in
    a )	selectVram=1
        startVramSlice
      ;;

    b ) selectVram=2
        startVramSlice
      ;;

    c ) selectVram=3
        startVramSlice
      ;;

    d ) selectVram=4
        startVramSlice
      ;;

    e ) selectVram=6
        startVramSlice
      ;;

    f ) selectVram=8
        startVramSlice
      ;;

    q ) main;;
    esac

  else # EN

    selectVram=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "select options: " 16 60 7 \
    "a" "slice to 1G vRam" \
    "b" "slice to 2G vRam" \
    "c" "slice to 3G vRam" \
    "d" "slice to 4G vRam" \
    "e" "slice to 6G vRam" \
    "f" "slice to 8G vRam" \
    "q" "Go back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$selectVram" in
    a )	selectVram=1
        startVramSlice
      ;;

    b ) selectVram=2
        startVramSlice
      ;;

    c ) selectVram=3
        startVramSlice
      ;;

    d ) selectVram=4
        startVramSlice
      ;;

    e ) selectVram=6
        startVramSlice
      ;;

    f ) selectVram=8
        startVramSlice
      ;;

    q ) main;;
    esac

  fi
}

deployQuadro(){

  runQuadro(){
    IDofTU="1EB1" #RTX4000
    SubIDofTU="12A0"

    IDofGP="1B30" #P6000
    SubIDofGP="11A0"

    IDofGM="13F0" #M5000
    SubIDofGM="1152"

    # check RTX?
    checkGPU=$(nvidia-smi --query-gpu=gpu_name --format=csv | grep -i geforce | head -n1)

    # delete any vgpu uuid conf if exist
    sed -i '/args: -uuid/d' /etc/pve/qemu-server/$vmid.conf

    # modify vm conf depends on gpu architecture
    if [ `grep -E "$AAA|$BBB|$CCC|$DDD|$EEE|$FFF|$GGG|$HHH|$III|$JJJ|$KKK" /etc/pve/qemu-server/$vmid.conf|wc -l` = 0 ]; then
      # if GP pascal
      if [ ! `lspci | grep GP | wc -l` = 0 ] || [[ $checkGPU =~ "10"* ]]; then
        if [ $uuidnumb = 1 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$AAA,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $AAA" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 2 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$BBB,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $BBB" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 3 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$CCC,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $CCC" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 4 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$DDD,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $DDD" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 5 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$EEE,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $EEE" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 6 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$FFF,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $FFF" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 7 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$GGG,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $GGG" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 8 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$HHH,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $HHH" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 9 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$III,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $III" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 10 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$JJJ,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $JJJ" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 11 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$KKK,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGP,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGP' -uuid $KKK" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

      
      fi

      # if TU turling  
      if [ ! `lspci | grep TU | wc -l` = 0 ] || [[ $checkGPU =~ "20"* ]] || [[ $checkGPU =~ "16"* ]]; then

        sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
        sed -i '/-uuid/d' /etc/pve/qemu-server/$vmid.conf

        if [ $uuidnumb = 1 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$AAA,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $AAA" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 2 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$BBB,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $BBB" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 3 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$CCC,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $CCC" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 4 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$DDD,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $DDD" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 5 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$EEE,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $EEE" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 6 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$FFF,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $FFF" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 7 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$GGG,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $GGG" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 8 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$HHH,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $HHH" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 9 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$III,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $III" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 10 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$JJJ,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $JJJ" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 11 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$KKK,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofTU,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofTU' -uuid $KKK" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

      
      fi

      # if GM maxwell
      if [ ! `lspci | grep GM | wc -l` = 0 ]; then
        if [ $uuidnumb = 1 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$AAA,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $AAA" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 2 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$BBB,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $BBB" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 3 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$CCC,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $CCC" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 4 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$DDD,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $DDD" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 5 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$EEE,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $EEE" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 6 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$FFF,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $FFF" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 7 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$GGG,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $GGG" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 8 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$HHH,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $HHH" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 9 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$III,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $III" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 10 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$JJJ,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $JJJ" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi

        if [ $uuidnumb = 11 ]; then
        sed -r -i "1i args: -device 'vfio-pci,sysfsdev=/sys/bus/mdev/devices/$KKK,display=off,id=hostpci0.0,bus=ich9-pcie-port-1,addr=0x0.0,x-pci-vendor-id=0x10de,x-pci-device-id=0x$IDofGM,x-pci-sub-vendor-id=0x10de,x-pci-sub-device-id=0x$SubIDofGM' -uuid $KKK" /etc/pve/qemu-server/$vmid.conf
        echo "$(tput setaf 2)Done modified $vmid! 已完成虚拟机ID为$vmid的Quadro显卡直通！$(tput setaf 0)"
        fi


      fi

      # add comments
      sed -r -i "1i #Quadro uuid=$uuidnumb" /etc/pve/qemu-server/$vmid.conf

      else
      echo "$(tput setaf 1)$vmid already has a Quadro! Skip it! $(tput setaf 0)"
      echo "$(tput setaf 1)虚拟机ID$vmid已存在Quadro显卡，已跳过！ $(tput setaf 0)"
    fi
  }

  typeuuid(){
    clear
    uuidnumb=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
    Choose Defined UUID between 1 to 11:
    请选择预设的11个UUID其中一个，建议按顺序选择：" 22 60 12 \
    "1" "Preset UUID 1 - 预设1"\
    "2" "Preset UUID 2 - 预设2"\
    "3" "Preset UUID 3 - 预设3"\
    "4" "Preset UUID 4 - 预设4"\
    "5" "Preset UUID 5 - 预设5"\
    "6" "Preset UUID 6 - 预设6"\
    "7" "Preset UUID 7 - 预设7"\
    "8" "Preset UUID 8 - 预设8"\
    "9" "Preset UUID 9 - 预设9"\
    "10" "Preset UUID 10 - 预设10"\
    "11" "Preset UUID 11 - 预设11"\
    3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      if [ "$uuidnumb" -le 11 -a "$vmid" -ge 1 ]; then runQuadro
      else
      whiptail --title "Warnning" --msgbox "Invalid UUID. Choose between 1-11! 请重新输入1-11范围内的数字！" 10 60
      typeuuid
      fi
    fi
  }

  if [ $L = "cn" ];then # CN
    if (whiptail --title "同意条款及注意事项" --yes-button "同意" --no-button "返回"  --yesno "
    此脚本将自动部署相对应架构的专业卡到虚拟机

    - 如为9系，则自动解锁为M5000专业显卡
    - 如为10系，则自动解锁为P6000专业显卡
    - 如为20系，则自动解锁为RTX4000专业显卡

    请注意：该脚本不支持6,7,8系和30系物理显卡" 15 80) then

    # select VM's ID
    clear
    list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
    echo -n "">lsvm
    ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
    ls=`cat lsvm`
    rm lsvm
    h=`echo $ls|wc -l`
    let h=$h*1
    if [ $h -lt 30 ];then
        h=30
    fi
    list1=`echo $list|awk 'NR>1{print $1}'`
    vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
    选择虚拟机：" 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then typeuuid
        else 
        whiptail --title "Warnning" --msgbox "请重新输入100-999范围内的数字！" 10 60
        deployQuadro
        fi
    fi
  else main
  fi

  else # EN
    if (whiptail --title "Notice" --yes-button "I Agree" --no-button "Go Back"  --yesno "
    Script auto detect graphics card on motherboard
    Then passthrough with appropriate Quadro

    - 9 series unlock to a M4000 Quadro
    - 10 series unlock to a P6000 Quadro
    - 20 series unlock to a RTX4000 Quadro

    Please be aware, 6,7,8 and 30 series are not supported" 15 80) then

    # select VM's ID
    clear
    list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
    echo -n "">lsvm
    ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
    ls=`cat lsvm`
    rm lsvm
    h=`echo $ls|wc -l`
    let h=$h*1
    if [ $h -lt 30 ];then
        h=30
    fi
    list1=`echo $list|awk 'NR>1{print $1}'`
    vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
    Choose a VM: " 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then typeuuid
        else 
        whiptail --title "Warnning" --msgbox "Invalid VM ID. Choose between 100-999!" 10 60
        deployQuadro
        fi
    fi

    else main
    fi

  
  fi
}

QuadroMenu(){
  if [[ $L = "cn" ]];then # CN

    OPTION=$(whiptail --title "同意条款及注意事项" --menu "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------
    Quadro切分无需授权，解锁除CUDA之外的所有功能
    自动记忆上一次切分，无需重复设置！需要重分配显存时，请重复执行步骤a
    ！！！注意：请停止所有VM再执行命令！！！
    ！！！注意：请停止所有VM再执行命令！！!
    ！！！注意：请停止所有VM再执行命令！！！" 22 80 3 \
    "a" "切分（重切分）显存" \
    "b" "部署已切分的显卡到虚拟机" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) chVram;;
    b ) deployQuadro;;
    q ) tput sgr 0
      main;;
    esac
    tput sgr 0

  else # EN

    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    I'll not take responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    Credits to vgpu_unlock github that make this happen.
    ----------------------------------------------------------------
    Option1 unlocks everything except for CUDA, doesn't require license
    Run step a) first, then run step b)
    Please STOP all VM before running this script !!!
    Please STOP all VM before running this script !!!
    Please STOP all VM before running this script !!!" 22 80 3 \
    "a" "Slice(Re-Slice) VRam profile" \
    "b" "Deploy a sliced Quadro to VM" \
    "q" "Go back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) chVram;;
    b ) deployQuadro;;
    q ) tput sgr 0
      main;;
    esac
    tput sgr 0

  fi
}

deployvGPU(){

  vGPUassign(){

    # delete any quadro conf if exist
    sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/args: -uuid/d' /etc/pve/qemu-server/$vmid.conf

    # delete comments for quadro and vgpu
    sed -i '/Quadro/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/vGPU added/d' /etc/pve/qemu-server/$vmid.conf

    # stop all mdevs if exist
    qm list|sed '1d'|awk '{print $1}'|while read line ; 
    do 
      if [ `grep -E "mdev=" /etc/pve/qemu-server/*.conf|wc -l` = 1 ];then
        mdevctl stop -u 00000000-0000-0000-0000-000000000$line
      fi;
    done

    # adding mdev
    # sed -i -r "1i hostpci0: $PCI,mdev=$vGPUtype" /etc/pve/qemu-server/$vmid.conf

    # adding uuid to conf
    sed -i -r "1i args: -uuid 00000000-0000-0000-0000-000000000$vmid" /etc/pve/qemu-server/$vmid.conf
    sed -r -i "1i #vGPU added" /etc/pve/qemu-server/$vmid.conf

    echo "$(tput setaf 2)vGPU assigned! Go to PVE webGui->VM$vmid->Hardward->add PCI device->choose your desire vGPU type$(tput setaf 0)"
    echo "$(tput setaf 2)设置完毕！请到网页端->虚拟机$vmid->硬件->PCI设备->添加你希望的类型$(tput setaf 0)"
  }

  # select VM's ID
  clear
  list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
  echo -n "">lsvm
  ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
  ls=`cat lsvm`
  rm lsvm
  h=`echo $ls|wc -l`
  let h=$h*1
  if [ $h -lt 30 ];then
      h=30
  fi
  list1=`echo $list|awk 'NR>1{print $1}'`
  vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
  Choose a VM: 
  选择虚拟机：" 20 60 10 \
  $(echo $ls) \
  3>&1 1>&2 2>&3)

  exitstatus=$?
  if [ $exitstatus = 0 ]; then
    if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then vGPUassign
    else 
    whiptail --title "Warnning" --msgbox "
    Invalid VM ID. Choose between 100-999!
    请重新输入100-999范围内的数字！" 10 60
    deployvGPU
    fi
  fi
}

vGPUMenu(){
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title "同意条款及注意事项" --menu "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------
    - vGPU将解锁所有功能，即完整的CUDA和OpenCL
    - vGPU切分需要购买正版授权，且需部署一台虚拟机授权服务器

    ！！！注意：请停止所有VM再执行命令！！！" 21 80 3 \
    "a" "添加vGPU到虚拟机" \
    "b" "创建授权服务器VM" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) deployvGPU;;
    b ) setupLXC;;
    q ) tput sgr 0
      main;;
    esac
    tput sgr 0
  else # EN
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    I'll not take responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    Credits to vgpu_unlock github that make this happen.
    ----------------------------------------------------------------
    - vGPU unlocks everything, including CUDA + OpenCL
    - You need to deploy a license-VM in order to use vGPU profile

    Please STOP all VM before running this script !!!" 21 80 3 \
    "a" "Deploy vGPU profile to a VM" \
    "b" "Deploy a VM for vGPU license server" \
    "q" "Go back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) deployvGPU;;
    b ) setupLXC;;
    q ) tput sgr 0
      main;;
    esac
    tput sgr 0
  fi
}

setupLXC(){
  if [ $L = "cn" ];then
    if (whiptail --title "LXC CentOS 7.9 授权服务器" --yes-button "继续" --no-button "返回"  --yesno "
      默认虚拟机配置：2CPU 2G
      默认IP地址：192.168.1.6
      默认访问网址：http://192.168.1.6:8080/licserver/
      默认终端登陆用户：root 密码：abc12345

      运行后注意事项：
      1）请在PVE网页端修改IP地址为你局域网的网段
      2）运行登陆后务必输入passwd修改默认密码
      3）第一次启动速度较慢，请耐心等待CPU占用接近0时再访问网址
      4）默认开机不启动，但强烈推荐设置VM为开机自启
      5）如遇无法下载，请手动百度云：
      https://pan.baidu.com/s/15TYh5PDfqmcEgwoDEv0aQQ 提取码：rldj
      下载完手动上传到/var/lib/vz/dump/里，注意下载的文件不要重命名！
      上传完毕后，重新运行此脚本即可
      " 23 80) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      vmid=$(whiptail --inputbox "请输入授权服务器的虚拟机ID，默认是100" 8 60 100 --title "输入VM的ID值" 3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then

            if [ ! -x /usr/local/bin/gdown ];then pip3 install gdown; fi
          
            if [ ! -f "/var/lib/vz/dump/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz" ];then
            echo "$(tput setaf 1)远程下载中，文件较大请耐心等候...$(tput setaf 0)"
            gdown https://drive.google.com/uc?id=11xQe_9F_8zKX3WEqE-oq9EnmpdUkWhSU -O /var/lib/vz/dump/
            
            pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
            echo "$(tput setaf 2)搞定，请移步PVE网页端查看！$(tput setaf 0)"
            else
            pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
            echo "$(tput setaf 2)搞定，请移步PVE网页端查看！$(tput setaf 0)"
            fi

          else 
          whiptail --title "Warnning" --msgbox "请重新输入100-999范围内的数字！" 10 60
          setupLXC
          fi
      fi
      else main
    fi
  else # EN
    if (whiptail --title "LXC CentOS 7.9 License Server" --yes-button "Continue" --no-button "Go Back"  --yesno "
    default VM settings: 2 vCPU + 2G ram
    default IP addr: 192.168.1.6
    default Webpage: http://192.168.1.6:8080/licserver/
    default login user: root / password: abc12345

    Notice:
    1) Go to PVE webgui, set IP addr to your local network IP range
    2) Please change root login password when first launch
    3) Slow on first launch, please be patient
    4) It's recommanded to set the VM start on PVE boots
    " 20 80) then

    read storage < <(
        declare -a array=()
        while read id foo{,,,,} dsc;do
        array+=($id "$dsc")
        done < <(pvesm status | sed -n '1!p')
        whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
    )

    vmid=$(whiptail --inputbox "What's the VM id you want to deploy license sever? default is 100" 8 60 100 --title "define VM ID" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then

          if [ ! -x /usr/local/bin/gdown ];then pip3 install gdown; fi

          if [ ! -f "/var/lib/vz/dump/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz" ];then
          echo "$(tput setaf 1)backup not exist, downloading...$(tput setaf 0)"
          gdown https://drive.google.com/uc?id=11xQe_9F_8zKX3WEqE-oq9EnmpdUkWhSU -O /var/lib/vz/dump/
          
          pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
          echo "$(tput setaf 2)Done! Please check webgui! $(tput setaf 0)"
          else
          pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
          echo "$(tput setaf 2)Done! Please check webgui! $(tput setaf 0)"
          fi

        else 
        whiptail --title "Warnning" --msgbox "Invalid VM ID. Choose between 100-999!" 10 60
        setupLXC
        fi
    fi
    else main
    fi
  fi
}

runReset(){
  echo "$(tput setaf 2)Start reset process... 正在重置...$(tput setaf 0)"
  sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/*.conf
  sed -i '/mdev/d' /etc/pve/qemu-server/*.conf
  sed -i '/-uuid/d' /etc/pve/qemu-server/*.conf
  sed -i '/#vGPU added/d' /etc/pve/qemu-server/*.conf
  sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/*.conf
  
  systemctl disable mdev-startup.service
  systemctl daemon-reload
  rm /etc/systemd/system/mdev-startup.service
  
  mdevctl stop -u $AAA
  mdevctl stop -u $BBB
  mdevctl stop -u $CCC
  mdevctl stop -u $DDD
  mdevctl stop -u $EEE
  mdevctl stop -u $FFF
  mdevctl stop -u $GGG
  mdevctl stop -u $HHH
  mdevctl stop -u $III
  mdevctl stop -u $JJJ
  mdevctl stop -u $KKK

  if [ ! `grep -E "mdev=" /etc/pve/qemu-server/*.conf|wc -l` = 0 ];then
    qm list|sed '1d'|awk '{print $1}'|while read line ; do mdevctl stop -u 00000000-0000-0000-0000-000000000$line; done
  fi

  echo "$(tput setaf 2)Done reset! All vGPU resources released! 初始化完成，所有vGPU资源均已释放完毕！$(tput setaf 0)"
}

resetDefaultvGPU(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "初始化状态" --yes-button "继续" --no-button "返回"  --yesno "
    初始化所有相关vGPU和Quadro设置，整个过程无需重启
    或者你希望重新设置切分，此脚本将恢复到最初状态

    1）释放所有mdev设备
    2）删除所有跟vGPU相关的自启动服务
    3）删除所有虚拟机conf跟vGPU相关的设置
    4）所有虚拟机将恢复成无显卡直通的初始化状态

    " 15 80) then runReset
    else main
    fi
  else # EN
    if (whiptail --title "Reset to default" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Script will auto reset everthing related to vGPU/Quadro
    Script doesn't require reboot after reset process

    1) Release all mdev devices to default
    2) Delete all startup services
    3) Delete all vGPU related settings for all VM's conf
    4) All VM will reset to no-vGPU mode

    " 15 80) then runReset
    else main
    fi
  fi
}

realtimeHW(){
  # ref variable, some not used
  memory=$(nvidia-smi --query-gpu=memory.total --format=csv | awk '/^memory/ {getline; print}' | awk '{print $1}')
  ClkSpeed=$(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p')
  MemSpeed=$(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p')
  GrpSpeed=$(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p')
  MaxMemClk=$(nvidia-smi -q -d CLOCK | grep Memory | sed -n '4p' | awk '{print $3}')
  MaxSMClk=$(nvidia-smi -q -d CLOCK | grep SM | sed -n '3p' | awk '{print $3}')
  fan=$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)
  useWatt=$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
  MaxWatt=$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $7}' | head -n1)
  gpuTemp=$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')
  gpuPerf=$(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  gpuUsage=$(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  gpuMemUse=$(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  gpuMemFree=$(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')
  gpuName=$(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')

  if [[ $L = "cn" ]];then # CN
  while :; do
  echo "
  ======realtime=======
  $gpuName
  温度：$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  性能：$(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  功耗：$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p') / $MaxWatt
  占用：$(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  风扇：$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)

  显存占用
  已用：$(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  空闲：$(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')

  实时频率
  核心：$(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p') / $MaxSMClk MHz
  显存：$(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p') / $MaxMemClk MHz
  图形：$(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p') / $MaxSMClk MHz

  $(nvidia-smi --query-gpu=timestamp --format=csv | sed -n '2p')
  =========ksh========="
  sleep 2
  done

  else # EN

  while :; do
  echo "
  ======realtime=======
  $gpuName
  Temp:  $(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  Perf:  $(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  Power: $(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p') / $MaxWatt
  Usage: $(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  fan:   $(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)

  Vram Usage
  Use:   $(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  Free:  $(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')

  Realtime Clock Speed
  Core:  $(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p') / $MaxSMClk MHz
  Mem:   $(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p') / $MaxMemClk MHz
  Graph: $(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p') / $MaxSMClk MHz

  $(nvidia-smi --query-gpu=timestamp --format=csv | sed -n '2p')
  =========ksh========="
  sleep 2
  done

  fi

}

checkNVlog(){
  whiptail --title "LOG" --scrolltext --msgbox "$(journalctl -r | grep -E 'vgpu|NVRM')" 30 150
  main
}

ulimenu(){

  beautifyLSCAT(){
  echo "======================================================================="
  echo "$(tput setaf 2)step 1 --> installing lsd + bat 正在安装美化程序...$(tput sgr 0)"
  echo "======================================================================="
  wget https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb
  wget https://github.com/sharkdp/bat/releases/download/v0.18.1/bat_0.18.1_amd64.deb
  dpkg -i lsd_0.20.1_amd64.deb && rm lsd_0.20.1_amd64.deb
  dpkg -i bat_0.18.1_amd64.deb && rm bat_0.18.1_amd64.deb
  if [ `grep "lsd" ~/.bashrc|wc -l` = 0 ];then
  cat <<EOF >> ~/.bashrc
alias ls='lsd'
alias l='ls -l'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'
alias cat='bat'
EOF
  else echo "$(tput setaf 2)√ Done lsd installed! 已添加ls环境变量$(tput sgr 0)"
  fi
  echo "======================================================================="
  echo "$(tput setaf 2)Done! Please restart SSH! 搞定！请重启SSH，食用方式：ls，ll，la，l，cat$(tput sgr 0)"
  echo "======================================================================="
  }

  LSCATmenu(){
    if [ $L = "cn" ];then # CN
      if (whiptail --title "安装美化版LS，CAT" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将自动安装及美化LS，CAT命令！完成后请关闭ssh再重新打开
      完成后输入ll，la，ls，lt，cat即可看到效果" 10 80) then beautifyLSCAT
      else ulimenu
      fi
    else # EN
      if (whiptail --title "beautify LS，CAT" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Install lsd and bat to PVE! Need to re-run ssh after installation!
      Type ll,la,ls,lt,cat to see the result!" 10 80) then beautifyLSCAT
      else ulimenu
      fi
    fi
  }

  installBPYTOP(){
    echo "======================================================================="
    echo "$(tput setaf 2)step 2 --> installing bpytop 正在安装bpytop...$(tput sgr 0)"
    echo "======================================================================="
    for app in python3-pip
    do
      if [ `dpkg -s $app|grep Status|wc -l` = 1 ]; then echo "$(tput setaf 2)√ 已安装$app$(tput sgr 0) Done installing $app"
      else 
        apt install -y $app
      fi
    done
    pip3 install bpytop --upgrade

    echo "======================================================================="
    echo "$(tput setaf 2)Done! Please restart SSH! 搞定！请重启SSH，食用方式：bpytop$(tput sgr 0)"
    echo "======================================================================="
  }

  BPYTOPmenu(){
    if [ $L = "cn" ];then # CN
      if (whiptail --title "安装硬件监控" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将自动安装增强版硬件监控，安装完成后执行bpytop查看
      可实时查看cpu，内存，硬盘，频率，温度，占用率等" 10 80) then installBPYTOP
      else ulimenu
      fi
    else # EN
      if (whiptail --title "Install bpytop" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Install bpytop for real time hardware monitoring
      After installation run bpytop to see the result" 10 80) then installBPYTOP
      else ulimenu
      fi
    fi
  }

  diskMenu(){
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    Configure disk passthrough to VM. 设置物理硬盘直通" 12 60 3 \
    "a" "Disk passthrough ----- 添加硬盘直通" \
    "b" "Remove passthrough --- 删除硬盘直通 " \
    "q" "Go back to MainMenu -- 返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) passthroDisk add;;
    b ) passthroDisk rm;;
    q ) ulimenu;;
    esac
  }

  passthroDisk(){ # credits to https://github.com/ivanhao/pvetools
    list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
    echo -n "">lsvm
    ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
    ls=`cat lsvm`
    rm lsvm
    h=`echo $ls|wc -l`
    let h=$h*1
    if [ $h -lt 30 ];then
        h=30
    fi
    list1=`echo $list|awk 'NR>1{print $1}'`
    vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
    Choose vmid to set disk:
    选择需要配置硬盘的vm：" 20 60 10 \
        $(echo $ls) \
        3>&1 1>&2 2>&3)
        exitstatus=$?
        if [ $exitstatus = 0 ]; then
            if(whiptail --title "Yes/No" --yesno "
    you choose: $vmid ,continue?
    你选的是：$vmid ，是否继续?
                " 10 60)then
                while [ true ]
                do
                    if [ `echo "$vmid"|grep "^[0-9]*$"|wc -l` = 0 ];then
                        whiptail --title "Warnning" --msgbox "
    wrong format, please retype:
    输入格式错误，请重新输入：
                        " 10 60
                        ulimenu
                    else
                        break
                    fi
                done
                if [ $1 = 'add' ];then
                    disks=`ls -alh /dev/disk/by-id|sed '/\.$/d'|sed '/^$/d'|awk 'NR>1{print $9" "$11" OFF"}'|sed 's/\.\.\///g'|sed '/wwn/d'|sed '/^dm/d'|sed '/lvm/d'|sed '/nvme-nvme/d'`
                    d=$(whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --checklist "
    disk list:
    已添加的硬盘:
    $(cat /etc/pve/qemu-server/$vmid.conf|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2" "$3}')
    -----------------------
    Choose disk:
    选择硬盘：" 30 90 10 \
                    $(echo $disks) \
                    3>&1 1>&2 2>&3)
                    exitstatus=$?
                    t=$(whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
    Choose disk type:
    选择硬盘接口类型：" 20 60 10 \
                    "sata" "vm sata type" \
                    "scsi" "vm scsi type" \
                    "ide" "vm ide type" \
                    3>&1 1>&2 2>&3)
                    exits=$?
                    if [ $exitstatus = 0 ] && [ $exits = 0 ]; then
                        did=`qm config $vmid|sed -n '/^'$t'/p'|awk -F ':' '{print $1}'|sort -u -r|grep '[0-9]*$' -o|awk 'NR==1{print $0}'`
                        if [ $did ];then
                            did=$((did+1))
                        else
                            did=0
                        fi
                        d=`echo $d|sed 's/\"//g'`
                        for i in $d
                        do
                            if [ `cat /etc/pve/qemu-server/$vmid.conf|grep $i|wc -l` = 0 ];then
                                if [ $t = "ide" ] && [ $did -gt 3 ];then
                                    whiptail --title "Warnning" --msgbox "
    ide is greate then 3, please select other types
    ide的类型已经超过3个,请重选其他类型!" 10 60
                                else
                                    qm set $vmid --$t$did /dev/disk/by-id/$i
                                fi
                                sleep 1
                                did=$((did+1))
                            fi
                        done
                        whiptail --title "Success" --msgbox "Done.
    配置完成" 10 60
                        ulimenu
                    else
                        ulimenu
                    fi
                fi
                if [ $1 = 'rm' ];then
                    disks=`qm config $vmid|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2$3" OFF"}'`
                    d=$(whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --checklist "
    Choose disk:
    选择硬盘：" 20 90 10 \
                    $(echo $disks) \
                    3>&1 1>&2 2>&3)
                    exitstatus=$?
                    if [ $exitstatus = 0 ]; then
                        for i in $d
                        do
                            i=`echo $i|sed 's/\"//g'`
                            qm set $vmid --delete $i
                        done
                        whiptail --title "Success" --msgbox "Done.
    配置完成" 10 60
                        ulimenu
                    else
                        ulimenu
                    fi
                fi
            else
                ulimenu
            fi
        fi

  }

  iommuGroupcheck(){ # credits to https://github.com/pavolelsig/IOMMU-viewer
    if [ $L = "cn" ];then # CN
      if (whiptail --title "IOMMU组 查看" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将自动检测主板IOMMU组设计，方便查看各种直通硬件对应的组编号
      脚本也会罗列出各硬件的ID编号及当前使用的内核驱动" 10 80) then
        GROUP=`find /sys/kernel/iommu_groups/ -type l | cut -d '/' -f 5,7 --output-delimiter='-'`
        for i in $GROUP; do
        k=`echo $i | cut -d '-' -f 1`
        l=`echo $i | cut -d '-' -f 2`
        j=`echo $i | cut -d ':' -f 2,3,4`
        m=`lspci -k -s $j | grep "Kernel driver in use"`		
        echo -n  "IOMMU组："
        if [ $k -lt 10 ]
        then
          echo -n " $k  "
        else
          echo -n " $k "
        fi
        echo -n " $l "
        echo -n "`lspci -nn | grep $j | cut -d ' ' -f 2-`"		
        if ! [ -z "$m" ]
        then
          echo -n "   内核驱动："
        fi	
        echo "$m" | cut -d ':' -f 2
        done | sort -nk2
      else
        ulimenu
      fi
    else # EN
      if (whiptail --title "IOMMU Check" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script lists all IOMMU group based for motherboard design
      Easy to see what's the Hardware ID, what kernel driver being used" 10 80) then
        GROUP=`find /sys/kernel/iommu_groups/ -type l | cut -d '/' -f 5,7 --output-delimiter='-'`
        for i in $GROUP; do
        k=`echo $i | cut -d '-' -f 1`
        l=`echo $i | cut -d '-' -f 2`
        j=`echo $i | cut -d ':' -f 2,3,4`
        m=`lspci -k -s $j | grep "Kernel driver in use"`		
        echo -n  "IommuGroup: "
        if [ $k -lt 10 ]
        then
          echo -n " $k  "
        else
          echo -n " $k "
        fi
        echo -n " $l "
        echo -n "`lspci -nn | grep $j | cut -d ' ' -f 2-`"		
        if ! [ -z "$m" ]
        then
          echo -n "   Driver: "
        fi	
        echo "$m" | cut -d ':' -f 2
        done | sort -nk2
      else
        ulimenu
      fi
    fi
  }

  extraGrubsettings(){ # breaking all iommu by applying pcie_acs_override
    # hugepagesz=2MB nvme_core.io_timeout=2 nvme.poll_queues=12 max_host_mem_size_mb=512 nvme.io_poll=0 nvme.io_poll_delay=0 
    # pcie_acs_override=downstream,multifunction vfio_iommu_type1.allow_unsafe_interrupts=1
    if [ $L = "cn" ];then # CN
      if (whiptail --title "彻底分离IOMMU组" --yes-button "继续" --no-button "返回"  --yesno "
      请谨慎执行该脚本，如果你并未碰到直通困难的硬件，请勿运行该脚本
      当你碰到某硬件无法直通时可尝试执行该脚本，以分离出更合理的IOMMU组" 10 80) then
        if [ `grep "pcie_acs_override" /etc/default/grub | wc -l` = 0 ];then
        sed -i 's#iommu=pt#iommu=pt pcie_acs_override=downstream,multifunction vfio_iommu_type1.allow_unsafe_interrupts=1#' /etc/default/grub && update-grub
        echo "$(tput setaf 2)√ 已开启额外IOMMU参数，请重新启动！$(tput sgr 0)"
        else echo "$(tput setaf 2)√ 已有额外IOMMU参数，无需设置！ √$(tput sgr 0)"
        fi
      else ulimenu
      fi
    else # EN
      if (whiptail --title "Breaking IOMMU" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Apply this script when you need pcie_acs_override
      Run script when you have trouble in hardware passthro" 10 80) then
        if [ `grep "pcie_acs_override" /etc/default/grub | wc -l` = 0 ];then
        sed -i 's#iommu=pt#iommu=pt pcie_acs_override=downstream,multifunction vfio_iommu_type1.allow_unsafe_interrupts=1#' /etc/default/grub && update-grub
        echo "$(tput setaf 2)√ Done pcie_acs_override, please reboot! $(tput sgr 0)"
        else echo "$(tput setaf 2)√ Already have pcie_acs_override enabled, skip it! √$(tput sgr 0)"
        fi
      else ulimenu
      fi
    fi
  }

  unlockFPSmenu(){
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    Configure vGPU FPS unlock to VM. 解除FPS帧率限制" 12 60 3 \
    "a" "Unlock FPS ----------- 解除帧率限制" \
    "b" "Undo FPS ------------- 恢复限制 " \
    "q" "Go back to MainMenu -- 返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) unlockFPS;;
    b ) undoFPS;;
    q ) ulimenu;;
    esac
  }

  unlockFPS(){
    # https://docs.nvidia.com/grid/latest/grid-vgpu-release-notes-generic-linux-kvm/index.html#lower-vgpu-benchmarks
    if [ $L = "cn" ];then # CN
      uuidnumb=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
      注意：取消限制只是临时的，重启宿主机后将恢复60帧限制！
      请选择你希望解锁的UUID预设编号，回车继续！" 24 60 13 \
      "1" "Preset UUID 1 - 预设1"\
      "2" "Preset UUID 2 - 预设2"\
      "3" "Preset UUID 3 - 预设3"\
      "4" "Preset UUID 4 - 预设4"\
      "5" "Preset UUID 5 - 预设5"\
      "6" "Preset UUID 6 - 预设6"\
      "7" "Preset UUID 7 - 预设7"\
      "8" "Preset UUID 8 - 预设8"\
      "9" "Preset UUID 9 - 预设9"\
      "10" "Preset UUID 10 - 预设10"\
      "11" "Preset UUID 11 - 预设11"\
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ $uuidnumb = 1 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$AAA/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 2 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$BBB/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 3 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$CCC/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 4 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$DDD/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 5 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$EEE/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 6 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$FFF/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 7 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$GGG/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 8 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$HHH/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 9 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$III/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 10 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$JJJ/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 11 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$KKK/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已解锁60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
      fi
    else # EN
      uuidnumb=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
      Unlock 60fps limit will be temporary, it recovered when host reboot.
      Select a UUID preset you want to unlock, press enter to cotinue: " 24 80 13 \
      "1" "Preset UUID 1"\
      "2" "Preset UUID 2"\
      "3" "Preset UUID 3"\
      "4" "Preset UUID 4"\
      "5" "Preset UUID 5"\
      "6" "Preset UUID 6"\
      "7" "Preset UUID 7"\
      "8" "Preset UUID 8"\
      "9" "Preset UUID 9"\
      "10" "Preset UUID 10"\
      "11" "Preset UUID 11"\
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ $uuidnumb = 1 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$AAA/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 2 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$BBB/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 3 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$CCC/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 4 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$DDD/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 5 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$EEE/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 6 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$FFF/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 7 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$GGG/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 8 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$HHH/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 9 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$III/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 10 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$JJJ/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 11 ]; then
            echo "frame_rate_limiter=0" > /sys/bus/mdev/devices/$KKK/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have unlock UUID=$uuidnumb 60fps limit, VM that use this UUID will have full GPU performance! √$(tput sgr 0)"
          fi
      fi
    fi
  }

  undoFPS(){
    if [ $L = "cn" ];then # CN
      uuidnumb=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
      请选择你希望恢复的UUID预设编号，回车继续！" 22 60 12 \
      "1" "Preset UUID 1 - 预设1"\
      "2" "Preset UUID 2 - 预设2"\
      "3" "Preset UUID 3 - 预设3"\
      "4" "Preset UUID 4 - 预设4"\
      "5" "Preset UUID 5 - 预设5"\
      "6" "Preset UUID 6 - 预设6"\
      "7" "Preset UUID 7 - 预设7"\
      "8" "Preset UUID 8 - 预设8"\
      "9" "Preset UUID 9 - 预设9"\
      "10" "Preset UUID 10 - 预设10"\
      "11" "Preset UUID 11 - 预设11"\
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ $uuidnumb = 1 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$AAA/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 2 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$BBB/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 3 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$CCC/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 4 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$DDD/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 5 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$EEE/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 6 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$FFF/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 7 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$GGG/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 8 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$HHH/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 9 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$III/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 10 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$JJJ/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 11 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$KKK/nvidia/vgpu_params
            echo "$(tput setaf 2)√ 搞定！UUID编号为$uuidnumb已恢复到默认，即开启60帧限制，请到网页端启动与之关联的虚拟机！ √$(tput sgr 0)"
          fi
      fi
    else # EN
      uuidnumb=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
      Select UUID preset you want to bring back limit: " 22 60 12 \
      "1" "Preset UUID 1"\
      "2" "Preset UUID 2"\
      "3" "Preset UUID 3"\
      "4" "Preset UUID 4"\
      "5" "Preset UUID 5"\
      "6" "Preset UUID 6"\
      "7" "Preset UUID 7"\
      "8" "Preset UUID 8"\
      "9" "Preset UUID 9"\
      "10" "Preset UUID 10"\
      "11" "Preset UUID 11"\
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ $uuidnumb = 1 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$AAA/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 2 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$BBB/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 3 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$CCC/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 4 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$DDD/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 5 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$EEE/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 6 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$FFF/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 7 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$GGG/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 8 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$HHH/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 9 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$III/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 10 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$JJJ/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
          if [ $uuidnumb = 11 ]; then
            echo "frame_rate_limiter=1" > /sys/bus/mdev/devices/$KKK/nvidia/vgpu_params
            echo "$(tput setaf 2)√ Done! You have uuid=$uuidnumb brought back 60fps limit! √$(tput sgr 0)"
          fi
      fi
    fi
  }

  reinstallDriver(){
    currentDriver=$(nvidia-smi --query-gpu=driver_version --format=csv | sed -n '2p')

    uninstallDriver(){
      /root/NVIDIA-Linux-x86_64-$currentDriver-vgpu-kvm.run --uninstall
      echo "$(tput setaf 2)Done uninstall driver, please reboot! 已卸载完毕，请重启！"
      echo "After reboot please run script again! 重启完毕后重新运行脚本选择你想要安装的版本！$(tput setaf 0)"
    }

    insDriver(){
      if [ $1 = '450' ];then
        driver="450.80"
      fi
      if [ $1 = '45089' ];then
        driver="450.89"
      fi
      if [ $1 = '450124' ];then
        driver="450.124"
      fi
      if [ $1 = '460' ];then
        driver="460.32.04"
      fi
      if [ $1 = '46073' ];then
        driver="460.73.02"
      fi

      # Install driver
      echo "======================================================================="
      echo "$(tput setaf 2)installing Nvidia $driver Driver... 正在安装原版$driver显卡驱动程序$(tput sgr 0)"
      echo "======================================================================="
      cd /root/
      if [ ! -x /usr/bin/nvidia-smi ];then
        if test -f "NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run";then
        chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
        /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run --dkms
        else
        wget https://f000.backblazeb2.com/file/vGPUdrivers/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run -P /root/
        chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
        /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run --dkms
        fi
      fi

      # modify driver
      if [ `grep "vgpu_unlock_hooks.c" /usr/src/nvidia-$driver/nvidia/os-interface.c|wc -l` = 0 ];then
        sed -i '20a#include "/root/vgpu_unlock/vgpu_unlock_hooks.c"' /usr/src/nvidia-$driver/nvidia/os-interface.c
      fi
      if [ `grep "kern.ld" /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild|wc -l` = 0 ];then
        echo "ldflags-y += -T /root/vgpu_unlock/kern.ld" >> /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild
      fi
      if [ `grep "vgpu_unlock" /lib/systemd/system/nvidia-vgpud.service|wc -l` = 0 ];then
        sed -i 's#ExecStart=#ExecStart=/root/vgpu_unlock/vgpu_unlock #' /lib/systemd/system/nvidia-vgpud.service
      fi
      if [ `grep "vgpu_unlock" /lib/systemd/system/nvidia-vgpu-mgr.service|wc -l` = 0 ];then
        sed -i 's#ExecStart=#ExecStart=/root/vgpu_unlock/vgpu_unlock #' /lib/systemd/system/nvidia-vgpu-mgr.service
      fi

      # reaload daemon
      systemctl daemon-reload

      # remove and reinstall driver
      echo "======================================================================="
      echo "$(tput setaf 2)reconfiguring driver... 正在重新构建驱动$(tput sgr 0)"
      echo "======================================================================="
      dkms remove  -m nvidia -v $driver --all
      dkms install -m nvidia -v $driver

      echo "$(tput setaf 2)======================================================================="
      echo "Done! Please reboot! 搞定！请重启PVE！"
      echo "                                                                 by ksh"
      echo "======================================================================="
      tput sgr 0

    }
    
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "Current Version is $currentDriver! 当前驱动版本$currentDriver！" 15 65 7 \
    "u" "Uninstall --- Current:$currentDriver 请先卸载当前版本" \
    "a" "450.124 ----- 最稳定性能首选推荐" \
    "b" "450.80 ------ 较均衡的OpenGL性能" \
    "c" "450.89 ------ 性能中规中矩" \
    "d" "460.32.04 --- 受支持的460分支版本" \
    "e" "460.73.02 --- 受支持的460分支最新版本" \
    "q" "Go back to X Menu ---------- 返回工具菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    u ) uninstallDriver;;
    a ) insDriver 450;;
    b ) insDriver 45089;;
    c ) insDriver 450124;;
    d ) insDriver 460;;
    e ) insDriver 46073;;
    q ) ulimenu;;
    esac
  }

  intelGVTmenu(){

    runIntelGVT(){
      clear
      if [ $1 = 'enable' ];then
        # adding extra grub settings
        if [ `grep "enable_gvt" /etc/default/grub | wc -l` = 0 ];then
            sed -i 's#iommu=pt#iommu=pt i915.enable_gvt=1#' /etc/default/grub && update-grub
            echo "$(tput setaf 2)Intel GVT-G added! 已开启GVT-G切分 √$(tput sgr 0)"
          else echo "$(tput setaf 2)GVT-G already added, skip it! 已开启GVT-G切分，无需重复设置！ √$(tput sgr 0)"
        fi

        # adding extra vfio modules
        if [ `grep "kvmgt" /etc/modules|wc -l` = 0 ];then
          echo -e "kvmgt\n#exngt\nvfio-mdev" >> /etc/modules
          echo "$(tput setaf 2)GVT-G module satisfied! 已添加GVT-G模组 √$(tput sgr 0)"
          else echo "$(tput setaf 2)GVT-G module already satisfied! 已添加GVT-G模组，无需重复设置！ √$(tput sgr 0)"
        fi

        # blacklist nouveau
        if [ `grep "nouveau" /etc/modprobe.d/blacklist.conf|wc -l` = 0 ];then
          echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf && update-initramfs -u -k all
          echo "$(tput setaf 2)vfio satisfied 已添加VFIO模组 √$(tput sgr 0)"
          else echo "$(tput setaf 2)nouveau already satisfied! 已屏蔽nouveau驱动，无需重复设置！ √$(tput sgr 0)"
        fi

        # echo
        echo "$(tput setaf 2)Done! Go to PVE webGui->VMid->Hardward->add PCI device->choose your desire GVT-G type$(tput setaf 0)"
        echo "$(tput setaf 2)设置完毕！请到网页端->虚拟机id->硬件->PCI设备->添加你希望的类型$(tput setaf 0)"
        tput setaf 0
      fi

      if [ $1 = 'disable' ];then
        # delete extra grub settings
        sed -i 's#iommu=pt i915.enable_gvt=1#iommu=pt#' /etc/default/grub
        update-grub

        # delete extra vfio modules
        sed -i '/kvmgt/d' /etc/modules
        sed -i '/#exngt/d' /etc/modules
        sed -i '/vfio-mdev/d' /etc/modules

        # update initramfs
        update-initramfs -u -k all

        # echo
        echo "$(tput setaf 2)GVT-G disabled! ! Please reboot!"
        echo "$(tput setaf 2)已关闭GVT-G！请重启宿主机！$(tput setaf 0)"
        tput setaf 0
      fi
    }

    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    Free GVT-G vGPU Slicing! Support from UHD510 to UHD630! Intel ONLY!
    免费vGPU切分，仅适用于Intel核显vGPU切分，支持UHD510-630等型号" 16 80 5 \
    "a" "Enable Intel GVT-G ------ 开启Intel核显切分" \
    "b" "Disable iGPU GVT-G ------ 关闭Intel核显切分" \
    "q" "Go back to X Menu ------- 返回工具菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) runIntelGVT enable;;
    b ) runIntelGVT disable;;
    q ) ulimenu;;
    esac
  }

  removeSubs(){
    libFire="/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js"
    runremove(){
      if [ $1 = 'remove' ];then
        clear
        sed -i.backup -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" ${libFire}
        systemctl restart pveproxy.service
        echo "$(tput setaf 2)Done! Please Ctrl+F5 refresh WebGUI! 搞定！请刷新网页查看！$(tput setaf 0)"
      fi

      if [ $1 = 'bringback' ];then
        clear
        rm ${libFire}
        cp /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.backup ${libFire}
        rm /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.backup
        systemctl restart pveproxy.service
        echo "$(tput setaf 2)Done undo! Please Ctrl+F5 refresh WebGUI! 已恢复！请刷新网页查看！$(tput setaf 0)"
      fi
    }

    if [ $L = "cn" ];then # CN
      OPTION=$(whiptail --title " 移除订阅弹窗 " --menu "
      此脚本将自动自动移除订阅弹窗，适用于PVE6.2-15及以上版本，包括最新版
      请注意，移除后肯能会造成系统死机，如没有特殊需求，不推荐移除订阅弹窗" 15 80 5 \
      "a" "移除弹窗订阅" \
      "b" "恢复弹窗订阅" \
      "q" "返回工具菜单" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) runremove remove;;
      b ) runremove bringback;;
      q ) ulimenu;;
      esac
    else # EN
      OPTION=$(whiptail --title " Remove popup window " --menu "
      Script will remove none-subscription pop-up window durling login.
      Please be aware of removing popup window may cause PVE crashed." 15 80 5 \
      "a" "Remove pop-up window" \
      "b" "Recover pop-up window" \
      "q" "Go Back to X Menu" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) runremove remove;;
      b ) runremove bringback;;
      q ) ulimenu;;
      esac
    fi
  }

  darkTheme(){ # https://github.com/Weilbyte/PVEDiscordDark
    rundarkTheme(){
      if [ $1 = 'install' ];then
        bash <(curl -s https://raw.githubusercontent.com/Weilbyte/PVEDiscordDark/master/PVEDiscordDark.sh ) install
        echo "$(tput setaf 2)Done! Please refresh Webgui! 已安装，请刷新网页查看！$(tput setaf 0)"
      fi

      if [ $1 = 'uninstall' ];then
        bash <(curl -s https://raw.githubusercontent.com/Weilbyte/PVEDiscordDark/master/PVEDiscordDark.sh ) uninstall
        echo "$(tput setaf 2)Done uninstall! Please refresh Webgui! 已卸载主题，请刷新网页查看！$(tput setaf 0)"
      fi
    }
    if [ $L = "cn" ];then # CN
      OPTION=$(whiptail --title " PVE暗黑主题 " --menu "
      此脚本将自动安装一套非常漂亮的PVE暗黑主题" 12 80 3 \
      "a" "安装主题" \
      "b" "卸载主题" \
      "q" "返回工具菜单" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) rundarkTheme install;;
      b ) rundarkTheme uninstall;;
      q ) ulimenu;;
      esac
    else # EN
      OPTION=$(whiptail --title " PVE Dark Theme " --menu "
      Script will auto install PVE dark theme" 12 80 3 \
      "a" "install dark theme" \
      "b" "remove dark theme" \
      "q" "Go Back to X menu" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      a ) rundarkTheme install;;
      b ) rundarkTheme uninstall;;
      q ) ulimenu;;
      esac
    fi
  }

  cpuPerformance(){
    runCPUAdjust(){
      if [ $1 = 'install' ];then
        if [ $L = "cn" ];then # CN
          if (whiptail --title "安装CPU模式调节工具" --yes-button "继续" --no-button "返回"  --yesno "
          即将安装CPU模式调节工具，回车继续！" 10 60) then
            if [ `grep "intel_pstate=disable" /etc/default/grub|wc -l` = 0 ];then
            sed -i 's|quiet|quiet intel_pstate=disable|' /etc/default/grub
            update-grub
            apt install -y cpufrequtils
            echo "$(tput setaf 2)搞定！请重启宿主机！$(tput setaf 0)"
            else
            whiptail --title "Warnning" --msgbox "  CPU性能调节工具已存在，无需重复安装！" 8 43
            ulimenu
            fi
          fi
        else # EN
          if (whiptail --title "cpufrequtils" --yes-button "Continue" --no-button "Go Back"  --yesno "
            Install cpufrequtils tools?" 10 60) then
              if [ `grep "intel_pstate=disable" /etc/default/grub|wc -l` = 0 ];then
              sed -i 's|quiet|quiet intel_pstate=disable|' /etc/default/grub
              update-grub
              apt install -y cpufrequtils
              echo "$(tput setaf 2)Done! Please reboot host! $(tput setaf 0)"
            else
            whiptail --title "Warnning" --msgbox "  CPUfreq already installed! Skip it! " 8 43
            ulimenu
            fi
          fi
        fi # end language
      fi # end install

      if [ $1 = 'save' ];then
        if [ $L = "cn" ];then # CN
          if (whiptail --title "CPU 省电模式" --yes-button "继续" --no-button "返回"  --yesno "
          省电模式将主频锁定在最低频运行，工作时频率不再跳动，节能环保低温！" 10 85) then
            if [ `grep 'GOVERNOR="powersave"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="powersave"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)已设置成省电模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
              else echo "$(tput setaf 2)已经是省电模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        else # EN
          if (whiptail --title "CPU PowerSave mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
          PowerSave mode locks CPU down to lowest frequency even with heavy load." 10 85) then
            if [ `grep 'GOVERNOR="powersave"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="powersave"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)CPU mode is set to PowerSave mode!$(tput setaf 0)"
              echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
              else echo "$(tput setaf 2)Already in PowerSave mode, skip it!$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        fi # end language
      fi # end save

      if [ $1 = 'ondemand' ];then
        if [ $L = "cn" ];then # CN
            if (whiptail --title "CPU 自适应模式" --yes-button "继续" --no-button "返回"  --yesno "
              自适应模式随系统繁忙程度而频繁跳动频率，低负载时也会让CPU主频以高频率运行！" 10 85) then
            if [ `grep 'GOVERNOR="ondemand"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="ondemand"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)已设置成自适应模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
              else echo "$(tput setaf 2)已经是自适应模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        else # EN
            if (whiptail --title "CPU ondemand mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
        Ondemand mode dynamically adjusts CPU frequency according system load." 10 85) then
            if [ `grep 'GOVERNOR="ondemand"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="ondemand"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)CPU mode is set to ondemand mode!$(tput setaf 0)"
              echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
              else echo "$(tput setaf 2)Already in ondemand mode, skip it!$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        fi # end language
      fi # ondemand

      if [ $1 = 'performance' ];then
        if [ $L = "cn" ];then # CN
            if (whiptail --title "CPU 高性能模式" --yes-button "继续" --no-button "返回"  --yesno "
              高性能模式将锁定CPU到最高频率，即便系统零负载！该模式为PVE默认，性能最高！" 10 85) then
            if [ `grep 'GOVERNOR="performance"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="performance"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)已设置成高性能模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
              else echo "$(tput setaf 2)已经是高性能模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        else # EN
            if (whiptail --title "CPU performance mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
        performance mode locks CPU to highest frequency even low system load." 10 85) then
            if [ `grep 'GOVERNOR="performance"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
              sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
              sed -i '43iGOVERNOR="performance"' /etc/init.d/cpufrequtils
              systemctl daemon-reload
              /etc/init.d/cpufrequtils restart
              clear
              echo "$(tput setaf 2)CPU mode is set to performance mode!$(tput setaf 0)"
              echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
              else echo "$(tput setaf 2)Already in performance mode, skip it!$(tput setaf 0)"
            fi
            else
            ulimenu
          fi
        fi # end language
      fi # end performance
    }
    if [ $L = "cn" ];then # CN
      OPTION=$(whiptail --title " CPU性能调整 " --menu "
      此脚本将将自动设置CPU性能，PVE默认为高性能（不降频）模式!
      请先安装调整工具，再选择模式，支持无缝切换无需重启！" 15 80 5 \
      "0" "安装CPU模式调整工具" \
      "a" "省电模式" \
      "b" "自适应模式" \
      "c" "高性能模式" \
      "q" "返回工具菜单" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      0 ) runCPUAdjust install;;
      a ) runCPUAdjust save;;
      b ) runCPUAdjust ondemand;;
      c ) runCPUAdjust performance;;
      q ) ulimenu;;
      esac
    else # EN
      OPTION=$(whiptail --title " CPU performance adjust tool " --menu "
      Adjusting CPU performance mode, by default PVE sets to Performance
      Please install CPUfreq util first, then select desire mode" 15 80 5 \
      "0" "First install CPUfrequtils" \
      "a" "Power Saving mode" \
      "b" "Power Ondemand mode" \
      "c" "High Performance mode" \
      "q" "Go Back to X menu" \
      3>&1 1>&2 2>&3)
      case "$OPTION" in
      0 ) runCPUAdjust install;;
      a ) runCPUAdjust save;;
      b ) runCPUAdjust ondemand;;
      c ) runCPUAdjust performance;;
      q ) ulimenu;;
      esac
    fi
  }

  gpuPassthrough(){
    if [ $L = "cn" ];then # CN
      # user selected GPU pcid with all functions
      if (whiptail --title "物理显卡直通" --yes-button "继续" --no-button "返回"  --yesno "一键物理显卡直通到指定虚拟机，无脑便捷！脚本暂只支持N卡直通！
        务必确保虚拟机采用Q35类型且为SeaBIOS！注意：直通后的N卡win10驱动版本需大于471.11" 10 85) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
            array+=($id "$dsc")
          done < <(lspci -nn | grep -i VGA)
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu '以下为当前主板已插入的物理显卡，请选择相应显卡，回车继续！' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        deviceID=$(echo $pciID | awk -F "." '{print $1}')
        # user selected VM's ID
        clear
        list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
        echo -n "">lsvm
        ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
        ls=`cat lsvm`
        rm lsvm
        h=`echo $ls|wc -l`
        let h=$h*1
        if [ $h -lt 30 ];then
            h=30
        fi
        list1=`echo $list|awk 'NR>1{print $1}'`
        vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
        选择虚拟机：" 20 60 10 \
        $(echo $ls) \
        3>&1 1>&2 2>&3)
        exitstatus=$?
        if [ $exitstatus = 0 ]; then
          if [ "$vmid" -le 9999 -a "$vmid" -ge 100 ]; then
            # first delete any vGPU conf if any
            sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/-uuid/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/#vGPU added/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/$vmid.conf
            # assign selected GPU to VM, example: hostpci0: 0000:09:00,pcie=1
            qm set $vmid -hostpci0 0000:$deviceID,pcie=1
            echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
          else
          ulimenu
          fi
        fi

      else ulimenu
      fi
    else # EN
      # user selected GPU pcid with all functions
      if (whiptail --title "GPU Passthrough" --yes-button "Continue" --no-button "Go Back"  --yesno "
        Support Nvidia ONLY! Make sure VM have Q35 and SeaBIOS configured. 
        For guest Win10 driver version shoudld larger than 471.11" 10 90) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
            array+=($id "$dsc")
          done < <(lspci -nn | grep -i VGA)
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu 'Select a GPU you want to passthrough' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        deviceID=$(echo $pciID | awk -F "." '{print $1}')
        # user selected VM's ID
        clear
        list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
        echo -n "">lsvm
        ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
        ls=`cat lsvm`
        rm lsvm
        h=`echo $ls|wc -l`
        let h=$h*1
        if [ $h -lt 30 ];then
            h=30
        fi
        list1=`echo $list|awk 'NR>1{print $1}'`
        vmid=$(whiptail  --title "vGPU Unlock Tools - Version : 0.0.4" --menu "
        Please select a VM: " 20 60 10 \
        $(echo $ls) \
        3>&1 1>&2 2>&3)
        exitstatus=$?
        if [ $exitstatus = 0 ]; then
          if [ "$vmid" -le 9999 -a "$vmid" -ge 100 ]; then
            # first delete any vGPU conf if any
            sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/-uuid/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/#vGPU added/d' /etc/pve/qemu-server/$vmid.conf
            sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/$vmid.conf
            # assign selected GPU to VM, example: hostpci0: 0000:09:00,pcie=1
            qm set $vmid -hostpci0 0000:$deviceID,pcie=1
            echo "$(tput setaf 2)Done! Please check PVE webgui! $(tput setaf 0)"
          else ulimenu
          fi
        fi
        else ulimenu
      fi
    fi # end EN language
  }

  changeSource(){
    if (whiptail --title "更换APT源" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将更改PVE源为国内服务器，以获得更快的下载更新速度" 10 80) then
        if [ `grep "ustc.edu.cn" /etc/apt/sources.list|wc -l` = 0 ];then
          cp /etc/apt/sources.list /etc/apt/sources.list.bak
echo "deb https://mirrors.ustc.edu.cn/debian/ $pveRepo main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo main contrib non-free
deb https://mirrors.ustc.edu.cn/debian/ $pveRepo-updates main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo-updates main contrib non-free
deb https://mirrors.ustc.edu.cn/debian/ $pveRepo-backports main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo-backports main contrib non-free
deb https://mirrors.ustc.edu.cn/debian-security/ $pveRepo/updates main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian-security/ $pveRepo/updates main contrib non-free" > /etc/apt/sources.list

        # for pve7 repo
        if [ $pveRepo = "bullseye" ];then
          sed -i 's/buster\/updates/bullseye-security/g;s/buster/bullseye/g' /etc/apt/sources.list
        fi
        echo "$(tput setaf 2)更换国内源成功！开始更新... $(tput setaf 0)"
        apt update && apt upgrade -y
        echo "$(tput setaf 2)完成更新！$(tput setaf 0)"
        else echo "$(tput setaf 2)已经是国内源！无需重复设置！$(tput setaf 0)"
        fi
    fi
  }

  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "                    各类实用工具集合" 24 60 14 \
    "a" "美化LS,CAT命令" \
    "b" "安装bpytop" \
    "c" "添加/删除 硬盘直通" \
    "d" "检查IOMMU分组" \
    "e" "完全拆分IOMMU组(慎用)" \
    "f" "vGPU帧率解锁" \
    "g" "物理显卡直通到虚拟机" \
    "h" "Intel GVT-G 适用于Intel核显vGPU切分" \
    "i" "移除订阅弹窗" \
    "j" "PVE暗黑主题" \
    "k" "CPU性能模式调整" \
    "l" "修改apt为国内源" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    else # EN
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "                    Useful tools for PVE" 24 60 14 \
    "a" "Beautify LS, CAT command" \
    "b" "Install bpytop" \
    "c" "Passthrough/Remove disk to VM" \
    "d" "Check IOMMU group" \
    "e" "Complete break down IOMMU group" \
    "f" "vGPU FPS unlock" \
    "g" "GPU passthrough to VM" \
    "h" "Intel GVT-G vGPU slicing" \
    "i" "Remove none-subs popup window" \
    "j" "PVE dark theme" \
    "k" "Adjust CPU performance" \
    "q" "Go back to Main Menu" \
    3>&1 1>&2 2>&3)
  fi

  case "$OPTION" in
    a ) LSCATmenu;;
    b ) BPYTOPmenu;;
    c ) diskMenu;;
    d ) iommuGroupcheck;;
    e ) extraGrubsettings;;
    f ) unlockFPSmenu;;
    g ) gpuPassthrough;;
    h ) intelGVTmenu;;
    i ) removeSubs;;
    j ) darkTheme;;
    k ) cpuPerformance;;
    l ) changeSource;;
    q ) main;;
  esac
  tput sgr 0
}

VMdeployMenu(){

  DSMid="666"
  UNRAIDid="555"
  OpenWRTid="777"
  MacOSid="888"
  isoFolder="/var/lib/vz/template/iso"

  deployDSM(){ # deploy a DSM vm
    if [ $1 = '3617' ];then
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署DSM" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为666，名字为DSM3617的虚拟机
      支持半虚拟化网卡，默认创建时CPU为1个，内存1G，DSM版本为6.1
      请复制以下地址到浏览器下载pat安装包，然后回车继续（国内直连下载）
      http://down.nas2x.com/synology/dsm/6.1/6.1.6/ds3617xs/DSM_DS3617xs_15266.pat" 12 90) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      # creates a VM
      clear
      echo "$(tput setaf 2)正在设置虚拟机，请稍候！$(tput setaf 0)"
      tput setaf 0
      qm create $DSMid -name DSM3617 -net0 virtio,bridge=vmbr0,firewall=1 -ostype win10 -memory 1024

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3617xs_DSM6.1_Broadwell.img" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3617xs_DSM6.1_Broadwell.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      /opt/img2kvm /opt/DS3617xs_DSM6.1_Broadwell.img $DSMid $storage
      qm set $DSMid -ide0 $storage:vm-$DSMid-disk-0
      qm set $DSMid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
      else VMdeployMenu
      fi
    else # EN
      if (whiptail --title "deploy DSM" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script auto deploys a VM(ID=666, name=DSM3617). Support paravirtualized.
      By default it has CPU=1, memory=1G, no extra hard disk, DSM version is 6.1
      
      First of all please download the pat file by this link:
      http://down.nas2x.com/synology/dsm/6.1/6.1.6/ds3617xs/DSM_DS3617xs_15266.pat" 12 90) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      # creates a VM
      clear
      echo "$(tput setaf 2)Setting up VM, please wait...$(tput setaf 0)"
      tput setaf 0
      qm create $DSMid -name DSM3617 -net0 virtio,bridge=vmbr0,firewall=1 -ostype win10 -memory 1024

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3617xs_DSM6.1_Broadwell.img" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3617xs_DSM6.1_Broadwell.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      /opt/img2kvm /opt/DS3617xs_DSM6.1_Broadwell.img $DSMid $storage
      qm set $DSMid -ide0 $storage:vm-$DSMid-disk-0
      qm set $DSMid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"
      else VMdeployMenu
      fi
    fi # end language
    fi # end $1 3617
  }

  deployUnraid(){ # deploy a Unraid vm
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署UNRAID" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为555，名字为Unraid的虚拟机，支持半虚拟化网卡！
      默认创建时CPU为1个，内存1G，无硬盘！该虚拟机将通过U盘启动进入Unraid系统！
      运行之前请务必将Unraid的启动U盘插到PVE宿主机主板上，再回车继续！" 12 90) then
      # user select unraid usb drive
        read usbdev < <(
        declare -a array=()
        while read foo{,,,,} id dsc;do
          array+=($id "$dsc")
        done < <(lsusb)
        whiptail --menu '以下为当前主板上已插入的各类usb设备，请选择unriad的启动u盘，回车继续！' 20 80 10 "${array[@]}" 2>&1 >/dev/tty)
        echo $usbdev
      else VMdeployMenu
      fi
      # if $usbdev is not null then create unraid VM
      if [ -n "${usbdev}" ];then
        echo "$(tput setaf 2)正在设置虚拟机，请稍候！$(tput setaf 0)"
        qm create $UNRAIDid -name Unraid -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -usb0 host=$usbdev
        qm set $UNRAIDid -boot order=usb0
        # echo done
        echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
      fi
    else # EN
      if (whiptail --title "Deploy UNRAID VM" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script auto deploys a VM(ID=555, name=Unraid). Support paravirtualized.
      By default it has CPU=1, memory=1G, no hard disk.
      You need to prepare an Unraid usb drive to boot into the system." 12 90) then
      # user select unraid usb drive
        read usbdev < <(
        declare -a array=()
        while read foo{,,,,} id dsc;do
          array+=($id "$dsc")
        done < <(lsusb)
        whiptail --menu 'List of current USB drive on your motherboard: ' 20 80 10 "${array[@]}" 2>&1 >/dev/tty)
        echo $usbdev
      else VMdeployMenu
      fi
      # if $usbdev is not null then create unraid VM
      if [ -n "${usbdev}" ];then
        echo "$(tput setaf 2)Setting up VM, please wait...$(tput setaf 0)"
        qm create $UNRAIDid -name Unraid -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -usb0 host=$usbdev
        qm set $UNRAIDid -boot order=usb0
        # echo done
        echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"
      fi
    fi
  }

  deployOpenWRT(){ # deploy a openwrt vm
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署OpenWRT" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为777，名字为OpenWRT的软路由虚拟机，固件为esir精品小包v7
      支持半虚拟化网卡，默认创建时CPU为1个，内存1G！运行后默认ip为192.168.5.1无密码" 12 90) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      # creates a VM
      clear
      echo "$(tput setaf 2)正在设置虚拟机，请稍候！$(tput setaf 0)"
      qm create $OpenWRTid -name OpenWRT -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024
      tput setaf 6

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img.gz" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img.gz -P /opt/
      fi
      chmod +x /opt/img2kvm
      gzip -dfk openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img.gz
      /opt/img2kvm /opt/openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img $OpenWRTid $storage
      qm set $OpenWRTid -ide0 $storage:vm-$OpenWRTid-disk-0
      qm set $OpenWRTid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)搞定！请到网页端查看！支持半虚拟化网卡，默认创建时CPU为1个，内存1G！运行后默认ip为192.168.5.1无密码！$(tput setaf 0)"
      echo "$(tput setaf 2)如需修改ip请在控制台运行如下命令进行修改：$(tput setaf 0)"
      echo "$(tput setaf 2)sed -i 's|192.168.5.1|x.x.x.x|' /etc/config/network （替换x.x.x.x为你希望设置的IP即可）$(tput setaf 0)"
      else VMdeployMenu
      fi
    else # EN
      echo "wip"
    fi # end language
  }

  deployMacOS(){ # deploy a MacOS vm
    # igpu passthro ref
    # https://www.reddit.com/r/VFIO/comments/innriq/successful_macos_catalina_with_intel_gvtg/
    # https://forum.proxmox.com/threads/host-crash-and-reboot-everytime-running-multiple-vm-with-gpu-and-igpu-passthrough.83874/

    # install ref
    # https://zhuanlan.zhihu.com/p/240710958
    runMacOSvm(){
      clear
      cd /opt/
      echo "Downloading MacOS BaseSystem dmg ...正在下载基础镜像..."
      if [ ! -f /opt/BaseSystem.dmg ];then
        if [ -f /opt/fetch-macOS-v2.py ];then
          chmod +x /opt/fetch-macOS-v2.py
          if [ $1 = 'Sierra' ];then
            echo "1" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Mojave' ];then
            echo "2" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Catalina' ];then
            echo "3" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'BigSur' ];then
            echo "4" | /opt/fetch-macOS-v2.py
          fi
        else
          wget https://f000.backblazeb2.com/file/vGPUdrivers/fetch-macOS-v2.py # CN
          # wget https://raw.githubusercontent.com/kholia/OSX-KVM/master/fetch-macOS-v2.py # EN
          chmod +x /opt/fetch-macOS-v2.py
          if [ $1 = 'Sierra' ];then
            echo "1" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Mojave' ];then
            echo "2" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Catalina' ];then
            echo "3" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'BigSur' ];then
            echo "4" | /opt/fetch-macOS-v2.py
          fi
        fi
      fi

      echo "Converting installer iso ...正在转换格式..."
      if [ ! -f $isoFolder/$1-installer.iso ];then
          qemu-img convert BaseSystem.dmg -O raw $1-installer.iso
          mv /opt/$1-installer.iso $isoFolder/
          rm /opt/BaseSystem.dmg
          rm /opt/BaseSystem.chunklist
      fi

      echo "Downloading OpenCore iso ...正在下载引导镜像..."
      if [ ! -f $isoFolder/OpenCore.iso ];then
          wget https://f000.backblazeb2.com/file/vGPUdrivers/OpenCore.iso.gz -P $isoFolder/ # CN+EN
          gzip -d $isoFolder/OpenCore.iso.gz
      fi

      echo "Creating VM ...正在创建虚拟机..."
      qm create $MacOSid -name MacOS -net0 vmxnet3,bridge=vmbr0,firewall=1 -ostype other -cpu host \
      -memory 4096 -balloon 0 -cores 4 -machine q35 -vga vmware -bios ovmf \
      -ide2 local:iso/OpenCore.iso,cache=unsafe \
      -ide0 local:iso/$1-installer.iso,cache=unsafe \
      -sata0 $storage:100,ssd=1,cache=unsafe

      qm set $MacOSid -efidisk0 $storage:0
      qm set $MacOSid -boot order=ide2

      # check cpu type
      if [ `cat /proc/cpuinfo|grep Intel|wc -l` = 0 ];then # AMD
          echo '-args: -device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc" -smbios type=2 -device usb-kbd,bus=ehci.0,port=2 -cpu Penryn,kvm=on,vendor=GenuineIntel,+kvm_pv_unhalt,+kvm_pv_eoi,+hypervisor,+invtsc,+pcid,+ssse3,+sse4.2,+popcnt,+avx,+avx2,+aes,+fma,+fma4,+bmi1,+bmi2,+xsave,+xsaveopt,check' >> /etc/pve/qemu-server/$MacOSid.conf
      else # Intel
          echo 'args: -device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc" -smbios type=2 -device usb-kbd,bus=ehci.0,port=2 -cpu host,kvm=on,vendor=GenuineIntel,+kvm_pv_unhalt,+kvm_pv_eoi,+hypervisor,+invtsc' >> /etc/pve/qemu-server/$MacOSid.conf
      fi
      cd /root/
    }

    selectMacOSversion(){
      if [ $L = "cn" ];then # CN
        OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
        无脑全自动部署一台ID为888，名字为MacOS的虚拟机，无痛黑果，小白福音！
        虚拟机默认创建时CPU为4个，内存4G，系统盘100G！请选择MacOS版本，回车继续：" 24 90 12 \
        "a" "High Sierra (10.13)" \
        "b" "Mojave (10.14)" \
        "c" "Catalina (10.15)" \
        "d" "Big Sur" \
        "q" "返回主菜单" \
        3>&1 1>&2 2>&3)
        case "$OPTION" in
          a ) runMacOSvm Sierra;;
          b ) runMacOSvm Mojave;;
          c ) runMacOSvm Catalina;;
          d ) runMacOSvm BigSur;;
          q ) main;;
        esac
      else # EN
        OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
        Script auto deploys a VM(ID=888, name=MacOS). Support multiple OSX.
        By default it has CPU=4, memory=4G, 100G hard disk" 24 90 12 \
        "a" "High Sierra (10.13)" \
        "b" "Mojave (10.14)" \
        "c" "Catalina (10.15)" \
        "d" "Big Sur" \
        "q" "Go Back to Main Menu" \
        3>&1 1>&2 2>&3)
        case "$OPTION" in
          a ) runMacOSvm Sierra;;
          b ) runMacOSvm Mojave;;
          c ) runMacOSvm Catalina;;
          d ) runMacOSvm BigSur;;
          q ) main;;
        esac
      fi
    }

    selectStorage(){
      if [ $L = "cn" ];then # CN
        read storage < <(
            declare -a array=()
            while read id foo{,,,,} dsc;do
            array+=($id "$dsc")
            done < <(pvesm status | sed -n '1!p')
            whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        selectMacOSversion
      else # EN
        read storage < <(
            declare -a array=()
            while read id foo{,,,,} dsc;do
            array+=($id "$dsc")
            done < <(pvesm status | sed -n '1!p')
            whiptail --title "vGPU Unlock Tools - Version : 0.0.4" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        selectMacOSversion
      fi
    }

    if [ $L = "cn" ];then # CN
      # check if pve has msr option enabled
      if [ `grep "ignore_msrs=Y" /etc/modprobe.d/kvm.conf | wc -l` = 0 ];then
          if (whiptail --title "MacOS虚拟机必要设置检查" --yes-button "继续" --no-button "返回"  --yesno "
          检测到系统尚未设置ignore选项，该设置与MacOS是否顺利安装息息相关
          回车继续设置，返回取消设置：" 10 80) then
              echo "options kvm ignore_msrs=Y" >> /etc/modprobe.d/kvm.conf
              update-initramfs -k all -u
              if (whiptail --title "MacOS虚拟机必要设置检查" --yes-button "是！重启！" --no-button "返回"  --yesno "
              搞定！请重启宿主机，回车将立即重启系统，是否继续？" 10 80) then reboot
              else exit
              fi
          else exit
          fi
      else # has msr
        selectStorage
      fi
    else # EN
        # check if pve has msr option enabled
        if [ `grep "ignore_msrs=Y" /etc/modprobe.d/kvm.conf | wc -l` = 0 ];then
            if (whiptail --title "MacOS VM msr settings" --yes-button "Continue" --no-button "Go Back"  --yesno "
            Your PVE seems not yet being correctly setup ignore_msr, which MacOS vm require.
            Press enter to enable this option: " 10 80) then
                echo "options kvm ignore_msrs=Y" >> /etc/modprobe.d/kvm.conf
                update-initramfs -k all -u
                if (whiptail --title "MacOS VM msr settings" --yes-button "Reboot host!" --no-button "Go Back"  --yesno "
                Done! Please reboot host, cotinue to reboot?" 10 80) then reboot
                else exit
                fi
            else exit
            fi
        else # has msr
          selectStorage
        fi
    fi
  }
  
  # select vm menu
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    此脚本提供一系列VM虚拟机，无脑一键自动化部署即可使用
    后续会陆续增加更多一键虚拟机无脑自动化部署" 20 70 10 \
    "a" "新部署一台3617 - 支持半虚拟化网卡" \
    "b" "新部署一台Unraid - NAS虚拟机" \
    "c" "新部署一台MacOS - 黑果虚拟机" \
    "d" "新部署一台OpenWRT - x86软路由虚拟机" \
    "v" "新部署一台vGPU授权LXC - 用于提供vWS授权" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
      a ) deployDSM 3617;;
      b ) deployUnraid;;
      c ) deployMacOS;;
      d ) deployOpenWRT;;
      v ) setupLXC;;
      q ) main;;
    esac
  else # EN
    OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
    Script will auto deploying all kinds of VM
    There will be more automations in future, please stay tuned!" 20 75 10 \
    "a" "Deploy DSM3617 with virtio paravirtualized" \
    "b" "Deploy Unraid VM with virtio paravirtualized" \
    "c" "Deploy MacOS VM" \
    "v" "Deploy vGPU LXC for hosting license" \
    "q" "Go Back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
      a ) deployDSM 3617;;
      b ) deployUnraid;;
      c ) deployMacOS;;
      v ) setupLXC;;
      q ) main;;
    esac
  fi
}

# --------------------------------------------------------- end module function --------------------------------------------------------- #

main(){
  # adding unlock command
  if [ `grep "alias unlock" ~/.bashrc|wc -l` = 0 ];then
    if [ -f "/root/begin.sh" ];then
      echo "alias unlock='/root/begin.sh'" >> ~/.bashrc
      echo "alias zmkm='/root/begin.sh'" >> ~/.bashrc
    fi
  fi
  
  if (whiptail --title "Select Language 选择语言 ksh v0.0.4" --yes-button "中文" --no-button "English"  --yesno "
    █░█ █▀▀ █▀█ █░█   █░█ █▄░█ █░░ █▀█ █▀▀ █▄▀
    ▀▄▀ █▄█ █▀▀ █▄█   █▄█ █░▀█ █▄▄ █▄█ █▄▄ █░█
    ------------------------------------------
       https://github.com/kevinshane/unlock
    ------------------------------------------" 12 55) then
    L="cn"
    else
    L="en"
  fi

  if [ $L = "cn" ];then
  OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
  新装PVE请务必先执行步骤a和b
  根据需求选择c和d其中之一，请勿同时混用两种方案" 22 60 11 \
  "a" "更新系统" \
  "b" "解锁vGPU" \
  "c" "方案一：Quadro切分" \
  "d" "方案二：vGPU切分" \
  "s" "当前切分状态" \
  "r" "重置所有设置" \
  "t" "实时硬件状态" \
  "v" "查看日志" \
  "x" "实用工具" \
  "z" "一键虚拟机部署" \
  "q" "退出程序" \
  3>&1 1>&2 2>&3)
  else
  OPTION=$(whiptail --title " vGPU Unlock Tools - Version : 0.0.4 " --menu "
  For fresh install PVE, run step a and b first
  Do not mix running opt1 and opt2 at the same time" 22 60 11 \
  "a" "Update PVE" \
  "b" "Unlock vGPU" \
  "c" "Opt1: Quadro unlock" \
  "d" "Opt2: vGPU unlock" \
  "s" "Current slice status" \
  "r" "Reset to default" \
  "t" "Real time info" \
  "v" "Check log" \
  "x" "PVE tools" \
  "z" "One click auto VM deploy" \
  "q" "Quit" \
  3>&1 1>&2 2>&3)
  fi

  case "$OPTION" in
  a ) startUpdate;;
  b ) startUnlock;;
  c ) QuadroMenu;;
  d ) vGPUMenu;;
  s ) checkStatus;;
  r ) resetDefaultvGPU;;
  t ) realtimeHW;;
  v ) checkNVlog;;
  x ) ulimenu;;
  z ) VMdeployMenu;;
  q ) tput sgr 0
      exit;;
  esac
  tput sgr 0
}

while getopts "hcdstpoevr" opt; do
  case ${opt} in
    h ) # help menu
      echo "=================================================================================="
      echo "$(tput setaf 3)unlock version       0.0.4 English help menu               中文版本0.0.4帮助菜单$(tput setaf 7)"
      echo "$(tput setaf 2)                                                                   "
      echo "vGPU:  unlock        Typing unlock displays GUI            输入unlock进入图形界面"
      echo "       unlock -h     Display this help menu                显示帮助菜单"
      echo "       unlock -c     Quadro unlock menu                    Quadro解锁菜单"
      echo "       unlock -c cn  Chinese Quadro unlock menu            中文版Quadro解锁菜单"
      echo "       unlock -d     vGPU unlock menu                      vGPU解锁菜单"
      echo "       unlock -d cn  Chinese vGPU unlock menu              中文版vGPU解锁菜单"
      echo "       unlock -s     Check current vGPU slicing status     查看当前vGPU切分状态"
      echo "       unlock -s cn  Chinese vGPU slicing status           中文版vGPU切分状态"
      echo "       unlock -t     Check real time GPU hardware stat     实时物理显卡状态"
      echo "       unlock -t cn  Chinese real time GPU hardware stat   中文版实时物理显卡状态"
      echo "       unlock -r     Reset vGPU to default stat            初始化vGPU"
      echo "       unlock -v     Check vGPU detail logs                查看vGPU日志"
      echo "       vgpu          Check vGPU support type               查看vGPU虚拟型号$(tput setaf 7)"
      echo "$(tput setaf 4)                                                                   "
      echo "CPU:   unlock -p     Set CPU power mode to performance     设置CPU模式为高性能"
      echo "       unlock -o     Set CPU power mode to ondemand        设置CPU模式为自适应"
      echo "       unlock -e     Set CPU power mode to powersave       设置CPU模式为省电"
      echo "       cpu           Check real time CPU freq detail       查看CPU实时频率$(tput setaf 7)"
      echo "                                                                          $(tput setaf 3)--by ksh$(tput setaf 7)"
      echo "=================================================================================="
      exit 0
      ;;
    s ) # check gpu status
      if [[ $2 = "cn" ]];then
        L="cn"
        else
        L="en"
      fi
      checkStatus
      exit
      ;;
    t ) # real time gpu status
      if [[ $2 = "cn" ]];then
        L="cn"
        else
        L="en"
      fi
      realtimeHW
      ;;
    p ) # cpu performance mode
      if [ -x /usr/bin/cpufreq-info ];then
        if [ `grep 'GOVERNOR="performance"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
          sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
          sed -i '43iGOVERNOR="performance"' /etc/init.d/cpufrequtils
          systemctl daemon-reload
          /etc/init.d/cpufrequtils restart
        fi
        echo "√ Done!"
        exit 0
      else
        echo "Please install CPUfrequtils first! 请先安装CPUfrequtils！"
        exit 0
      fi
      ;;
    o ) # cpu ondemand mode
      if [ -x /usr/bin/cpufreq-info ];then
        if [ `grep 'GOVERNOR="ondemand"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
          sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
          sed -i '43iGOVERNOR="ondemand"' /etc/init.d/cpufrequtils
          systemctl daemon-reload
          /etc/init.d/cpufrequtils restart
        fi
        echo "√ Done!"
        exit 0
      else
        echo "Please install CPUfrequtils first! 请先安装CPUfrequtils！"
        exit 0
      fi
      ;;
    e ) # cpu save mode
      if [ -x /usr/bin/cpufreq-info ];then
        if [ `grep 'GOVERNOR="powersave"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
          sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
          sed -i '43iGOVERNOR="powersave"' /etc/init.d/cpufrequtils
          systemctl daemon-reload
          /etc/init.d/cpufrequtils restart
        fi
        echo "√ Done!"
        exit 0
      else
        echo "Please install CPUfrequtils first! 请先安装CPUfrequtils！"
        exit 0
      fi
      ;;
    c ) # run Quadro unlock
      if [[ $2 = "cn" ]];then
        L="cn"
        else
        L="en"
      fi
      QuadroMenu
      exit 0
      ;;
    d ) # run vGPU unlock
      if [[ $2 = "cn" ]];then
        L="cn"
        else
        L="en"
      fi
      vGPUMenu
      exit 0
      ;;
    v ) # check vgpu log
      journalctl | grep -E 'vgpu|NVRM'
      exit 0
      ;;
    r ) # reset vGPU to default
      L="cn"
      runReset
      tput sgr 0
      exit 0
      ;;
    \? )
      echo error "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done
if [ "$opt" = "?" ]
  then main
fi
