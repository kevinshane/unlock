#!/bin/bash
  ####################################-- $ksh4pveToolversion --####################################
  #
  # ██████╗░██╗░░░██╗███████╗  ████████╗░█████╗░░█████╗░██╗░░░░░
  # ██╔══██╗██║░░░██║██╔════╝  ╚══██╔══╝██╔══██╗██╔══██╗██║░░░░░
  # ██████╔╝╚██╗░██╔╝█████╗░░  ░░░██║░░░██║░░██║██║░░██║██║░░░░░
  # ██╔═══╝░░╚████╔╝░██╔══╝░░  ░░░██║░░░██║░░██║██║░░██║██║░░░░░
  # ██║░░░░░░░╚██╔╝░░███████╗  ░░░██║░░░╚█████╔╝╚█████╔╝███████╗
  # ╚═╝░░░░░░░░╚═╝░░░╚══════╝  ░░░╚═╝░░░░╚════╝░░╚════╝░╚══════╝
  #  Author : ksh
  #  Mail: kevinshane@vip.qq.com
  #  Version: v0.0.5
  #  Github: https://github.com/kevinshane/unlock
  ####################################-- $ksh4pveToolversion --####################################

ksh4pveToolversion="ksh for pve Tool v0.0.5"
# ---------------------- detect pve version ----------------------
pveRepo=`cat /etc/debian_version |awk -F"." '{print $1}'`
case "$pveRepo" in
  11 )
      pveRepo="bullseye"
      ;;
  10 )
      pveRepo="buster"
      ;;
  9 )
      pveRepo="stretch"
      ;;
  * )
      pveRepo=""
esac

# ---------------------- install lsd + bat ----------------------
beautifyLSCAT(){
  if [ $L = "cn" ];then # CN
  echo "======================================================================="
  echo "$(tput setaf 2)正在安装美化程序...$(tput sgr 0)"
  echo "======================================================================="
  else # EN
  echo "======================================================================="
  echo "$(tput setaf 2)Installing lsd + bat...$(tput sgr 0)"
  echo "======================================================================="
  fi
  
  if [ `grep "lsd" ~/.bashrc|wc -l` = 0 ];then
  wget https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb
  wget https://github.com/sharkdp/bat/releases/download/v0.19.0/bat_0.19.0_amd64.deb
  dpkg -i lsd_0.20.1_amd64.deb && rm lsd_0.20.1_amd64.deb
  dpkg -i bat_0.19.0_amd64.deb && rm bat_0.19.0_amd64.deb
  cat <<EOF >> ~/.bashrc
alias ls='lsd'
alias l='ls -l'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'
alias cat='bat'
EOF
exec "$BASH"
  else
  if [ $L = "cn" ];then # CN
  echo "$(tput setaf 2)√ 已添加ls环境变量$(tput sgr 0)"
  else # EN
  echo "$(tput setaf 2)√ Done lsd installed!$(tput sgr 0)"
  fi
  fi

  if [ $L = "cn" ];then # CN
  echo "======================================================================="
  echo "$(tput setaf 2)搞定！请重启SSH，食用方式：ls，ll，la，l，cat$(tput sgr 0)"
  echo "======================================================================="
  else # EN
  echo "======================================================================="
  echo "$(tput setaf 2)Done! Please restart SSH! $(tput sgr 0)"
  echo "======================================================================="
  fi
}
LSCATmenu(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "安装美化版LS，CAT" --yes-button "继续" --no-button "返回"  --yesno "
    此脚本将自动安装及美化LS，CAT命令！完成后请关闭ssh再重新打开
    完成后输入ll，la，ls，lt，cat即可看到效果" 10 80) then beautifyLSCAT
    else CNmenu
    fi
  else # EN
    if (whiptail --title "beautify LS，CAT" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Install lsd and bat to PVE! Need to re-run ssh after installation!
    Type ll,la,ls,lt,cat to see the result!" 10 80) then beautifyLSCAT
    else ENmenu
    fi
  fi
}

# ------------------------ install BTOP ------------------------
installBTOP(){
  if [ $L = "cn" ];then # CN
  echo "======================================================================="
  echo "$(tput setaf 2)正在安装BTOP...$(tput sgr 0)"
  echo "======================================================================="
  else # EN
  echo "======================================================================="
  echo "$(tput setaf 2)Installing BTOP...$(tput sgr 0)"
  echo "======================================================================="
  fi

  # for app in python3-pip
  # do
  #   if [ `dpkg -s $app|grep Status|wc -l` = 1 ]; then echo "$(tput setaf 2)√ $app $(tput sgr 0)"
  #   else 
  #     apt install -y $app
  #   fi
  # done
  # pip3 install bpytop --upgrade
  mkdir /root/btop && cd /root/btop
  apt install -y sudo build-essential
  wget https://github.com/aristocratos/btop/releases/download/v1.2.0/btop-x86_64-linux-musl.tbz
  bzip2 -d btop-x86_64-linux-musl.tbz
  tar -xvf btop-x86_64-linux-musl.tar
  /root/btop/install.sh
  cd /root && rm -r /root/btop

  if [ $L = "cn" ];then # CN
  echo "======================================================================="
  echo "$(tput setaf 2)搞定！食用方式：btop$(tput sgr 0)"
  echo "======================================================================="
  else # EN
  echo "======================================================================="
  echo "$(tput setaf 2)Done! Type btop to start monitoring! $(tput sgr 0)"
  echo "======================================================================="
  fi
}
BTOPmenu(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "安装硬件监控" --yes-button "继续" --no-button "返回"  --yesno "
    此脚本将自动安装增强版硬件监控，安装完成后执行BTOP查看
    可实时查看cpu，内存，硬盘，频率，温度，占用率等" 10 80) then installBTOP
    else CNmenu
    fi
  else # EN
    if (whiptail --title "Install BTOP" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Install BTOP for real time hardware monitoring
    After installation run BTOP to see the result" 10 80) then installBTOP
    else ENmenu
    fi
  fi
}

# ----------------------- passthrough disk -----------------------
passthroDisk(){ # credits to https://github.com/ivanhao/pvetools
  if [ $L = "cn" ];then # CN
  list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
  echo -n "">lsvm
  ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
  ls=`cat lsvm`
  rm lsvm
  h=`echo $ls|wc -l`
  let h=$h*1
  if [ $h -lt 30 ];then
      h=30
  fi
  list1=`echo $list|awk 'NR>1{print $1}'`
  vmid=$(whiptail  --title "$ksh4pveToolversion" --menu "选择需要配置的虚拟机：" 20 60 10 \
      $(echo $ls) \
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if(whiptail --title "$ksh4pveToolversion" --yesno "你选的是：$vmid ，是否继续?" 8 60)then
              while [ true ]
              do
                  if [ `echo "$vmid"|grep "^[0-9]*$"|wc -l` = 0 ];then
                      whiptail --title "Warnning" --msgbox "输入格式错误，请重新输入：" 8 60
                      CNmenu
                  else
                      break
                  fi
              done
              if [ $1 = 'add' ];then
                  disks=`ls -alh /dev/disk/by-id|sed '/\.$/d'|sed '/^$/d'|awk 'NR>1{print $9" "$11" OFF"}'|sed 's/\.\.\///g'|sed '/wwn/d'|sed '/^dm/d'|sed '/lvm/d'|sed '/nvme-nvme/d'`
                  d=$(whiptail --title "$ksh4pveToolversion" --checklist "已添加的硬盘:
  $(cat /etc/pve/qemu-server/$vmid.conf|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2" "$3}')
  -----------------------
  选择硬盘：" 30 90 10 \
                  $(echo $disks) \
                  3>&1 1>&2 2>&3)
                  exitstatus=$?
                  t=$(whiptail --title "$ksh4pveToolversion" --menu "选择硬盘接口类型：" 18 60 10 \
                  "sata" "设置为sata类型" \
                  "scsi" "设置为scsi类型" \
                  "ide" "设置为ide类型" \
                  3>&1 1>&2 2>&3)
                  exits=$?
                  if [ $exitstatus = 0 ] && [ $exits = 0 ]; then
                      did=`qm config $vmid|sed -n '/^'$t'/p'|awk -F ':' '{print $1}'|sort -u -r|grep '[0-9]*$' -o|awk 'NR==1{print $0}'`
                      if [ $did ];then
                          did=$((did+1))
                      else
                          did=0
                      fi
                      d=`echo $d|sed 's/\"//g'`
                      for i in $d
                      do
                          if [ `cat /etc/pve/qemu-server/$vmid.conf|grep $i|wc -l` = 0 ];then
                              if [ $t = "ide" ] && [ $did -gt 3 ];then
                                  whiptail --title "Warnning" --msgbox "ide的类型已经超过3个,请重选其他类型!" 8 60
                                  CNmenu
                              else
                                  qm set $vmid --$t$did /dev/disk/by-id/$i
                              fi
                              sleep 1
                              did=$((did+1))
                          fi
                      done
                      whiptail --title "Success" --msgbox "配置完成!" 8 60
                      CNmenu
                  else
                      CNmenu
                  fi
              fi
              if [ $1 = 'rm' ];then
                  disks=`qm config $vmid|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2$3" OFF"}'`
                  d=$(whiptail --title "$ksh4pveToolversion" --checklist "选择需要移除的硬盘：" 20 90 10 \
                  $(echo $disks) \
                  3>&1 1>&2 2>&3)
                  exitstatus=$?
                  if [ $exitstatus = 0 ]; then
                      for i in $d
                      do
                          i=`echo $i|sed 's/\"//g'`
                          qm set $vmid --delete $i
                      done
                      whiptail --title "Success" --msgbox "配置完成!" 8 60
                      CNmenu
                  else
                      CNmenu
                  fi
              fi
          else
              CNmenu
          fi
      fi # end CN

  else # EN

  list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
  echo -n "">lsvm
  ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
  ls=`cat lsvm`
  rm lsvm
  h=`echo $ls|wc -l`
  let h=$h*1
  if [ $h -lt 30 ];then
      h=30
  fi
  list1=`echo $list|awk 'NR>1{print $1}'`
  vmid=$(whiptail  --title "$ksh4pveToolversion" --menu "Select a VM for disk passthrough" 20 60 10 \
      $(echo $ls) \
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if(whiptail --title "$ksh4pveToolversion" --yesno "You have selected $vmid, continue?" 8 60)then
              while [ true ]
              do
                  if [ `echo "$vmid"|grep "^[0-9]*$"|wc -l` = 0 ];then
                      whiptail --title "Warnning" --msgbox "Wrong format, abort!" 8 60
                      ENmenu
                  else
                      break
                  fi
              done
              if [ $1 = 'add' ];then
                  disks=`ls -alh /dev/disk/by-id|sed '/\.$/d'|sed '/^$/d'|awk 'NR>1{print $9" "$11" OFF"}'|sed 's/\.\.\///g'|sed '/wwn/d'|sed '/^dm/d'|sed '/lvm/d'|sed '/nvme-nvme/d'`
                  d=$(whiptail --title "$ksh4pveToolversion" --checklist "List of current VM disks:
  $(cat /etc/pve/qemu-server/$vmid.conf|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2" "$3}')
  -----------------------
  Select a disk type: " 30 90 10 \
                  $(echo $disks) \
                  3>&1 1>&2 2>&3)
                  exitstatus=$?
                  t=$(whiptail --title "$ksh4pveToolversion" --menu "Select disk type:" 18 60 10 \
                  "sata" "set to sata type" \
                  "scsi" "set to scsi type" \
                  "ide" "set to ide type" \
                  3>&1 1>&2 2>&3)
                  exits=$?
                  if [ $exitstatus = 0 ] && [ $exits = 0 ]; then
                      did=`qm config $vmid|sed -n '/^'$t'/p'|awk -F ':' '{print $1}'|sort -u -r|grep '[0-9]*$' -o|awk 'NR==1{print $0}'`
                      if [ $did ];then
                          did=$((did+1))
                      else
                          did=0
                      fi
                      d=`echo $d|sed 's/\"//g'`
                      for i in $d
                      do
                          if [ `cat /etc/pve/qemu-server/$vmid.conf|grep $i|wc -l` = 0 ];then
                              if [ $t = "ide" ] && [ $did -gt 3 ];then
                                  whiptail --title "Warnning" --msgbox "Can not assign more than 3 ide types!" 8 60
                                  ENmenu
                              else
                                  qm set $vmid --$t$did /dev/disk/by-id/$i
                              fi
                              sleep 1
                              did=$((did+1))
                          fi
                      done
                      whiptail --title "Success" --msgbox "Done!" 8 60
                      ENmenu
                  else
                      ENmenu
                  fi
              fi
              if [ $1 = 'rm' ];then
                  disks=`qm config $vmid|grep -E '^ide[0-9]|^scsi[0-9]|^sata[0-9]'|awk -F ":" '{print $1" "$2$3" OFF"}'`
                  d=$(whiptail --title "$ksh4pveToolversion" --checklist "Select a disk:" 20 90 10 \
                  $(echo $disks) \
                  3>&1 1>&2 2>&3)
                  exitstatus=$?
                  if [ $exitstatus = 0 ]; then
                      for i in $d
                      do
                          i=`echo $i|sed 's/\"//g'`
                          qm set $vmid --delete $i
                      done
                      whiptail --title "Success" --msgbox "Done!" 8 60
                      ENmenu
                  else
                      ENmenu
                  fi
              fi
          else
              ENmenu
          fi
      fi # end EN
  
  fi
}
diskMenu(){
  if [ $L = "cn" ];then # CN
  OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
  设置物理硬盘直通" 12 60 3 \
  "a" "添加硬盘直通" \
  "b" "删除硬盘直通 " \
  "q" "返回主菜单" \
  3>&1 1>&2 2>&3)
  case "$OPTION" in
  a ) passthroDisk add;;
  b ) passthroDisk rm;;
  q ) CNmenu;;
  esac
  else # EN
  OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
  Configure disk passthrough" 12 60 3 \
  "a" "Add disk passthrough to VM" \
  "b" "Remove disk from VM" \
  "q" "Go back to Menu" \
  3>&1 1>&2 2>&3)
  case "$OPTION" in
  a ) passthroDisk add;;
  b ) passthroDisk rm;;
  q ) ENmenu;;
  esac
  fi
}

# ---------------------- IOMMU driver check ----------------------
iommuGroupcheck(){ # credits to https://github.com/pavolelsig/IOMMU-viewer
  if [ $L = "cn" ];then # CN
    if (whiptail --title "IOMMU组 查看" --yes-button "继续" --no-button "返回"  --yesno "
    此脚本将自动检测主板IOMMU组设计，方便查看各种直通硬件对应的组编号
    脚本也会罗列出各硬件的ID编号及当前使用的内核驱动" 10 80) then
      GROUP=`find /sys/kernel/iommu_groups/ -type l | cut -d '/' -f 5,7 --output-delimiter='-'`
      for i in $GROUP; do
      k=`echo $i | cut -d '-' -f 1`
      l=`echo $i | cut -d '-' -f 2`
      j=`echo $i | cut -d ':' -f 2,3,4`
      m=`lspci -k -s $j | grep "Kernel driver in use"`		
      echo -n  "IOMMU组："
      if [ $k -lt 10 ]
      then
        echo -n " $k  "
      else
        echo -n " $k "
      fi
      echo -n " $l "
      echo -n "`lspci -nn | grep $j | cut -d ' ' -f 2-`"		
      if ! [ -z "$m" ]
      then
        echo -n "   内核驱动："
      fi	
      echo "$m" | cut -d ':' -f 2
      done | sort -nk2
    else
      CNmenu
    fi
  else # EN
    if (whiptail --title "IOMMU Check" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Script lists all IOMMU group based for motherboard design
    Easy to see what's the Hardware ID, what kernel driver being used" 10 80) then
      GROUP=`find /sys/kernel/iommu_groups/ -type l | cut -d '/' -f 5,7 --output-delimiter='-'`
      for i in $GROUP; do
      k=`echo $i | cut -d '-' -f 1`
      l=`echo $i | cut -d '-' -f 2`
      j=`echo $i | cut -d ':' -f 2,3,4`
      m=`lspci -k -s $j | grep "Kernel driver in use"`		
      echo -n  "IommuGroup: "
      if [ $k -lt 10 ]
      then
        echo -n " $k  "
      else
        echo -n " $k "
      fi
      echo -n " $l "
      echo -n "`lspci -nn | grep $j | cut -d ' ' -f 2-`"		
      if ! [ -z "$m" ]
      then
        echo -n "   Driver: "
      fi	
      echo "$m" | cut -d ':' -f 2
      done | sort -nk2
    else
      ENmenu
    fi
  fi
}

# ------------ # breaking iommu by pcie_acs_override -------------
extraGrubsettings(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "彻底分离IOMMU组" --yes-button "继续" --no-button "返回"  --yesno "
    请谨慎执行该脚本，如果你并未碰到直通困难的硬件，请勿运行该脚本
    当你碰到某硬件无法直通时可尝试执行该脚本，以分离出更合理的IOMMU组" 10 80) then
      if [ `grep "pcie_acs_override" /etc/default/grub | wc -l` = 0 ];then
      sed -i 's#iommu=pt#iommu=pt pcie_acs_override=downstream,multifunction vfio_iommu_type1.allow_unsafe_interrupts=1#' /etc/default/grub && update-grub
      echo "$(tput setaf 2)√ 已开启额外IOMMU参数，请重新启动！$(tput sgr 0)"
      else echo "$(tput setaf 2)√ 已有额外IOMMU参数，无需设置！ √$(tput sgr 0)"
      fi
    else CNmenu
    fi
  else # EN
    if (whiptail --title "Breaking IOMMU" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Apply this script when you need pcie_acs_override
    Run script when you have trouble in hardware passthro" 10 80) then
      if [ `grep "pcie_acs_override" /etc/default/grub | wc -l` = 0 ];then
      sed -i 's#iommu=pt#iommu=pt pcie_acs_override=downstream,multifunction vfio_iommu_type1.allow_unsafe_interrupts=1#' /etc/default/grub && update-grub
      echo "$(tput setaf 2)√ Done pcie_acs_override, please reboot! $(tput sgr 0)"
      else echo "$(tput setaf 2)√ Already have pcie_acs_override enabled, skip it! √$(tput sgr 0)"
      fi
    else ENmenu
    fi
  fi
}

# ------------------------ GPU passthrough -----------------------
gpuPassthrough(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "物理显卡直通" --yes-button "继续" --no-button "返回"  --yesno "一键物理显卡直通到指定虚拟机，无脑便捷！脚本暂只支持N卡直通！
      务必确保虚拟机采用Q35类型且为SeaBIOS！注意：直通后的N卡win10驱动版本需大于471.11" 10 85) then
      read pciID < <(
        declare -a array=()
        while read id foo{,,,} dsc;do
          array+=($id "$dsc")
        done < <(lspci -nn | grep -i VGA)
        whiptail --title "$ksh4pveToolversion" --menu '以下为当前主板已插入的物理显卡，请选择相应显卡，回车继续！' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )
      deviceID=$(echo $pciID | awk -F "." '{print $1}')
      # user selected VM's ID
      clear
      list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
      echo -n "">lsvm
      ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
      ls=`cat lsvm`
      rm lsvm
      h=`echo $ls|wc -l`
      let h=$h*1
      if [ $h -lt 30 ];then
          h=30
      fi
      list1=`echo $list|awk 'NR>1{print $1}'`
      vmid=$(whiptail  --title "$ksh4pveToolversion" --menu "
      选择虚拟机：" 20 60 10 \
      $(echo $ls) \
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 9999 -a "$vmid" -ge 100 ]; then
          # first delete any vGPU conf if any
          sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/-uuid/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/#vGPU added/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/$vmid.conf
          # assign selected GPU to VM, example: hostpci0: 0000:09:00,pcie=1
          qm set $vmid -hostpci0 0000:$deviceID,pcie=1
          echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
        else
        CNmenu
        fi
      fi

    else CNmenu
    fi
  else # EN
    # user selected GPU pcid with all functions
    if (whiptail --title "GPU Passthrough" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Support Nvidia ONLY! Make sure VM have Q35 and SeaBIOS configured. 
      For guest Win10 driver version shoudld larger than 471.11" 10 90) then
      read pciID < <(
        declare -a array=()
        while read id foo{,,,} dsc;do
          array+=($id "$dsc")
        done < <(lspci -nn | grep -i VGA)
        whiptail --title "$ksh4pveToolversion" --menu 'Select a GPU you want to passthrough' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )
      deviceID=$(echo $pciID | awk -F "." '{print $1}')
      # user selected VM's ID
      clear
      list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
      echo -n "">lsvm
      ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
      ls=`cat lsvm`
      rm lsvm
      h=`echo $ls|wc -l`
      let h=$h*1
      if [ $h -lt 30 ];then
          h=30
      fi
      list1=`echo $list|awk 'NR>1{print $1}'`
      vmid=$(whiptail  --title "$ksh4pveToolversion" --menu "
      Please select a VM: " 20 60 10 \
      $(echo $ls) \
      3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 9999 -a "$vmid" -ge 100 ]; then
          # first delete any vGPU conf if any
          sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/-uuid/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/#vGPU added/d' /etc/pve/qemu-server/$vmid.conf
          sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/$vmid.conf
          # assign selected GPU to VM, example: hostpci0: 0000:09:00,pcie=1
          qm set $vmid -hostpci0 0000:$deviceID,pcie=1
          echo "$(tput setaf 2)Done! Please check PVE webgui! $(tput setaf 0)"
        else ENmenu
        fi
      fi
      else ENmenu
    fi
  fi # end EN language
}

# ------------------------ Intel GVT-G vGPU ----------------------
intelGVTmenu(){
  runIntelGVT(){
    clear
    if [ $1 = 'enable' ];then
      # adding extra grub settings
      if [ `grep "enable_gvt" /etc/default/grub | wc -l` = 0 ];then
          sed -i 's#iommu=pt#iommu=pt i915.enable_gvt=1#' /etc/default/grub && update-grub
          if [ $L = "cn" ];then # CN
          echo "$(tput setaf 2)已开启GVT-G切分！√$(tput sgr 0)"
          else # EN
          echo "$(tput setaf 2)Intel GVT-G added! √$(tput sgr 0)"
          fi
        else
          if [ $L = "cn" ];then # CN
          echo "$(tput setaf 2)已开启GVT-G切分，无需重复设置！√$(tput sgr 0)"
          else # EN
          echo "$(tput setaf 2)GVT-G already added, skip it! √$(tput sgr 0)"
          fi
      fi

      # adding extra vfio modules
      if [ `grep "kvmgt" /etc/modules|wc -l` = 0 ];then
        echo -e "kvmgt\n#exngt\nvfio-mdev" >> /etc/modules
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已添加GVT-G模组！√$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)GVT-G module satisfied! √$(tput sgr 0)"
        fi
        else
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已添加GVT-G模组，无需重复设置！√$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)GVT-G module already satisfied! √$(tput sgr 0)"
        fi
      fi

      # blacklist nouveau
      if [ `grep "nouveau" /etc/modprobe.d/blacklist.conf|wc -l` = 0 ];then
        echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf && update-initramfs -u -k all
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已添加VFIO模组！√$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)vfio satisfied! √$(tput sgr 0)"
        fi
        else
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已屏蔽nouveau驱动，无需重复设置！ √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)nouveau already satisfied! √$(tput sgr 0)"
        fi
      fi

      # echo
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)设置完毕！请到网页端->虚拟机id->硬件->PCI设备->添加你希望的类型$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)Done! Go to PVE webGui->VMid->Hardward->add PCI device->choose your desire GVT-G type$(tput setaf 0)"
      fi
      tput setaf 0
    fi

    if [ $1 = 'disable' ];then
      # delete extra grub settings
      sed -i 's#iommu=pt i915.enable_gvt=1#iommu=pt#' /etc/default/grub
      update-grub

      # delete extra vfio modules
      sed -i '/kvmgt/d' /etc/modules
      sed -i '/#exngt/d' /etc/modules
      sed -i '/vfio-mdev/d' /etc/modules

      # update initramfs
      update-initramfs -u -k all

      # echo
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)已关闭GVT-G！请重启宿主机！$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)GVT-G disabled! ! Please reboot!"
      fi  
      tput setaf 0
    fi
  }
  
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
    免费vGPU切分，仅适用于Intel核显vGPU切分，支持UHD510-630等型号" 14 80 5 \
    "a" "开启Intel核显切分" \
    "b" "关闭Intel核显切分" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) runIntelGVT enable;;
    b ) runIntelGVT disable;;
    q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
    Free GVT-G vGPU Slicing! Support from UHD510 to UHD630! Intel ONLY!" 14 80 5 \
    "a" "Enable Intel GVT-G" \
    "b" "Disable iGPU GVT-G" \
    "q" "Go back to Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) runIntelGVT enable;;
    b ) runIntelGVT disable;;
    q ) ENmenu;;
    esac
  fi
}

# ---------------------- remove pop-up window --------------------
removeSubs(){
  libFire="/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js"
  runremove(){
    if [ $1 = 'remove' ];then
      sed -i.backup -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" ${libFire}
      systemctl restart pveproxy.service
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)搞定！请刷新网页查看！$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)Done! Please Ctrl+F5 refresh WebGUI! $(tput setaf 0)"
      fi
    fi

    if [ $1 = 'bringback' ];then
      rm ${libFire}
      cp /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.backup ${libFire}
      rm /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.backup
      systemctl restart pveproxy.service
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)已恢复！请刷新网页查看！$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)Done undo! Please Ctrl+F5 refresh WebGUI! $(tput setaf 0)"
      fi
    fi
  }

  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " 移除订阅弹窗 " --menu "
    此脚本将自动自动移除订阅弹窗，适用于PVE6.2-15及以上版本，包括最新版
    请注意，移除后可能会造成系统死机，如没有特殊需求，不推荐移除订阅弹窗" 15 80 5 \
    "a" "移除弹窗订阅" \
    "b" "恢复弹窗订阅" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) runremove remove;;
    b ) runremove bringback;;
    q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title " Remove popup window " --menu "
    Script will remove none-subscription pop-up window durling login.
    Please be aware of removing popup window may cause PVE crashed." 15 80 5 \
    "a" "Remove pop-up window" \
    "b" "Recover pop-up window" \
    "q" "Go Back to Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) runremove remove;;
    b ) runremove bringback;;
    q ) ENmenu;;
    esac
  fi
}

# ---------------------- cpufreq performance ---------------------
cpuPerformance(){
  runCPUAdjust(){
    if [ $1 = 'install' ];then

      # adding cpu freq quick check
      if [ `grep "alias cpu" ~/.bashrc|wc -l` = 0 ];then
        echo "alias cpu='watch -n1 grep MHz /proc/cpuinfo'" >> ~/.bashrc
        exec "$BASH"
      fi

      if [ $L = "cn" ];then # CN
        if (whiptail --title "安装CPU模式调节工具" --yes-button "继续" --no-button "返回"  --yesno "
        即将安装CPU模式调节工具，回车继续！" 10 60) then
          if [ `grep "intel_pstate=disable" /etc/default/grub|wc -l` = 0 ];then
          sed -i 's|quiet|quiet intel_pstate=disable|' /etc/default/grub
          update-grub
          apt install -y cpufrequtils
      if (whiptail --title "是否重启?" --yes-button "是！重启！" --no-button "返回"  --yesno "
      搞定！请重启宿主机，回车将立即重启系统，是否继续？" 10 60) then reboot
      else exit
      fi
          else
          whiptail --title "Warnning" --msgbox "  CPU性能调节工具已存在，无需重复安装！" 8 43
          CNmenu
          fi
        fi
      else # EN
        if (whiptail --title "cpufrequtils" --yes-button "Continue" --no-button "Go Back"  --yesno "
          Install cpufrequtils tools?" 10 60) then
            if [ `grep "intel_pstate=disable" /etc/default/grub|wc -l` = 0 ];then
            sed -i 's|quiet|quiet intel_pstate=disable|' /etc/default/grub
            update-grub
            apt install -y cpufrequtils
      if (whiptail --title "Reboot host?" --yes-button "Reboot host!" --no-button "Go Back"  --yesno "
      Done! Please reboot host, cotinue to reboot?" 10 60) then reboot
      else exit
      fi
          else
          whiptail --title "Warnning" --msgbox "  CPUfreq already installed! Skip it! " 8 43
          ENmenu
          fi
        fi
      fi # end language
    fi # end install

    if [ $1 = 'save' ];then
      if [ $L = "cn" ];then # CN
        if (whiptail --title "CPU 省电模式" --yes-button "继续" --no-button "返回"  --yesno "
        省电模式将主频锁定在最低频运行，工作时频率不再跳动，节能环保低温！" 10 85) then
          if [ `grep 'GOVERNOR="powersave"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="powersave"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)已设置成省电模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
            else echo "$(tput setaf 2)已经是省电模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
          fi
          else
          CNmenu
        fi
      else # EN
        if (whiptail --title "CPU PowerSave mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
        PowerSave mode locks CPU down to lowest frequency even with heavy load." 10 85) then
          if [ `grep 'GOVERNOR="powersave"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="powersave"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)CPU mode is set to PowerSave mode!$(tput setaf 0)"
            echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
            else echo "$(tput setaf 2)Already in PowerSave mode, skip it!$(tput setaf 0)"
          fi
          else
          ENmenu
        fi
      fi # end language
    fi # end save

    if [ $1 = 'ondemand' ];then
      if [ $L = "cn" ];then # CN
          if (whiptail --title "CPU 自适应模式" --yes-button "继续" --no-button "返回"  --yesno "
            自适应模式随系统繁忙程度而频繁跳动频率，低负载时也会让CPU主频以高频率运行！" 10 85) then
          if [ `grep 'GOVERNOR="ondemand"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="ondemand"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)已设置成自适应模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
            else echo "$(tput setaf 2)已经是自适应模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
          fi
          else
          CNmenu
        fi
      else # EN
          if (whiptail --title "CPU ondemand mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Ondemand mode dynamically adjusts CPU frequency according system load." 10 85) then
          if [ `grep 'GOVERNOR="ondemand"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="ondemand"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)CPU mode is set to ondemand mode!$(tput setaf 0)"
            echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
            else echo "$(tput setaf 2)Already in ondemand mode, skip it!$(tput setaf 0)"
          fi
          else
          ENmenu
        fi
      fi # end language
    fi # ondemand

    if [ $1 = 'performance' ];then
      if [ $L = "cn" ];then # CN
          if (whiptail --title "CPU 高性能模式" --yes-button "继续" --no-button "返回"  --yesno "
            高性能模式将锁定CPU到最高频率，即便系统零负载！该模式为PVE默认，性能最高！" 10 85) then
          if [ `grep 'GOVERNOR="performance"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="performance"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)已设置成高性能模式！请运行cpu快速查看实时频率！$(tput setaf 0)"
            else echo "$(tput setaf 2)已经是高性能模式，请运行cpu快速查看实时频率！$(tput setaf 0)"
          fi
          else
          CNmenu
        fi
      else # EN
          if (whiptail --title "CPU performance mode" --yes-button "Continue" --no-button "Go Back"  --yesno "
      performance mode locks CPU to highest frequency even low system load." 10 85) then
          if [ `grep 'GOVERNOR="performance"' /etc/init.d/cpufrequtils | wc -l` = 0 ];then
            sed -i '/GOVERNOR=/d' /etc/init.d/cpufrequtils
            sed -i '43iGOVERNOR="performance"' /etc/init.d/cpufrequtils
            systemctl daemon-reload
            /etc/init.d/cpufrequtils restart
            clear
            echo "$(tput setaf 2)CPU mode is set to performance mode!$(tput setaf 0)"
            echo "$(tput setaf 2)Please run <cpu> to check real time CPU freq!$(tput setaf 0)"
            else echo "$(tput setaf 2)Already in performance mode, skip it!$(tput setaf 0)"
          fi
          else
          ENmenu
        fi
      fi # end language
    fi # end performance
  }
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " CPU性能调整 " --menu "
    此脚本将自动设置CPU性能，PVE默认为高性能（不降频）模式!
    请先安装调整工具，再选择模式，支持无缝切换无需重启！" 15 80 5 \
    "0" "安装CPU模式调整工具" \
    "a" "省电模式" \
    "b" "自适应模式" \
    "c" "高性能模式" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    0 ) runCPUAdjust install;;
    a ) runCPUAdjust save;;
    b ) runCPUAdjust ondemand;;
    c ) runCPUAdjust performance;;
    q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title " CPU performance adjust tool " --menu "
    Adjusting CPU performance mode, by default PVE sets to Performance
    Please install CPUfreq util first, then select desire mode" 15 80 5 \
    "0" "First install CPUfrequtils" \
    "a" "Power Saving mode" \
    "b" "Power Ondemand mode" \
    "c" "High Performance mode" \
    "q" "Go Back to menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    0 ) runCPUAdjust install;;
    a ) runCPUAdjust save;;
    b ) runCPUAdjust ondemand;;
    c ) runCPUAdjust performance;;
    q ) ENmenu;;
    esac
  fi
}

# --------------------- PVE dark theme install -------------------
darkTheme(){ # https://github.com/Weilbyte/PVEDiscordDark
  rundarkTheme(){
    if [ $1 = 'install' ];then
      bash <(curl -s https://raw.githubusercontent.com/Weilbyte/PVEDiscordDark/master/PVEDiscordDark.sh ) install
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)已安装，请刷新网页查看！$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)Done! Please refresh Webgui!$(tput setaf 0)"
      fi
    fi

    if [ $1 = 'uninstall' ];then
      bash <(curl -s https://raw.githubusercontent.com/Weilbyte/PVEDiscordDark/master/PVEDiscordDark.sh ) uninstall
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)已卸载主题，请刷新网页查看！$(tput setaf 0)"
      else # EN
      echo "$(tput setaf 2)Done uninstall! Please refresh Webgui! $(tput setaf 0)"
      fi
    fi
  }
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " PVE暗黑主题 " --menu "
    此脚本将自动安装一套非常漂亮的PVE暗黑主题" 12 80 3 \
    "a" "安装主题" \
    "b" "卸载主题" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) rundarkTheme install;;
    b ) rundarkTheme uninstall;;
    q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title " PVE Dark Theme " --menu "
    Script will auto install PVE dark theme" 12 80 3 \
    "a" "install dark theme" \
    "b" "remove dark theme" \
    "q" "Go Back to menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) rundarkTheme install;;
    b ) rundarkTheme uninstall;;
    q ) ENmenu;;
    esac
  fi
}

# ----------------------- China mainland repo --------------------
changeSource(){
    if (whiptail --title "更换APT源" --yes-button "继续" --no-button "返回"  --yesno "
      此脚本将更改PVE源为国内服务器，以获得更快的下载更新速度" 10 80) then
        if [ `grep "ustc.edu.cn" /etc/apt/sources.list|wc -l` = 0 ];then
          cp /etc/apt/sources.list /etc/apt/sources.list.bak
echo "deb https://mirrors.ustc.edu.cn/debian/ $pveRepo main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo main contrib non-free
deb https://mirrors.ustc.edu.cn/debian/ $pveRepo-updates main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo-updates main contrib non-free
deb https://mirrors.ustc.edu.cn/debian/ $pveRepo-backports main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian/ $pveRepo-backports main contrib non-free
deb https://mirrors.ustc.edu.cn/debian-security/ $pveRepo/updates main contrib non-free
deb-src https://mirrors.ustc.edu.cn/debian-security/ $pveRepo/updates main contrib non-free" > /etc/apt/sources.list

      # for pve7 repo
      if [ $pveRepo = "bullseye" ];then
        sed -i 's/buster\/updates/bullseye-security/g;s/buster/bullseye/g' /etc/apt/sources.list
      fi
      echo "$(tput setaf 2)更换国内源成功！开始更新... $(tput setaf 0)"
      apt update && apt upgrade -y
      echo "$(tput setaf 2)完成更新！$(tput setaf 0)"
      else echo "$(tput setaf 2)已经是国内源！无需重复设置！$(tput setaf 0)"
      fi
    else # back to menu
    CNmenu
  fi
}

# --------------------- One click VM deployment ------------------
VMdeployMenu(){

  DSMid="666"
  DSM3615id="333"
  DSM918id="222"
  UNRAIDid="555"
  OpenWRTid="777"
  MacOSid="888"
  isoFolder="/var/lib/vz/template/iso"

  deployDSM(){ # deploy DSM vm
    if [ $1 = '3617' ];then # dsm6
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署DSM" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为666，名字为DSM3617的虚拟机
      支持半虚拟化网卡，默认创建时CPU为1个，内存1G，DSM版本为6.1
      请复制以下地址到浏览器下载pat安装包，然后回车继续（国内直连下载）
      http://down.nas2x.com/synology/dsm/6.1/6.1.6/ds3617xs/DSM_DS3617xs_15266.pat" 12 90) then

      # creates a VM
      clear
      echo "正在设置虚拟机，请稍候！"
      qm create $DSMid -name DSM3617 -net0 virtio,bridge=vmbr0,firewall=1 -ostype win10 -memory 1024

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3617xs_DSM6.1_Broadwell.img" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3617xs_DSM6.1_Broadwell.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      /opt/img2kvm /opt/DS3617xs_DSM6.1_Broadwell.img $DSMid local-lvm
      qm set $DSMid -ide0 local-lvm:vm-$DSMid-disk-0
      qm set $DSMid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
      else VMdeployMenu
      fi
    else # EN
      if (whiptail --title "deploy DSM" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script auto deploys a VM(ID=666, name=DSM3617). Support paravirtualized.
      By default it has CPU=1, memory=1G, no extra hard disk, DSM version is 6.1
      
      First of all please download the pat file by this link:
      http://down.nas2x.com/synology/dsm/6.1/6.1.6/ds3617xs/DSM_DS3617xs_15266.pat" 12 90) then

      # read storage < <(
      #     declare -a array=()
      #     while read id foo{,,,,} dsc;do
      #     array+=($id "$dsc")
      #     done < <(pvesm status | sed -n '1!p')
      #     whiptail --title "$ksh4pveToolversion" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      # )

      # creates a VM
      clear
      echo "Setting up VM, please wait..."
      qm create $DSMid -name DSM3617 -net0 virtio,bridge=vmbr0,firewall=1 -ostype win10 -memory 1024

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3617xs_DSM6.1_Broadwell.img" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3617xs_DSM6.1_Broadwell.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      # /opt/img2kvm /opt/DS3617xs_DSM6.1_Broadwell.img $DSMid $storage
      # qm set $DSMid -ide0 $storage:vm-$DSMid-disk-0
      /opt/img2kvm /opt/DS3617xs_DSM6.1_Broadwell.img $DSMid local-lvm
      qm set $DSMid -ide0 local-lvm:vm-$DSMid-disk-0
      qm set $DSMid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"
      else VMdeployMenu
      fi
    fi # end language
    fi # end $1 3617

    if [ $1 = '3615' ];then # dsm7
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署DSM7.0" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为333，名字为DSM3615的虚拟机
      支持半虚拟化网卡，默认创建时CPU为1个，内存1G，DSM版本为7.0.1" 12 75) then

      # creates a VM
      clear
      echo "正在设置虚拟机，请稍候！"
      qm create $DSM3615id -name DSM3615 -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -scsihw virtio-scsi-pci

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3615xs+LSI_7.0.1-42218.img" ];then
        # https://cndl.synology.cn/download/DSM/release/7.0.1/42218/DSM_DS3615xs_42218.pat
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3615xs%2BLSI_7.0.1-42218.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      /opt/img2kvm /opt/DS3615xs+LSI_7.0.1-42218.img $DSM3615id local-lvm
      qm set $DSM3615id -ide0 local-lvm:vm-$DSM3615id-disk-0
      qm set $DSM3615id -boot order=ide0

      cd /root/
      echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
      else VMdeployMenu
      fi
    else # EN
      if (whiptail --title "deploy DSM 7.0" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script auto deploys a VM(ID=333, name=DSM3615). Support paravirtualized.
      By default it has CPU=1, memory=1G, no extra hard disk, DSM version is 7.0.1" 12 90) then

      # creates a VM
      clear
      echo "Setting up VM, please wait..."
      qm create $DSM3615id -name DSM3615 -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -scsihw virtio-scsi-pci

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/DS3615xs+LSI_7.0.1-42218.img" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/DS3615xs%2BLSI_7.0.1-42218.img -P /opt/
      fi
      chmod +x /opt/img2kvm
      /opt/img2kvm /opt/DS3615xs+LSI_7.0.1-42218.img $DSM3615id local-lvm
      qm set $DSM3615id -ide0 local-lvm:vm-$DSM3615id-disk-0
      qm set $DSM3615id -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"
      else VMdeployMenu
      fi
    fi # end language
    fi # end $1 3615

    if [ $1 = '918' ];then # dsm7
      if (whiptail --title "$ksh4pveToolversion" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为222，名字为ksh918的虚拟机, DSM版本为7.0.1
      支持半虚拟化网卡，默认CPU=1，内存=1G，无硬盘! 请自行直通物理硬盘!" 10 80) then

      IMG918="$isoFolder/DS918+_7.0.1-42218.img"
      if [ $L = "cn" ]; then echo "正在创建虚拟机..."; else echo "Deploying VM..."; fi
      qm create $DSM918id -name ksh918 -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -scsihw virtio-scsi-pci -serial0 socket --machine q35 --cpu host

      # select hard disk pci devices
      if [ $L = "cn" ]; then echo "准备设置硬盘设备直通..."; else echo "Prepare for hard drive passthrough..."; fi; sleep 1; echo "2"; sleep 1; echo "1"; sleep 1
      if [ $L = "cn" ]; then
        if (whiptail --title "$ksh4pveToolversion" --yes-button "是的" --no-button "跳过"  --yesno "     是否需要直通硬盘pci等设备?" 8 40) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
          array+=($id "$dsc")
          done < <(lspci | egrep -i 'sata|sas|non-volatile')
          whiptail --title "$ksh4pveToolversion" --menu "       以下罗列出当前主板上的板载sata控制器, 请选择相应的pci设备进行直通: " 14 95 3 "${array[@]}" 2>&1 >/dev/tty
        )
        PCIdevs=$(echo $pciID | awk '{print $1}')
        echo "hostpci5: 0000:$PCIdevs" >> /etc/pve/qemu-server/$DSM918id.conf
        fi
      else # EN
        if (whiptail --title "$ksh4pveToolversion" --yes-button "Yes please" --no-button "No skip it"  --yesno "Do you want to passthrough a SATA controller?" 8 50) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
          array+=($id "$dsc")
          done < <(lspci | egrep -i 'sata|sas|non-volatile')
          whiptail --title "$ksh4pveToolversion" --menu "                           Please select an onboard SATA controller" 14 95 3 "${array[@]}" 2>&1 >/dev/tty
        )
        PCIdevs=$(echo $pciID | awk '{print $1}')
        echo "hostpci5: 0000:$PCIdevs" >> /etc/pve/qemu-server/$DSM918id.conf
        fi
      fi

      # select GPU devices
      if [ $L = "cn" ]; then echo "准备设置显卡直通..."; else echo "Prepare for GPU passthrough..."; fi; sleep 1; echo "2"; sleep 1; echo "1"; sleep 1
      if [ $L = "cn" ]; then
        if (whiptail --title "$ksh4pveToolversion" --yes-button "是的" --no-button "跳过"  --yesno "     是否需要直通物理显卡设备?" 8 40) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
            array+=($id "$dsc")
          done < <(lspci -nn | grep -i VGA)
          whiptail --title "$ksh4pveToolversion" --menu '以下罗列出当前主板上的物理显卡，请选择相应显卡，回车继续！' 14 95 3 "${array[@]}" 2>&1 >/dev/tty
        )
        deviceID=$(echo $pciID | awk -F "." '{print $1}')
        echo "hostpci6: 0000:$deviceID" >> /etc/pve/qemu-server/$DSM918id.conf
        fi
      else # EN
        if (whiptail --title "$ksh4pveToolversion" --yes-button "Yes" --no-button "No skip it"  --yesno "          Do you want to passthrough a GPU?" 8 60) then
        read pciID < <(
          declare -a array=()
          while read id foo{,,,} dsc;do
            array+=($id "$dsc")
          done < <(lspci -nn | grep -i VGA)
          whiptail --title "$ksh4pveToolversion" --menu 'Select a GPU to passing through' 14 95 3 "${array[@]}" 2>&1 >/dev/tty
        )
        deviceID=$(echo $pciID | awk -F "." '{print $1}')
        echo "hostpci6: 0000:$deviceID" >> /etc/pve/qemu-server/$DSM918id.conf
        fi
      fi

      if [ ! -f "$IMG918" ];then wget https://f000.backblazeb2.com/file/vGPUdrivers/DS918%2B_7.0.1-42218.img -P "$isoFolder"; fi
      echo "args: -device 'qemu-xhci,addr=0x18' -drive 'id=synoboot,file=$IMG918,if=none,format=raw' -device 'usb-storage,id=synoboot,drive=synoboot,bootindex=5'" >> /etc/pve/qemu-server/$DSM918id.conf
      if [ $L = "cn" ]; then echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"; else echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"; fi; tput setaf 0
      else VMdeployMenu
      fi # end whiptail
    fi # end $1 918
  }

  deployUnraid(){ # deploy a Unraid vm
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署UNRAID" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为555，名字为Unraid的虚拟机，支持半虚拟化网卡！
      默认创建时CPU为1个，内存1G，无硬盘！该虚拟机将通过U盘启动进入Unraid系统！
      运行之前请务必将Unraid的启动U盘插到PVE宿主机主板上，再回车继续！" 12 90) then
      # user select unraid usb drive
        read usbdev < <(
        declare -a array=()
        while read foo{,,,,} id dsc;do
          array+=($id "$dsc")
        done < <(lsusb)
        whiptail --menu '以下为当前主板上已插入的各类usb设备，请选择unriad的启动u盘，回车继续！' 20 80 10 "${array[@]}" 2>&1 >/dev/tty)
        echo $usbdev
      else VMdeployMenu
      fi
      # if $usbdev is not null then create unraid VM
      if [ -n "${usbdev}" ];then
        echo "$(tput setaf 2)正在设置虚拟机，请稍候！$(tput setaf 0)"
        qm create $UNRAIDid -name Unraid -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -usb0 host=$usbdev
        qm set $UNRAIDid -boot order=usb0
        # echo done
        echo "$(tput setaf 2)搞定！请到网页端查看！$(tput setaf 0)"
      fi
    else # EN
      if (whiptail --title "Deploy UNRAID VM" --yes-button "Continue" --no-button "Go Back"  --yesno "
      Script auto deploys a VM(ID=555, name=Unraid). Support paravirtualized.
      By default it has CPU=1, memory=1G, no hard disk.
      You need to prepare an Unraid usb drive to boot into the system." 12 90) then
      # user select unraid usb drive
        read usbdev < <(
        declare -a array=()
        while read foo{,,,,} id dsc;do
          array+=($id "$dsc")
        done < <(lsusb)
        whiptail --menu 'List of current USB drive on your motherboard: ' 20 80 10 "${array[@]}" 2>&1 >/dev/tty)
        echo $usbdev
      else VMdeployMenu
      fi
      # if $usbdev is not null then create unraid VM
      if [ -n "${usbdev}" ];then
        echo "$(tput setaf 2)Setting up VM, please wait...$(tput setaf 0)"
        qm create $UNRAIDid -name Unraid -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024 -usb0 host=$usbdev
        qm set $UNRAIDid -boot order=usb0
        # echo done
        echo "$(tput setaf 2)Done! Check the result by going to PVE webgui! $(tput setaf 0)"
      fi
    fi
  }

  deployOpenWRT(){ # deploy a openwrt vm
    if [ $L = "cn" ];then # CN
      if (whiptail --title "部署OpenWRT" --yes-button "继续" --no-button "返回"  --yesno "
      无脑全自动部署一台ID为777，名字为OpenWRT的软路由虚拟机，固件为esir精品小包v7
      支持半虚拟化网卡，默认创建时CPU为1个，内存1G！运行后默认ip为192.168.5.1无密码" 12 90) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "$ksh4pveToolversion" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      # creates a VM
      clear
      echo "$(tput setaf 2)正在设置虚拟机，请稍候！$(tput setaf 0)"
      qm create $OpenWRTid -name OpenWRT -net0 virtio,bridge=vmbr0,firewall=1 -ostype l26 -memory 1024
      tput setaf 6

      # pull img2kvm then convert img to disk
      cd /opt
      if [ ! -f "/opt/img2kvm" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/img2kvm -P /opt/
      fi
      if [ ! -f "/opt/openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img.gz" ];then
        wget https://f000.backblazeb2.com/file/vGPUdrivers/openwrt-spp-v7-1%5B2021%5D-x86-64-generic-squashfs-legacy.img.gz -P /opt/
      fi
      chmod +x /opt/img2kvm
      gzip -dfk openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img.gz
      /opt/img2kvm /opt/openwrt-spp-v7-1[2021]-x86-64-generic-squashfs-legacy.img $OpenWRTid $storage
      qm set $OpenWRTid -ide0 $storage:vm-$OpenWRTid-disk-0
      qm set $OpenWRTid -boot order=ide0

      # echo done
      cd /root/
      echo "$(tput setaf 2)搞定！请到网页端查看！支持半虚拟化网卡，默认创建时CPU为1个，内存1G！运行后默认ip为192.168.5.1无密码！$(tput setaf 0)"
      echo "$(tput setaf 2)如需修改ip请在控制台运行如下命令进行修改：$(tput setaf 0)"
      echo "$(tput setaf 2)sed -i 's|192.168.5.1|x.x.x.x|' /etc/config/network （替换x.x.x.x为你希望设置的IP即可）$(tput setaf 0)"
      else VMdeployMenu
      fi
    else # EN
      echo "no openwrt vm for english version"
    fi # end language
  }

  deployMacOS(){ # deploy a MacOS vm
    # igpu passthro ref
    # https://www.reddit.com/r/VFIO/comments/innriq/successful_macos_catalina_with_intel_gvtg/
    # https://forum.proxmox.com/threads/host-crash-and-reboot-everytime-running-multiple-vm-with-gpu-and-igpu-passthrough.83874/

    # install ref
    # https://zhuanlan.zhihu.com/p/240710958
    runMacOSvm(){
      clear
      cd /opt/
      echo "Downloading MacOS BaseSystem dmg ...正在下载基础镜像..."
      if [ ! -f /opt/BaseSystem.dmg ];then
        if [ -f /opt/fetch-macOS-v2.py ];then
          chmod +x /opt/fetch-macOS-v2.py
          if [ $1 = 'Sierra' ];then
            echo "1" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Mojave' ];then
            echo "2" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Catalina' ];then
            echo "3" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'BigSur' ];then
            echo "4" | /opt/fetch-macOS-v2.py
          fi
        else
          wget https://f000.backblazeb2.com/file/vGPUdrivers/fetch-macOS-v2.py # CN
          # wget https://raw.githubusercontent.com/kholia/OSX-KVM/master/fetch-macOS-v2.py # EN
          chmod +x /opt/fetch-macOS-v2.py
          if [ $1 = 'Sierra' ];then
            echo "1" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Mojave' ];then
            echo "2" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'Catalina' ];then
            echo "3" | /opt/fetch-macOS-v2.py
          fi
          if [ $1 = 'BigSur' ];then
            echo "4" | /opt/fetch-macOS-v2.py
          fi
        fi
      fi

      if [ $L = "cn" ];then # CN
      echo "正在转换格式..."
      else # EN
      echo "Converting installer iso ..."
      fi
      if [ ! -f $isoFolder/$1-installer.iso ];then
          qemu-img convert BaseSystem.dmg -O raw $1-installer.iso
          mv /opt/$1-installer.iso $isoFolder/
          rm /opt/BaseSystem.dmg
          rm /opt/BaseSystem.chunklist
      fi

      if [ $L = "cn" ];then # CN
      echo "正在下载引导镜像..."
      else # EN
      echo "Downloading OpenCore iso ..."
      fi
      if [ ! -f $isoFolder/OpenCore.iso ];then
          wget https://f000.backblazeb2.com/file/vGPUdrivers/OpenCore.iso.gz -P $isoFolder/ # CN+EN
          gzip -d $isoFolder/OpenCore.iso.gz
      fi

      if [ $L = "cn" ];then # CN
      echo "正在创建虚拟机..."
      else # EN
      echo "Creating VM ..."
      fi
      qm create $MacOSid -name MacOS -net0 vmxnet3,bridge=vmbr0,firewall=1 -ostype other -cpu host \
      -memory 4096 -balloon 0 -cores 4 -machine q35 -vga vmware -bios ovmf \
      -ide2 local:iso/OpenCore.iso,cache=unsafe \
      -ide0 local:iso/$1-installer.iso,cache=unsafe \
      -sata0 $storage:100,ssd=1,cache=unsafe

      qm set $MacOSid -efidisk0 $storage:0
      qm set $MacOSid -boot order=ide2

      # check cpu type
      if [ `cat /proc/cpuinfo|grep Intel|wc -l` = 0 ];then # AMD
          echo '-args: -device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc" -smbios type=2 -device usb-kbd,bus=ehci.0,port=2 -cpu Penryn,kvm=on,vendor=GenuineIntel,+kvm_pv_unhalt,+kvm_pv_eoi,+hypervisor,+invtsc,+pcid,+ssse3,+sse4.2,+popcnt,+avx,+avx2,+aes,+fma,+fma4,+bmi1,+bmi2,+xsave,+xsaveopt,check' >> /etc/pve/qemu-server/$MacOSid.conf
      else # Intel
          echo 'args: -device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc" -smbios type=2 -device usb-kbd,bus=ehci.0,port=2 -cpu host,kvm=on,vendor=GenuineIntel,+kvm_pv_unhalt,+kvm_pv_eoi,+hypervisor,+invtsc' >> /etc/pve/qemu-server/$MacOSid.conf
      fi
      cd /root/
    }

    selectMacOSversion(){
      if [ $L = "cn" ];then # CN
        OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
        无脑全自动部署一台ID为888，名字为MacOS的虚拟机，无痛黑果，小白福音！
        虚拟机默认创建时CPU为4个，内存4G，系统盘100G！请选择MacOS版本，回车继续：" 24 90 12 \
        "a" "High Sierra (10.13)" \
        "b" "Mojave (10.14)" \
        "c" "Catalina (10.15)" \
        "d" "Big Sur" \
        "q" "返回主菜单" \
        3>&1 1>&2 2>&3)
        case "$OPTION" in
          a ) runMacOSvm Sierra;;
          b ) runMacOSvm Mojave;;
          c ) runMacOSvm Catalina;;
          d ) runMacOSvm BigSur;;
          q ) CNmenu;;
        esac
      else # EN
        OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
        Script auto deploys a VM(ID=888, name=MacOS). Support multiple OSX.
        By default it has CPU=4, memory=4G, 100G hard disk" 24 90 12 \
        "a" "High Sierra (10.13)" \
        "b" "Mojave (10.14)" \
        "c" "Catalina (10.15)" \
        "d" "Big Sur" \
        "q" "Go Back to Menu" \
        3>&1 1>&2 2>&3)
        case "$OPTION" in
          a ) runMacOSvm Sierra;;
          b ) runMacOSvm Mojave;;
          c ) runMacOSvm Catalina;;
          d ) runMacOSvm BigSur;;
          q ) ENmenu;;
        esac
      fi
    }

    selectStorage(){
      if [ $L = "cn" ];then # CN
        read storage < <(
            declare -a array=()
            while read id foo{,,,,} dsc;do
            array+=($id "$dsc")
            done < <(pvesm status | sed -n '1!p')
            whiptail --title "$ksh4pveToolversion" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        selectMacOSversion
      else # EN
        read storage < <(
            declare -a array=()
            while read id foo{,,,,} dsc;do
            array+=($id "$dsc")
            done < <(pvesm status | sed -n '1!p')
            whiptail --title "$ksh4pveToolversion" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
        )
        selectMacOSversion
      fi
    }

    if [ $L = "cn" ];then # CN
      # check if pve has msr option enabled
      if [ `grep "ignore_msrs=Y" /etc/modprobe.d/kvm.conf | wc -l` = 0 ];then
          if (whiptail --title "MacOS虚拟机必要设置检查" --yes-button "继续" --no-button "返回"  --yesno "
          检测到系统尚未设置ignore选项，该设置与MacOS是否顺利安装息息相关
          回车继续设置，返回取消设置：" 10 80) then
              echo "options kvm ignore_msrs=Y" >> /etc/modprobe.d/kvm.conf
              update-initramfs -k all -u
              if (whiptail --title "MacOS虚拟机必要设置检查" --yes-button "是！重启！" --no-button "返回"  --yesno "
              搞定！请重启宿主机，回车将立即重启系统，是否继续？" 10 80) then reboot
              else exit
              fi
          else exit
          fi
      else # has msr
        selectStorage
      fi
    else # EN
        # check if pve has msr option enabled
        if [ `grep "ignore_msrs=Y" /etc/modprobe.d/kvm.conf | wc -l` = 0 ];then
            if (whiptail --title "MacOS VM msr settings" --yes-button "Continue" --no-button "Go Back"  --yesno "
            Your PVE seems not yet being correctly setup ignore_msr, which MacOS vm require.
            Press enter to enable this option: " 10 80) then
                echo "options kvm ignore_msrs=Y" >> /etc/modprobe.d/kvm.conf
                update-initramfs -k all -u
                if (whiptail --title "MacOS VM msr settings" --yes-button "Reboot host!" --no-button "Go Back"  --yesno "
                Done! Please reboot host, cotinue to reboot?" 10 80) then reboot
                else exit
                fi
            else exit
            fi
        else # has msr
          selectStorage
        fi
    fi
  }
  
  # select vm menu
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
    此脚本提供一系列VM虚拟机，无脑一键自动化部署即可使用
    后续会陆续增加更多一键虚拟机无脑自动化部署" 20 70 10 \
    "a" "部署DSM3617 -- 6.1 支持半虚拟化网卡" \
    "b" "部署DSM3615 -- 7.0 支持半虚拟化网卡" \
    "c" "部署DSM918+ -- 7.0 支持半虚拟化网卡" \
    "u" "部署Unraid --- NAS 虚拟机" \
    "m" "部署Mac OS ----黑果 虚拟机" \
    "o" "部署OpenWRT -- x86 软路由虚拟机" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
      a ) deployDSM 3617;;
      b ) deployDSM 3615;;
      c ) deployDSM 918;;
      u ) deployUnraid;;
      m ) deployMacOS;;
      o ) deployOpenWRT;;
      q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "
    Script will auto deploying all kinds of VM
    There will be more automations in future, please stay tuned!" 20 75 10 \
    "a" "Deploy DSM3617-6.1 with virtio paravirtualized" \
    "b" "Deploy DSM3615-7.0 with virtio paravirtualized" \
    "c" "Deploy DSM918 -7.0 with virtio paravirtualized" \
    "u" "Deploy Unraid VM with virtio paravirtualized" \
    "m" "Deploy MacOS VM" \
    "q" "Go Back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
      a ) deployDSM 3617;;
      b ) deployDSM 3615;;
      c ) deployDSM 918;;
      u ) deployUnraid;;
      m ) deployMacOS;;
      o ) deployOpenWRT;;
      q ) CNmenu;;
    esac
  fi
}

cpuPinning(){

  runCPUpinning(){
    # cat /sys/devices/system/cpu/cpu*/topology/thread_siblings_list | sort | uniq | tr '\n' '|'
    pinCPUs=$(whiptail --title "请输入核心编号" --inputbox "当前CPU配对:
$(lscpu -e)" 18 80 0,1,2,3 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      echo $pinCPUs > /etc/pve/qemu-server/$vmid.cpuset
      taskset --cpu-list  --all-tasks --pid $pinCPUs "$(< /run/qemu-server/$vmid.pid)"
      qm set $vmid --hookscript local:snippets/taskset-hook.sh
      clear
      echo "$(tput setaf 2)虚拟机$vmid的CPU已锁定为$pinCPUs! √$(tput sgr 0)"
    fi
  }

  if [ ! -f "/var/lib/vz/snippets/taskset-hook.sh" ];then # check if script exists
  mkdir /var/lib/vz/snippets
  cat <<< '#!/bin/bash
  vmid="$1"
  phase="$2"
  if [[ "$phase" == "post-start" ]]; then
      main_pid="$(< /run/qemu-server/$vmid.pid)"
      cpuset="$(< /etc/pve/qemu-server/$vmid.cpuset)"
      taskset --cpu-list  --all-tasks --pid "$cpuset" "$main_pid"
  fi' > /var/lib/vz/snippets/taskset-hook.sh
  chmod +x  /var/lib/vz/snippets/taskset-hook.sh
  fi

  # select VM's ID
  list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
  echo -n "">lsvm
  ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
  ls=`cat lsvm`
  rm lsvm
  h=`echo $ls|wc -l`
  let h=$h*1
  if [ $h -lt 30 ];then
      h=30
  fi
  list1=`echo $list|awk 'NR>1{print $1}'`
  
  if [ $L = "cn" ];then # CN
    vmid=$(whiptail  --title "vGPU Unlock Tools - Version : v0.0.7" --menu "选择虚拟机：" 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then runCPUpinning
      else 
      whiptail --title "Warnning" --msgbox "请重新输入100-999范围内的数字！" 10 60
      cpuPinning
      fi
    fi
  else # EN
    vmid=$(whiptail  --title "vGPU Unlock Tools - Version : v0.0.7" --menu "Choose a VM: " 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then runCPUpinning
      else 
      whiptail --title "Warnning" --msgbox "Invalid VM ID. Choose between 100-999!" 10 60
      cpuPinning
      fi
    fi
  fi
}

# --------------- adding short commands to .bashrc ---------------
  if [ `grep "alias twgdh" ~/.bashrc|wc -l` = 0 ] || [ `grep "alias pvetool" ~/.bashrc|wc -l` = 0 ];then
    if [ -f "/root/ksh4pveTool.sh" ];then
      echo "alias twgdh='/root/ksh4pveTool.sh -z'" >> ~/.bashrc
      echo "alias pvetool='/root/ksh4pveTool.sh -e'" >> ~/.bashrc
      exec "$BASH"
    elif [ -f "/root/ksh4pveTool.sh.x" ];then
      echo "alias twgdh='/root/ksh4pveTool.sh.x -z'" >> ~/.bashrc
      echo "alias pvetool='/root/ksh4pveTool.sh.x -e'" >> ~/.bashrc
      exec "$BASH"
    fi
  fi

# # --------------- adding short commands to .zshrc ---------------
#   if [ `grep "alias twgdh" ~/.zshrc|wc -l` = 0 ] || [ `grep "alias pvetool" ~/.zshrc|wc -l` = 0 ];then
#     if [ -f "/root/ksh4pveTool.sh" ];then
#       echo "alias twgdh='/root/ksh4pveTool.sh -z'" >> ~/.zshrc
#       echo "alias pvetool='/root/ksh4pveTool.sh -e'" >> ~/.zshrc
#       exec "$BASH"
#     elif [ -f "/root/ksh4pveTool.sh.x" ];then
#       echo "alias twgdh='/root/ksh4pveTool.sh.x -z'" >> ~/.zshrc
#       echo "alias pvetool='/root/ksh4pveTool.sh.x -e'" >> ~/.zshrc
#       exec "$BASH"
#     fi
#   fi

CNmenu(){ # Chinese main menu
  OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "               各类实用工具集合" 22 50 14 \
  "a" "安装美化版LS,CAT" \
  "b" "安装BTOP硬件监控" \
  "c" "添加/删除 硬盘直通" \
  "d" "查看IOMMU分组" \
  "e" "完全拆分IOMMU组(慎用)" \
  "f" "显卡直通" \
  "g" "Intel GVT-G 核显切分" \
  "h" "移除订阅弹窗" \
  "i" "PVE暗黑主题" \
  "j" "CPU性能模式调整" \
  "k" "修改apt为国内源" \
  "l" "虚拟机CPU Pinning" \
  "z" "一键虚拟机部署" \
  "q" "退出" \
  3>&1 1>&2 2>&3)
  L="cn"
  case "$OPTION" in
  a ) LSCATmenu;;
  b ) BTOPmenu;;
  c ) diskMenu;;
  d ) iommuGroupcheck;;
  e ) extraGrubsettings;;
  f ) gpuPassthrough;;
  g ) intelGVTmenu;;
  h ) removeSubs;;
  i ) darkTheme;;
  j ) cpuPerformance;;
  k ) changeSource;;
  l ) cpuPinning;;
  z ) VMdeployMenu;;
  q ) exit;;
  esac
  tput sgr 0
}
ENmenu(){ # English main menu
  OPTION=$(whiptail --title " $ksh4pveToolversion " --menu "              Useful tools for PVE" 22 50 14 \
  "a" "Install beautify LS, CAT" \
  "b" "Install BTOP" \
  "c" "Passthrough/Remove disk to VM" \
  "d" "Check IOMMU group" \
  "e" "Break IOMMU by pcie_acs_override" \
  "f" "GPU passthrough" \
  "g" "Intel GVT-G vGPU slicing" \
  "h" "Remove none-subs popup window" \
  "i" "PVE dark theme" \
  "j" "Adjust CPU performance" \
  "l" "VM CPU Pinning" \
  "z" "One click auto VM deploy" \
  "q" "Quit" \
  3>&1 1>&2 2>&3)
  L="en"
  case "$OPTION" in
  a ) LSCATmenu;;
  b ) BTOPmenu;;
  c ) diskMenu;;
  d ) iommuGroupcheck;;
  e ) extraGrubsettings;;
  f ) gpuPassthrough;;
  g ) intelGVTmenu;;
  h ) removeSubs;;
  i ) darkTheme;;
  j ) cpuPerformance;;
  k ) changeSource;;
  l ) cpuPinning;;
  z ) VMdeployMenu;;
  q ) exit;;
  esac
  tput sgr 0
}

while getopts "ze" opt; do
  case ${opt} in
    z ) 
        CNmenu
        exit;;
    e ) 
        ENmenu
        exit;;
    \? )
    echo error "Invalid option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done
if [ "$opt" = "?" ]
  then ENmenu
fi