#!/bin/bash
  ####################################--vgpu unlock tools--####################################
  #             ______  _______  __    __      __    __          __                   __       
  #            /      \|       \|  \  |  \    |  \  |  \        |  \                 |  \      
  #  __     __|  ▓▓▓▓▓▓\ ▓▓▓▓▓▓▓\ ▓▓  | ▓▓    | ▓▓  | ▓▓_______ | ▓▓ ______   _______| ▓▓   __ 
  # |  \   /  \ ▓▓ __\▓▓ ▓▓__/ ▓▓ ▓▓  | ▓▓    | ▓▓  | ▓▓       \| ▓▓/      \ /       \ ▓▓  /  \
  #  \▓▓\ /  ▓▓ ▓▓|    \ ▓▓    ▓▓ ▓▓  | ▓▓    | ▓▓  | ▓▓ ▓▓▓▓▓▓▓\ ▓▓  ▓▓▓▓▓▓\  ▓▓▓▓▓▓▓ ▓▓_/  ▓▓
  #   \▓▓\  ▓▓| ▓▓ \▓▓▓▓ ▓▓▓▓▓▓▓| ▓▓  | ▓▓    | ▓▓  | ▓▓ ▓▓  | ▓▓ ▓▓ ▓▓  | ▓▓ ▓▓     | ▓▓   ▓▓ 
  #    \▓▓ ▓▓ | ▓▓__| ▓▓ ▓▓     | ▓▓__/ ▓▓    | ▓▓__/ ▓▓ ▓▓  | ▓▓ ▓▓ ▓▓__/ ▓▓ ▓▓_____| ▓▓▓▓▓▓\ 
  #     \▓▓▓   \▓▓    ▓▓ ▓▓      \▓▓    ▓▓     \▓▓    ▓▓ ▓▓  | ▓▓ ▓▓\▓▓    ▓▓\▓▓     \ ▓▓  \▓▓\
  #      \▓     \▓▓▓▓▓▓ \▓▓       \▓▓▓▓▓▓       \▓▓▓▓▓▓ \▓▓   \▓▓\▓▓ \▓▓▓▓▓▓  \▓▓▓▓▓▓▓\▓▓   \▓▓
  #  Author : ksh
  #  Mail: kevinshane@vip.qq.com
  #  Github: https://github.com/kevinshane/unlock
  #  Credit: https://github.com/DualCoder/vgpu_unlock
  #          https://gitlab.com/polloloco/vgpu-5.15
  #  Discor: https://discord.gg/qh6dvPtxvb
  ####################################--vgpu unlock tools--####################################

# For errors look in dmesg for:
  # - BAR3 mapped
  # - Magic Found
  # - Key Found
  # - Failed to find ...
  # - Invalid sign or blocks pointer
  # - Generate signature
  # - Signature does not match
  # - Decrypted first block

# nvidia-smi Perf
  # - P0/P1 - Maximum 3D performance
  # - P2/P3 - Balanced 3D performance-power
  # - P8 - Basic HD video playback
  # - P10 - DVD playback
  # - P12 - Minimum idle power consumption

# driver download
  # https://cloud.google.com/compute/docs/gpus/grid-drivers-table

# vGaming Windows guest bypass command
  # reg add "HKLM\SOFTWARE\NVIDIA Corporation\Global" /v vGamingMarketplace /t REG_DWORD /d 2

# Linux guest bypass command
  # echo 'options nvidia NVreg_RegistryDwords="UnlicensedUnrestrictedStateTimeout=0x5A0;UnlicensedRestricted1StateTimeout=0x5A0"' | sudo tee /etc/modprobe.d/nvidia.conf
  # sudo update-initramfs -u
  # sudo reboot

# define stuff
vgpuScriptPath="/root/vgpu-name.sh"
PCI="$(lspci | grep -i nvidia | grep -i vga | awk '{print $1}' | head -n 1)"
UnlockToolsVersion="vGPU Unlock Tools - Version : v0.0.9"

# which pve version
pveRepo=`cat /etc/debian_version |awk -F"." '{print $1}'`
case "$pveRepo" in
  11 )
      pveRepo="bullseye"
      ;;
  10 )
      pveRepo="buster"
      ;;
  9 )
      pveRepo="stretch"
      ;;
  * )
      pveRepo=""
esac

startUpdate(){
  runUpdate(){
    # enable iommu group
    if [ `grep "iommu" /etc/default/grub | wc -l` = 0 ];then
      if [ `cat /proc/cpuinfo|grep Intel|wc -l` = 0 ];then 
        sed -i 's#quiet#quiet amd_iommu=on iommu=pt#' /etc/default/grub && update-grub
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已开启AMD IOMMU √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)AMD iommu satisfied √$(tput sgr 0)"
        fi
      else
        sed -i 's#quiet#quiet intel_iommu=on iommu=pt#' /etc/default/grub && update-grub
        if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已开启Intel IOMMU √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)Intel iommu satisfied √$(tput sgr 0)"
        fi
      fi
    else
      if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)已开启IOMMU √$(tput sgr 0)"
      else # EN
      echo "$(tput setaf 2)iommu satisfied √$(tput sgr 0)"
      fi
    fi

    # add vfio modules
    if [ `grep "vfio" /etc/modules|wc -l` = 0 ];then
      echo -e "vfio\nvfio_iommu_type1\nvfio_pci\nvfio_virqfd" >> /etc/modules
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已添加VFIO模组 √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)vfio satisfied √$(tput sgr 0)"
      fi
      else
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已添加VFIO模组 √$(tput sgr 0)"
        else # En
        echo "$(tput setaf 2)vfio satisfied √$(tput sgr 0)"
      fi
    fi

    # blacklist nouveau
    if [ `grep "nouveau" /etc/modprobe.d/blacklist.conf|wc -l` = 0 ];then
      echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf && update-initramfs -u
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已屏蔽nouveau驱动 √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)nouveau blocked √$(tput sgr 0)"
      fi
      else
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已屏蔽nouveau驱动 √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)nouveau blocked √$(tput sgr 0)"
      fi
    fi

    # prevent sleep when laptop lid close
    if [ `grep "HandleLidSwitch=ignore" /etc/systemd/logind.conf|wc -l` = 0 ];then
      echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf
    fi

    # remove enterprise repo
    if test -f "/etc/apt/sources.list.d/pve-enterprise.list";then 
      rm /etc/apt/sources.list.d/pve-enterprise.list
    fi

    # add none-enterprise repo
    if [ `grep "pve-no-subscription" /etc/apt/sources.list|wc -l` = 0 ];then
      echo "deb http://download.proxmox.com/debian/pve $pveRepo pve-no-subscription" >> /etc/apt/sources.list
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已设置源 √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)repo satisfied √$(tput sgr 0)"
      fi
      else
      if [ $L = "cn" ];then # CN
        echo "$(tput setaf 2)已设置源 √$(tput sgr 0)"
        else # EN
        echo "$(tput setaf 2)repo satisfied √$(tput sgr 0)"
      fi
    fi

    # update pve
    apt update && apt upgrade -y

    # by default 7.1 shipped with 5.13 which won't work with vGPU, need to upgrade to kernel 5.15
    if [ $L = "cn" ];then # CN
      if (whiptail --title "$UnlockToolsVersion" --yes-button "好的" --no-button "跳过更新"  --yesno "是否更新到5.15内核?" 8 50) then
        pveVer=`pveversion | awk -F"/" '{print $2}' | awk -F"-" '{print $1}'`
        if [ $pveVer = 7.1 ];then
          apt install -y pve-kernel-5.15
          apt install -y pve-headers-5.15
        fi
      fi
    else # EN
      if (whiptail --title "$UnlockToolsVersion" --yes-button "Yes Please" --no-button "No Skip it"  --yesno "Update PVE kernel to 5.15?" 8 50) then
        pveVer=`pveversion | awk -F"/" '{print $2}' | awk -F"-" '{print $1}'`
        if [ $pveVer = 7.1 ];then
          apt install -y pve-kernel-5.15
          apt install -y pve-headers-5.15
        fi
      fi
    fi

    # adding cpu freq quick check
    if [ `grep "alias cpu" ~/.bashrc|wc -l` = 0 ];then
      echo "alias cpu='watch -n1 grep MHz /proc/cpuinfo'" >> ~/.bashrc
    fi

    if [ $L = "cn" ];then # CN
    echo "$(tput setaf 2)=================================================================================="
    echo "搞定！请重启PVE."
    echo "重启后运行<zmkm>命令可重新进入脚本主界面(zmkm = "芝麻开门" 的拼音首字母缩写:P)"
    echo "重启后可运行<cpu>快速查看当前CPU频率"
    echo "==================================================================================$(tput setaf 0)"
    else # EN
    echo "$(tput setaf 2)=================================================================================="
    echo "Done PVE updated ! --> please reboot "
    echo "After reboot, run <unlock> to bring back the main GUI script."
    echo "You can also run <cpu> to check CPU freq. "
    echo "==================================================================================$(tput setaf 0)"
    fi

    tput sgr 0
  }
  if [ $L = "cn" ];then # CN
    if (whiptail --title "同意条款及注意事项" --yes-button "继续" --no-button "返回"  --yesno "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------

    PVE无痛自动化:
    1. 一键无脑更新PVE到最新系统
    2. 添加社区源，开启IOMMU支持，添加VFIO模组，屏蔽nouveau驱动
    3. 适合反复食用，已设置或已安装的程序会自动跳过
    4. 保持良好的网络环境

    " 20 80) then
        runUpdate
    else
        CNmenu
    fi
  else # EN
    if (whiptail --title "Agreement" --yes-button "Continue" --no-button "Go Back"  --yesno "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    You are responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    ----------------------------------------------------------------

    PVE Automation:
    1. Auto run apt updating PVE to latest
    2. Adding community repo, VFIO modules, blacklist nouveau
    3. Enable IOMMU depends on hardware
    
    " 20 80) then
        runUpdate
    else
        ENmenu
    fi
  fi
  tput sgr 0
}

checkStatus(){

  # ref
  # nvidia-smi --query-gpu=timestamp,name,pci.bus_id,driver_version,pstate,pcie.link.gen.max,
  # pcie.link.gen.current,temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used --format=csv

  memory=$(nvidia-smi --query-gpu=memory.total --format=csv | awk '/^memory/ {getline; print}' | awk '{print $1}')

  # check if currently has sliced mdev, if returns a list, then no sliced mdev
  if [ ! `$vgpuScriptPath -p ALL | grep -w "$(mdevctl list | grep -m1 nvidia | awk '{print $3}')" | wc -l` = 1 ];then
    currentType=0
    Vnum=0
    Vmemory=$(($memory / 1000))
    float=0
  else
    currentType=$($vgpuScriptPath -p ALL | grep -w "$(mdevctl list | grep -m1 nvidia | awk '{print $3}')")
    Vnum=$(echo "$currentType" | grep -o '[[:digit:]]*' | sed -n '2p')
    Vmemory=$(($memory / 1000))
    float=$(($Vmemory / $Vnum))
  fi

  # check which VM has vgpu
  clear
  if [ $L = "cn" ];then # CN
  echo "正在检测，请稍等片刻..."
  else # EN
  echo "Checking status..."
  fi

  echo -n "">lsvm
  qm list|sed '1d'|awk '{print $1}'|while read line ; 
  do 
  if [ ! `qm config $line | grep -E 'Quadro uuid|vGPU added' | wc -l` = 0 ];then
    echo $(qm config $line | grep 'name:' | awk '{print $2}') ID:$line >> lsvm
    echo "$line"
  fi
  done
  hasVGPU=`cat lsvm`
  rm lsvm

  # check currently which unlock option
  if [ -f /etc/systemd/system/mdev-startup.service ]; then unlockType="Quadro"
  else unlockType="vGPU"
  fi

  if [[ $L = "cn" ]];then # CN
  echo "$(tput setaf 2)  =====================================================================
  - 物理显卡参数
  驱动：$(nvidia-smi --query-gpu=driver_version --format=csv | sed -n '2p')
  型号：$(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')
  总线：$(nvidia-smi --query-gpu=gpu_bus_id --format=csv | sed -n '2p')
  温度：$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  功耗：$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
  显存：$memory兆

  - 切分小贴士
  1）当切分为1G显存时（即1Q型号），可同时运行$(($Vmemory / 1))台VM虚拟机
  2）当切分为2G显存时（即2Q型号），可同时运行$(($Vmemory / 2))台VM虚拟机
  3）当切分为3G显存时（即3Q型号），可同时运行$(($Vmemory / 3))台VM虚拟机
  4）当切分为4G显存时（即4Q型号），可同时运行$(($Vmemory / 4))台VM虚拟机
  5）当切分为6G显存时（即6Q型号），可同时运行$(($Vmemory / 6))台VM虚拟机
  6）当切分为8G显存时（即8Q型号），可同时运行$(($Vmemory / 8))台VM虚拟机

  - 当前切分状态
  切分型号：$currentType
  切分显存："$Vnum"G
  解锁类型：$unlockType

  - 以下VM正在使用切分：
  $hasVGPU
                                                                -- by ksh
  =======================================================================$(tput sgr 0)"
  else # EN
  echo "$(tput setaf 2)  =====================================================================
  - Graphics Card
  Version: $(nvidia-smi --query-gpu=driver_version --format=csv | sed -n '2p')
  Type: $(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')
  BusID: $(nvidia-smi --query-gpu=gpu_bus_id --format=csv | sed -n '2p')
  Temp: $(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  Power: $(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
  Vram: $memory Mib

  - Slice Tips
  1) When slicing to 1G Vram, it can run up to $(($Vmemory / 1)) VM simultaneously
  2) When slicing to 2G Vram, it can run up to $(($Vmemory / 2)) VM simultaneously
  3) When slicing to 3G Vram, it can run up to $(($Vmemory / 3)) VM simultaneously
  4) When slicing to 4G Vram, it can run up to $(($Vmemory / 4)) VM simultaneously
  5) When slicing to 6G Vram, it can run up to $(($Vmemory / 6)) VM simultaneously
  6) When slicing to 8G Vram, it can run up to $(($Vmemory / 8)) VM simultaneously

  - vGPU slicing status
  Sliced type: $currentType
  Sliced vRam: "$Vnum"G
  Unlock type: $unlockType

  - Which VM has vGPU
  $hasVGPU
                                                                -- by ksh
  =======================================================================$(tput sgr 0)"
  fi
}

deployvGPU(){
  vGPUassign(){
    # delete any quadro conf if exist
    sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/mdev/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/args: -uuid/d' /etc/pve/qemu-server/$vmid.conf

    # delete comments for quadro and vgpu
    sed -i '/Quadro/d' /etc/pve/qemu-server/$vmid.conf
    sed -i '/vGPU added/d' /etc/pve/qemu-server/$vmid.conf

    # stop all mdevs if exist
    qm list|sed '1d'|awk '{print $1}'|while read line ; 
    do 
      if [ `grep -E "mdev=" /etc/pve/qemu-server/*.conf|wc -l` = 1 ];then
        mdevctl stop -u 00000000-0000-0000-0000-000000000$line
      fi;
    done

    # adding uuid to conf
    sed -i -r "1i args: -uuid 00000000-0000-0000-0000-000000000$vmid" /etc/pve/qemu-server/$vmid.conf
    sed -r -i "1i #vGPU added" /etc/pve/qemu-server/$vmid.conf

    # add mdev device, example: hostpci0: 03:00.0,mdev=nvidia-437
    sed -r -i "1i hostpci1: $PCI,mdev=$vGPUtype" /etc/pve/qemu-server/$vmid.conf

    if [ $L = "cn" ];then # CN
    echo "$(tput setaf 2)设置完毕！请到网页端启动虚拟机$vmid查看! $(tput setaf 0)"
    else # EN
    echo "$(tput setaf 2)vGPU assigned! Go to PVE webGui->VM$vmid->start VM $(tput setaf 0)"
    fi
  }

  vGPUselect(){ 
    # check RTX or GTX
    checkGPU=$(nvidia-smi --query-gpu=gpu_name --format=csv | grep -i geforce | head -n1)

    if [ `lspci | grep VGA | grep TU | wc -l` -ne 0 ] || [[ $checkGPU =~ .*"20".* ]] || [[ $checkGPU =~ .*"16".* ]]; then # is a RTX turing
      if [ $L = "cn" ];then # CN
        vGPUtype=$(whiptail  --title "$UnlockToolsVersion" --menu "脚本默认添加A型号！如需选择其他型号，请在WebGui里自行修改" 16 65 8 \
        "a" "RTX6000-1A  -> 切分为1G显存"  \
        "b" "RTX6000-2A  -> 切分为2G显存"  \
        "c" "RTX6000-3A  -> 切分为3G显存"  \
        "d" "RTX6000-4A  -> 切分为4G显存"  \
        "e" "RTX6000-6A  -> 切分为6G显存"  \
        "f" "RTX6000-8A  -> 切分为8G显存"  \
        "g" "RTX6000-12A -> 切分为12G显存"  \
        "h" "RTX6000-24A -> 切分为24G显存"  \
        3>&1 1>&2 2>&3)
      else # EN
        vGPUtype=$(whiptail  --title "$UnlockToolsVersion" --menu "Script creates A type ONLY, for other types plz modify in WebGui" 16 70 8 \
        "a" "RTX6000-1A  -> Slice to 1G profile"  \
        "b" "RTX6000-2A  -> Slice to 2G profile"  \
        "c" "RTX6000-3A  -> Slice to 3G profile"  \
        "d" "RTX6000-4A  -> Slice to 4G profile"  \
        "e" "RTX6000-6A  -> Slice to 6G profile"  \
        "f" "RTX6000-8A  -> Slice to 8G profile"  \
        "g" "RTX6000-12A -> Slice to 12G profile"  \
        "h" "RTX6000-24A -> Slice to 24G profile"  \
        3>&1 1>&2 2>&3)
      fi
      case "$vGPUtype" in
        a )	vGPUtype="nvidia-437"
            vGPUassign
            ;;
        b )	vGPUtype="nvidia-438"
            vGPUassign
            ;;
        c )	vGPUtype="nvidia-439"
            vGPUassign
            ;;
        d )	vGPUtype="nvidia-440"
            vGPUassign
            ;;
        e )	vGPUtype="nvidia-441"
            vGPUassign
            ;;
        f )	vGPUtype="nvidia-442"
            vGPUassign
            ;;
        g )	vGPUtype="nvidia-443"
            vGPUassign
            ;;
        h )	vGPUtype="nvidia-444"
            vGPUassign
            ;;
      esac
    fi
    if [ `lspci | grep VGA | grep GP | wc -l` -ne 0 ] || [[ $checkGPU =~ .*"10".* ]]; then # is a GTX pascal
      if [ $L = "cn" ];then # CN
        vGPUtype=$(whiptail  --title "$UnlockToolsVersion" --menu "脚本默认添加A型号！如需选择其他型号，请在webUI里自行修改" 16 65 8 \
        "a" "P40-1A  -> 切分为1G显存"  \
        "b" "P40-2A  -> 切分为2G显存"  \
        "c" "P40-3A  -> 切分为3G显存"  \
        "d" "P40-4A  -> 切分为4G显存"  \
        "e" "P40-6A  -> 切分为6G显存"  \
        "f" "P40-8A  -> 切分为8G显存"  \
        "g" "P40-12A -> 切分为12G显存"  \
        "h" "P40-24A -> 切分为24G显存"  \
        3>&1 1>&2 2>&3)
      else # EN
        vGPUtype=$(whiptail  --title "$UnlockToolsVersion" --menu "Script creates A type ONLY, for other types plz modify in WebGui" 16 70 8 \
        "a" "P40-1A  -> Slice to 1G profile"  \
        "b" "P40-2A  -> Slice to 2G profile"  \
        "c" "P40-3A  -> Slice to 3G profile"  \
        "d" "P40-4A  -> Slice to 4G profile"  \
        "e" "P40-6A  -> Slice to 6G profile"  \
        "f" "P40-8A  -> Slice to 8G profile"  \
        "g" "P40-12A -> Slice to 12G profile"  \
        "h" "P40-24A -> Slice to 24G profile"  \
        3>&1 1>&2 2>&3)
      fi
      case "$vGPUtype" in
      a )	vGPUtype="nvidia-54"
          vGPUassign
          ;;
      b )	vGPUtype="nvidia-55"
          vGPUassign
          ;;
      c )	vGPUtype="nvidia-56"
          vGPUassign
          ;;
      d )	vGPUtype="nvidia-57"
          vGPUassign
          ;;
      e )	vGPUtype="nvidia-58"
          vGPUassign
          ;;
      f )	vGPUtype="nvidia-59"
          vGPUassign
          ;;
      g )	vGPUtype="nvidia-60"
          vGPUassign
          ;;
      h )	vGPUtype="nvidia-61"
          vGPUassign
          ;;
      esac
    fi
  }

  # select VM's ID
  clear
  list=`qm list|awk 'NR>1{print $1":"$2".................."$3" "}'`
  echo -n "">lsvm
  ls=`for i in $list;do echo $i|awk -F ":" '{print $1" "$2}'>>lsvm;done`
  ls=`cat lsvm`
  rm lsvm
  h=`echo $ls|wc -l`
  let h=$h*1
  if [ $h -lt 30 ];then
      h=30
  fi
  list1=`echo $list|awk 'NR>1{print $1}'`
  
  if [ $L = "cn" ];then # CN
    vmid=$(whiptail  --title "$UnlockToolsVersion" --menu "选择虚拟机：" 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then vGPUselect
      else 
      whiptail --title "Warnning" --msgbox "请重新输入100-999范围内的数字！" 10 60
      deployvGPU
      fi
    fi
  else # EN
    vmid=$(whiptail  --title "$UnlockToolsVersion" --menu "Choose a VM: " 20 60 10 \
    $(echo $ls) \
    3>&1 1>&2 2>&3)

    exitstatus=$?
    if [ $exitstatus = 0 ]; then
      if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then vGPUselect
      else 
      whiptail --title "Warnning" --msgbox "Invalid VM ID. Choose between 100-999!" 10 60
      deployvGPU
      fi
    fi
  fi
}

vGPUMenu(){
  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title "同意条款及注意事项" --menu "
    ----------------------------------------------------------------------
    此脚本涉及的命令行操作具备一定程度损坏硬件的风险，固仅供测试
    此脚本核心代码均来自网络，up主仅搬运流程并自动化，固版权归属原作者
    部署及使用者需自行承担相关操作风险及后果，up主不对操作承担任何相关责任
    继续执行则表示使用者无条件同意以上条款，如不同意请退出脚本
    ----------------------------------------------------------------------
    - vGPU将解锁所有功能，即完整的CUDA和OpenCL
    - vGPU需正版授权，请自行购买支持正版

    ！！！注意：请停止所有VM再执行命令！！！" 21 80 3 \
    "a" "添加vGPU到虚拟机" \
    "b" "部署一台授权服务器VM" \
    "q" "返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) deployvGPU;;
    b ) setupLXC;;
    q ) tput sgr 0
        CNmenu;;
    esac
    tput sgr 0
  else # EN
    OPTION=$(whiptail --title " $UnlockToolsVersion " --menu "
    ----------------------------------------------------------------
    Script may possible damaging your harware, use at your own risk.
    I'll not take responible to what you have done in the next step.
    Please do not use for commercial or any production environment.
    Credits to vgpu_unlock github that make this happen.
    ----------------------------------------------------------------
    - vGPU unlocks everything, including CUDA + OpenCL
    - You need to deploy a license-VM in order to use vGPU profile

    Please STOP all VM before running this script !!!" 21 80 3 \
    "a" "Assign vGPU profile to a VM" \
    "b" "Deploy a VM for vGPU license server" \
    "q" "Go back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) deployvGPU;;
    b ) setupLXC;;
    q ) tput sgr 0
        ENmenu;;
    esac
    tput sgr 0
  fi
}

setupLXC(){
  if [ $L = "cn" ];then
    if (whiptail --title "LXC CentOS 7.9 授权服务器" --yes-button "继续" --no-button "返回"  --yesno "
      默认虚拟机配置：2CPU 2G
      默认IP地址：192.168.1.6
      默认访问网址：http://192.168.1.6:8080/licserver/
      默认终端登陆用户：root 密码：abc12345

      运行后注意事项：
      1）请在PVE网页端修改IP地址为你局域网的网段
      2）运行登陆后务必输入passwd修改默认密码
      3）第一次启动速度较慢，请耐心等待CPU占用接近0时再访问网址
      4）默认开机不启动，但强烈推荐设置VM为开机自启
      5）如遇无法下载，请手动百度云：
      https://pan.baidu.com/s/15TYh5PDfqmcEgwoDEv0aQQ 提取码：rldj
      下载完手动上传到/var/lib/vz/dump/里，注意下载的文件不要重命名！
      上传完毕后，重新运行此脚本即可
      " 23 80) then

      read storage < <(
          declare -a array=()
          while read id foo{,,,,} dsc;do
          array+=($id "$dsc")
          done < <(pvesm status | sed -n '1!p')
          whiptail --title "$UnlockToolsVersion" --menu '请选择虚拟机的存储位置，建议存放于SSD或NVME硬盘里：' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
      )

      vmid=$(whiptail --inputbox "请输入授权服务器的虚拟机ID，默认是100" 8 60 100 --title "输入VM的ID值" 3>&1 1>&2 2>&3)
      exitstatus=$?
      if [ $exitstatus = 0 ]; then
          if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then

            if [ ! -x /usr/local/bin/gdown ];then pip3 install gdown; fi
          
            if [ ! -f "/var/lib/vz/dump/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz" ];then
            echo "$(tput setaf 1)远程下载中，文件较大请耐心等候...$(tput setaf 0)"
            gdown https://drive.google.com/uc?id=11xQe_9F_8zKX3WEqE-oq9EnmpdUkWhSU -O /var/lib/vz/dump/
            
            pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
            echo "$(tput setaf 2)搞定，请移步PVE网页端查看！$(tput setaf 0)"
            else
            pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
            echo "$(tput setaf 2)搞定，请移步PVE网页端查看！$(tput setaf 0)"
            fi

          else 
          whiptail --title "Warnning" --msgbox "请重新输入100-999范围内的数字！" 10 60
          setupLXC
          fi
      fi
      else CNmenu
    fi
  else # EN
    if (whiptail --title "LXC CentOS 7.9 License Server" --yes-button "Continue" --no-button "Go Back"  --yesno "
    default VM settings: 2 vCPU + 2G ram
    default IP addr: 192.168.1.6
    default Webpage: http://192.168.1.6:8080/licserver/
    default login user: root / password: abc12345

    Notice:
    1) Go to PVE webgui, set IP addr to your local network IP range
    2) Please change root login password when first launch
    3) Slow on first launch, please be patient
    4) It's recommanded to set the VM start on PVE boots
    " 20 80) then

    read storage < <(
        declare -a array=()
        while read id foo{,,,,} dsc;do
        array+=($id "$dsc")
        done < <(pvesm status | sed -n '1!p')
        whiptail --title "$UnlockToolsVersion" --menu 'Which storage you want the VM to store from:' 12 90 4 "${array[@]}" 2>&1 >/dev/tty
    )

    vmid=$(whiptail --inputbox "What's the VM id you want to deploy license sever? default is 100" 8 60 100 --title "define VM ID" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        if [ "$vmid" -le 999 -a "$vmid" -ge 100 ]; then

          if [ ! -x /usr/local/bin/gdown ];then pip3 install gdown; fi

          if [ ! -f "/var/lib/vz/dump/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz" ];then
          echo "$(tput setaf 1)backup not exist, downloading...$(tput setaf 0)"
          gdown https://drive.google.com/uc?id=11xQe_9F_8zKX3WEqE-oq9EnmpdUkWhSU -O /var/lib/vz/dump/
          
          pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
          echo "$(tput setaf 2)Done! Please check webgui! $(tput setaf 0)"
          else
          pct restore $vmid local:backup/vzdump-lxc-104-2021_04_26-20_21_33.tar.gz --storage $storage --unique 1 --memory 2048 --cores 2
          echo "$(tput setaf 2)Done! Please check webgui! $(tput setaf 0)"
          fi

        else 
        whiptail --title "Warnning" --msgbox "Invalid VM ID. Choose between 100-999!" 10 60
        setupLXC
        fi
    fi
    else ENmenu
    fi
  fi
}

runReset(){
  if [ $L = "cn" ];then # CN
  echo "$(tput setaf 2)正在重置...$(tput setaf 0)"
  else # EN
  echo "$(tput setaf 2)Start reset process...$(tput setaf 0)"
  fi
  sed -i '/vfio-pci,sysfsdev=/d' /etc/pve/qemu-server/*.conf
  sed -i '/mdev/d' /etc/pve/qemu-server/*.conf
  sed -i '/-uuid/d' /etc/pve/qemu-server/*.conf
  sed -i '/#vGPU added/d' /etc/pve/qemu-server/*.conf
  sed -i '/#Quadro uuid/d' /etc/pve/qemu-server/*.conf

  if [ ! `grep -E "mdev=" /etc/pve/qemu-server/*.conf|wc -l` = 0 ];then
    qm list|sed '1d'|awk '{print $1}'|while read line ; do mdevctl stop -u 00000000-0000-0000-0000-000000000$line; done
  fi

  if [ $L = "cn" ];then # CN
  echo "$(tput setaf 2)初始化完成，所有vGPU资源均已释放完毕！$(tput setaf 0)"
  else # EN
  echo "$(tput setaf 2)Done reset! All vGPU resources released!$(tput setaf 0)"
  fi
}

resetDefaultvGPU(){
  if [ $L = "cn" ];then # CN
    if (whiptail --title "初始化状态" --yes-button "继续" --no-button "返回"  --yesno "
    初始化所有相关vGPU和Quadro设置，整个过程无需重启
    或者你希望重新设置切分，此脚本将恢复到最初状态

    1）释放所有mdev设备
    2）删除所有跟vGPU相关的自启动服务
    3）删除所有虚拟机conf跟vGPU相关的设置
    4）所有虚拟机将恢复成无显卡直通的初始化状态

    " 15 80) then runReset
    else CNmenu
    fi
  else # EN
    if (whiptail --title "Reset to default" --yes-button "Continue" --no-button "Go Back"  --yesno "
    Script will auto reset everthing related to vGPU/Quadro
    Script doesn't require reboot after reset process

    1) Release all mdev devices to default
    2) Delete all startup services
    3) Delete all vGPU related settings for all VM's conf
    4) All VM will reset to no-vGPU mode

    " 15 80) then runReset
    else ENmenu
    fi
  fi
}

realtimeHW(){
  # ref variable, some not used
  memory=$(nvidia-smi --query-gpu=memory.total --format=csv | awk '/^memory/ {getline; print}' | awk '{print $1}')
  ClkSpeed=$(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p')
  MemSpeed=$(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p')
  GrpSpeed=$(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p')
  MaxMemClk=$(nvidia-smi -q -d CLOCK | grep Memory | sed -n '4p' | awk '{print $3}')
  MaxSMClk=$(nvidia-smi -q -d CLOCK | grep SM | sed -n '3p' | awk '{print $3}')
  fan=$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)
  useWatt=$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p')
  MaxWatt=$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $7}' | head -n1)
  gpuTemp=$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')
  gpuPerf=$(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  gpuUsage=$(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  gpuMemUse=$(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  gpuMemFree=$(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')
  gpuName=$(nvidia-smi --query-gpu=gpu_name --format=csv | sed -n '2p')

  if [[ $L = "cn" ]];then # CN
  while :; do
  echo "
  ======realtime=======
  $gpuName
  温度：$(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  性能：$(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  功耗：$(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p') / $MaxWatt
  占用：$(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  风扇：$(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)

  显存占用
  已用：$(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  空闲：$(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')

  实时频率
  核心：$(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p') / $MaxSMClk MHz
  显存：$(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p') / $MaxMemClk MHz
  图形：$(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p') / $MaxSMClk MHz

  $(nvidia-smi --query-gpu=timestamp --format=csv | sed -n '2p')
  =========ksh========="
  sleep 1
  done

  else # EN

  while :; do
  echo "
  ======realtime=======
  $gpuName
  Temp:  $(nvidia-smi --query-gpu=temperature.gpu --format=csv | sed -n '2p')°C
  Perf:  $(nvidia-smi --query-gpu=pstate --format=csv | sed -n '2p')
  Power: $(nvidia-smi --query-gpu=power.draw --format=csv | sed -n '2p') / $MaxWatt
  Usage: $(nvidia-smi --query-gpu=utilization.gpu --format=csv | sed -n '2p')
  fan:   $(nvidia-smi | awk '/[Gg]e[Ff]orce/ {f=NR} f && NR==f+1' | awk '{print $2}' | head -n1)

  Vram Usage
  Use:   $(nvidia-smi --query-gpu=memory.used --format=csv | sed -n '2p')
  Free:  $(nvidia-smi --query-gpu=memory.free --format=csv | sed -n '2p')

  Realtime Clock Speed
  Core:  $(nvidia-smi --query-gpu=clocks.sm --format=csv | sed -n '2p') / $MaxSMClk MHz
  Mem:   $(nvidia-smi --query-gpu=clocks.mem --format=csv | sed -n '2p') / $MaxMemClk MHz
  Graph: $(nvidia-smi --query-gpu=clocks.gr --format=csv | sed -n '2p') / $MaxSMClk MHz

  $(nvidia-smi --query-gpu=timestamp --format=csv | sed -n '2p')
  =========ksh========="
  sleep 1
  done

  fi

}

checkNVlog(){
  journalctl --no-hostname -f -n 30 -u nvidia-vgpu-mgr.service
}

selectRustVersion(){
  checkIOMMU(){ # check iommu first
    if compgen -G "/sys/kernel/iommu_groups/*/devices/*" > /dev/null; then
      # download helper script
      wget https://raw.githubusercontent.com/DualCoder/vgpu_unlock/master/scripts/vgpu-name.sh -P /root/
      chmod +x /root/vgpu-name.sh
      echo "alias vgpu='/root/vgpu-name.sh -p ALL'" >> ~/.bashrc

      # run into fn
      if [ $driver = "pve7" ];then pve7install
        else runRustUnlock
      fi
    else # vt-d not supported/enabled
      if [ $L = "cn" ];then # CN
        if (whiptail --title "主板或硬件不符合要求" --ok-button "返回" --msgbox "当前主板似乎没有打开VT-D, 请确认主板支持且在BIOS里启用该选项" 8 65) then CNmenu
        fi
        else # EN
        if (whiptail --title "Motherboard issue" --ok-button "Go Back" --msgbox "It seems you don't have VT-D enabled! Please enable in BIOS!" 8 65) then ENmenu
        fi
      fi
    fi
  }

  if [ $L = "cn" ];then # CN
    OPTION=$(whiptail --title "选择驱动版本" --menu "请根据当前PVE版本选择相应的宿主机驱动" 13 65 5 \
    "a" "PVE6 450.156 ------- PVE6.4 稳定高性能首选" \
    "b" "PVE7 460.73.01 ----- PVE7.1 仅适用于7.1版本" \
    "q" "Main Menu ---------- 返回主菜单" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) driver="450.156"
        checkIOMMU;;
    b ) driver="pve7"
        checkIOMMU;;
    q ) CNmenu;;
    esac
  else # EN
    OPTION=$(whiptail --title "Select driver version" --menu "Select drivers suit for your current PVE" 13 65 5 \
    "a" "PVE6 450.156 --------- Most stable on PVE6.4" \
    "b" "PVE7 460.73.01 ------- Only works on 7.1" \
    "q" "Main Menu ------------ Go Back to Main Menu" \
    3>&1 1>&2 2>&3)
    case "$OPTION" in
    a ) driver="450.156"
        checkIOMMU;;
    b ) driver="pve7"
        checkIOMMU;;
    q ) ENmenu;;
    esac
  fi
}

runRustUnlock(){ # for pve 6.4
  # setup rust env
  apt update && apt install -y build-essential dkms pve-headers git rustc jq

  # prepare unlock files and drivers
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在解锁...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)unlocking...$(tput sgr 0)"
    echo "======================================================================="
    fi

    # pull from github
    cd /root
    if [ ! -d "/root/vgpu_unlock" ];then 
      git clone https://github.com/DualCoder/vgpu_unlock.git && chmod -R +x /root/vgpu_unlock/
      sed -i 's#if 0#if 1#' /root/vgpu_unlock/vgpu_unlock_hooks.c # enable debug for detail log
    fi

    # Install driver for rust
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在安装原版$driver显卡驱动程序...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)installing Nvidia $driver Driver... $(tput sgr 0)"
    echo "======================================================================="
    fi
    if [ ! -x /usr/bin/nvidia-smi ];then
      if test -f "NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run";then
      chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
      else
      wget https://f000.backblazeb2.com/file/vGPUdrivers/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run -P /root/
      chmod +x /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run
      fi
    fi
    /root/NVIDIA-Linux-x86_64-$driver-vgpu-kvm.run --dkms

    # modify driver for vgpu_unlock repo
    if [ `grep "vgpu_unlock_hooks.c" /usr/src/nvidia-$driver/nvidia/os-interface.c|wc -l` = 0 ];then
        sed -i '32a#include "/root/vgpu_unlock/vgpu_unlock_hooks.c"' /usr/src/nvidia-$driver/nvidia/os-interface.c
    fi
    if [ `grep "kern.ld" /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild|wc -l` = 0 ];then
        echo "ldflags-y += -T /root/vgpu_unlock/kern.ld" >> /usr/src/nvidia-$driver/nvidia/nvidia.Kbuild
    fi

    # remove and reinstall driver for rust
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在重新构建...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)reconfiguring...$(tput sgr 0)"
    echo "======================================================================="
    fi
    dkms remove  -m nvidia -v $driver --all
    dkms install -m nvidia -v $driver

    # manual rust cargo compile library
    # https://github.com/mbilker/vgpu_unlock-rs
    # cd /root/vgpu_unlock-rs/
    # cargo build --release
    # mv /root/vgpu_unlock-rs/target/release/libvgpu_unlock_rs.so /root/libvgpu_unlock_rs.so
    # cd /root/ && rm -r vgpu_unlock-rs/

    # pull from remote precompiled lib
    wget https://f000.backblazeb2.com/file/vGPUdrivers/libvgpu_unlock_rs.so -P /root/ # precompiled lib
    chmod +x /root/libvgpu_unlock_rs.so

    # rust modify for unlock
    mkdir -p /etc/systemd/system/nvidia-vgpud.service.d
    mkdir -p /etc/systemd/system/nvidia-vgpu-mgr.service.d
    echo -e "[Service]\nEnvironment=LD_PRELOAD=/root/libvgpu_unlock_rs.so" >> /etc/systemd/system/nvidia-vgpud.service.d/vgpu_unlock.conf
    echo -e "[Service]\nEnvironment=LD_PRELOAD=/root/libvgpu_unlock_rs.so" >> /etc/systemd/system/nvidia-vgpu-mgr.service.d/vgpu_unlock.conf
    mkdir -p /etc/vgpu_unlock && cd /etc/vgpu_unlock
    if [ $L = "cn" ];then # CN
      wget https://f000.backblazeb2.com/file/vGPUdrivers/CN_profile_override.toml -O /etc/vgpu_unlock/profile_override.toml # CN comment
      else # EN
      wget https://f000.backblazeb2.com/file/vGPUdrivers/EN_profile_override.toml -O /etc/vgpu_unlock/profile_override.toml # EN comment
    fi

    # modify for easy access command
    cd /root
    if [ `grep "libvgpu_unlock_rs" ~/.bashrc|wc -l` = 0 ];then
      echo "alias nvidia-smi='LD_PRELOAD=/root/libvgpu_unlock_rs.so nvidia-smi'" >> ~/.bashrc
    fi
    if [ `grep "profile_override" ~/.bashrc|wc -l` = 0 ];then
      echo "alias edit='nano /etc/vgpu_unlock/profile_override.toml'" >> ~/.bashrc
    fi

    # install mdev
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在安装mdev设备...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)installing mdev...$(tput sgr 0)"
    echo "======================================================================="
    fi
    
    cd /root
    if [ -x /usr/sbin/mdevctl ];then
    if [ $L = "cn" ];then # CN
      echo "$(tput setaf 2)√ 已安装mdev$(tput sgr 0)"
      else # EN
      echo "$(tput setaf 2)√ mdev installed! $(tput sgr 0)"
    fi
    else
    wget https://github.com/mdevctl/mdevctl/archive/refs/tags/0.81.tar.gz
    tar -zxvf 0.81.tar.gz && rm 0.81.tar.gz
    cd mdevctl-0.81
    make install
    cd /root/ && rm -r /root/mdevctl-0.81/
    fi

  # needs reboot
  if [ $L = "cn" ];then # CN
    echo "$(tput setaf 2)√ 搞定! 请重启宿主机! $(tput sgr 0)"
    else # EN
    echo "$(tput setaf 2)√ Done! Please reboot the host! $(tput sgr 0)"
  fi
}

pve7install(){ # for pve 7.x
  # setup rust env for pve7
  if [ $L = "cn" ];then # CN
  echo "======================================================================="
  echo "$(tput setaf 2)正在安装依赖...$(tput sgr 0)"
  echo "======================================================================="
  else # EN
  echo "======================================================================="
  echo "$(tput setaf 2)Installing apps...$(tput sgr 0)"
  echo "======================================================================="
  fi

  # install stuff
  apt install -y build-essential dkms git rustc jq mdevctl

  # set driver version for pve7
  driver="460.73.01"
  preUnlocked="NVIDIA-Linux-x86_64-460.73.01-grid-vgpu-kvm.run"

  # prepare unlock files and drivers
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在解锁...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)unlocking...$(tput sgr 0)"
    echo "======================================================================="
    fi

    # Install driver for rust
    if [ $L = "cn" ];then # CN
    echo "======================================================================="
    echo "$(tput setaf 2)正在安装宿主机$driver显卡驱动程序...$(tput sgr 0)"
    echo "======================================================================="
    else # EN
    echo "======================================================================="
    echo "$(tput setaf 2)installing host Nvidia ver:$driver Driver... $(tput sgr 0)"
    echo "======================================================================="
    fi

    # use precompiled drivers instead
    if [ ! -x /usr/bin/nvidia-smi ];then
      if test -f $preUnlocked;then
      chmod +x /root/$preUnlocked
      else
      wget https://github.com/VGPU-Community-Drivers/Merged-Rust-Drivers/raw/main/$preUnlocked -P /root/
      chmod +x /root/$preUnlocked
      fi
    fi
    /root/$preUnlocked --dkms

    # pull latest precompiled lib from remote
    if [ $L = "cn" ];then # CN
      wget https://f000.backblazeb2.com/file/vGPUdrivers/libvgpu_unlock_rs.so -P /root/ # CN
      else # EN
      wget https://github.com/kevinshane/unlock/releases/download/unlock/libvgpu_unlock_rs.so -P /root/ # EN
    fi
    chmod +x /root/libvgpu_unlock_rs.so
    cp /root/libvgpu_unlock_rs.so /usr/lib/nvidia
    cp /root/libvgpu_unlock_rs.so /usr/lib/x86_64-linux-gnu/

    # manual rust modify for unlock
      # mkdir -p /etc/systemd/system/nvidia-vgpud.service.d
      # mkdir -p /etc/systemd/system/nvidia-vgpu-mgr.service.d
      # echo -e "[Service]\nEnvironment=LD_PRELOAD=/root/libvgpu_unlock_rs.so" >> /etc/systemd/system/nvidia-vgpud.service.d/vgpu_unlock.conf
      # echo -e "[Service]\nEnvironment=LD_PRELOAD=/root/libvgpu_unlock_rs.so" >> /etc/systemd/system/nvidia-vgpu-mgr.service.d/vgpu_unlock.conf
    # end manual rust modify for unlock

    # pull example override.toml
    mkdir -p /etc/vgpu_unlock && cd /etc/vgpu_unlock
    if [ $L = "cn" ];then # CN
      wget https://f000.backblazeb2.com/file/vGPUdrivers/CN_profile_override.toml -O /etc/vgpu_unlock/profile_override.toml # CN comment
      else # EN
      wget https://f000.backblazeb2.com/file/vGPUdrivers/EN_profile_override.toml -O /etc/vgpu_unlock/profile_override.toml # EN comment
    fi

    # modify for easy access command
    cd /root
    if [ `grep "libvgpu_unlock_rs" ~/.bashrc|wc -l` = 0 ];then
      echo "alias nvidia-smi='LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libvgpu_unlock_rs.so nvidia-smi'" >> ~/.bashrc
    fi
    if [ `grep "profile_override" ~/.bashrc|wc -l` = 0 ];then
      echo "alias edit='nano /etc/vgpu_unlock/profile_override.toml'" >> ~/.bashrc
    fi

  # # final clean up
  rm /root/libvgpu_unlock_rs.so
  rm /root/$preUnlocked

  if [ $L = "cn" ];then # CN
    echo "$(tput setaf 2)√ 搞定! 请重启宿主机! $(tput sgr 0)"
    else # EN
    echo "$(tput setaf 2)√ Done! Please reboot PVE! $(tput sgr 0)"
  fi
}

# -------------------------------------------------- adding easy commands to .bashrc ------------------------------------------------- #
if [ `grep "alias unlock" ~/.bashrc|wc -l` = 0 ] || [ `grep "alias zmkm" ~/.bashrc|wc -l` = 0 ];then
  if [ -f "/root/rust-unlock.sh" ];then
    echo "alias unlock='/root/rust-unlock.sh -e'" >> ~/.bashrc # EN
    echo "alias zmkm='/root/rust-unlock.sh -z'" >> ~/.bashrc # ZH-CN
    echo "alias vlog='journalctl --no-hostname -f -n 30 -u nvidia-vgpu-mgr.service'" >> ~/.bashrc
    source .bashrc
  elif [ -f "/root/rust-unlock.sh.x" ];then
    echo "alias unlock='/root/rust-unlock.sh.x -e'" >> ~/.bashrc # EN
    echo "alias zmkm='/root/rust-unlock.sh.x -z'" >> ~/.bashrc # ZH-CN
    echo "alias vlog='journalctl --no-hostname -f -n 30 -u nvidia-vgpu-mgr.service'" >> ~/.bashrc
    source .bashrc
  fi
fi

# --------------------------------------------------------- Startup Main Menu --------------------------------------------------------- #
CNmenu(){ # Chinese main menu
  OPTION=$(whiptail --title " $UnlockToolsVersion " --menu "新装PVE请务必先执行第一步和第二步" 16 60 8 \
  "a" "第一步-更新系统" \
  "b" "第二步-解锁vGPU" \
  "c" "第三步-vGPU切分部署" \
  "s" "当前切分状态" \
  "r" "重置所有设置" \
  "t" "实时硬件状态" \
  "v" "查看日志" \
  "q" "退出程序" \
  3>&1 1>&2 2>&3)
  L="cn"
  case "$OPTION" in
  a ) startUpdate;;
  b ) selectRustVersion;;
  c ) vGPUMenu;;
  s ) checkStatus;;
  r ) resetDefaultvGPU;;
  t ) realtimeHW;;
  v ) checkNVlog;;
  q ) tput sgr 0
      exit;;
  esac
  tput sgr 0
}

ENmenu(){ # English main menu
  OPTION=$(whiptail --title " $UnlockToolsVersion " --menu "Run step A and B first if it's a fresh PVE" 16 60 8 \
  "a" "Update PVE" \
  "b" "Unlock vGPU" \
  "c" "vGPU deployment" \
  "s" "Current slice status" \
  "r" "Reset to default" \
  "t" "Real time info" \
  "v" "Check vGPU log" \
  "q" "Quit" \
  3>&1 1>&2 2>&3)
  L="en"
  case "$OPTION" in
  a ) startUpdate;;
  b ) selectRustVersion;;
  c ) vGPUMenu;;
  s ) checkStatus;;
  r ) resetDefaultvGPU;;
  t ) realtimeHW;;
  v ) checkNVlog;;
  q ) tput sgr 0
      exit;;
  esac
  tput sgr 0
}

while getopts "ze" opt; do
  case ${opt} in
    z ) 
        CNmenu
        exit;;
    e ) 
        ENmenu
        exit;;
    \? )
    echo error "Invalid option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done
if [ "$opt" = "?" ]
  then ENmenu
fi